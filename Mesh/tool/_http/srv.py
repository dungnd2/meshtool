import SimpleHTTPServer
import SocketServer
try:
    import thread
except ImportError:
    import _thread as thread


PORT = 8888

class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            print "GET /"
            self.path = 'index.html'
        return SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

class SimpleHttpServer:
    def __init__(self):
        self.handler = MyRequestHandler
        self.server = SocketServer.TCPServer(('', PORT), self.handler)
        

    def start(self):

        def run(*args):
            self.server.serve_forever()

        thread.start_new_thread(run, ())


# server = SimpleHttpServer()
# server.start()
