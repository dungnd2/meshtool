import http.client
import json


class HttpCli:
    def __init__(self, host):
        #"vng.icbm.vn"
        self.host = host
        self.connection = http.client.HTTPSConnection(self.host, timeout=2)

    def close(self):
        self.connection.close()

    def GET(self, path):
        try:
            print "GET: {}".format(path)
            self.connection.request("GET", path)
            response = self.connection.getresponse()
            return response.read().decode()

        except:
            # print "GET EXCEPTION"
            return ""
        