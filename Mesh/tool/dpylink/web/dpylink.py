
import pylink
import sys
import time
import os


import subprocess

try:
    import thread
except ImportError:
    import _thread as thread

import threading

import paho.mqtt.client as mqtt

import json


FCONFIG = open("./setting.json", "r")
SETTING  = json.loads(FCONFIG.read())
HARD_UNICAST = 1
jlink = pylink.JLink()

DEVICE_TYPE = ""


def on_connect(client, userdata, flags, rc):
    # print("MQTT connected with result code "+str(rc))
    client.subscribe(SETTING["DEVICE_RECEIVER_CHANNEL"])

def on_message(client, userdata, msg):

    MSG = json.loads(str(msg.payload))    
    if SETTING["FLASHOR"] == MSG["by_user"]:
        UNICAST = MSG["address"]

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_messages

def mqtt_loop():
    global client
    while True:
        client.loop()

def mqtt_init():
    print "dung"
    # global client
    # client.connect(BROKER, 1883, 10)
    # thread.start_new_thread(mqtt_loop, ())

def mqtt_publish(data):
    global client
    # print "MQTT publishing:", data
    client.publish(SEVER_RECEIVER_CHANNEL, data)



class UnicastGetter(mqtt.Client):

    def __init__(self):
        self.unicast = 1
        self.on_connect = self.on_connect_
        self.on_message = self.on_message_

    def on_connect_(self, userdata, flags, rc):
        self.subscribe(SETTING["DEVICE_RECEIVER_CHANNEL"])
    
    def on_message_(self, userdata, msg):
        MSG = json.loads(str(msg.payload))    
        if SETTING["FLASHOR"] == MSG["by_user"]:
            self.unicast = MSG["address"]

    def wait_unicast(self, mac, retries_max = 3):
        self.unicast = 0
        retries = 0

        req = {
            "by_user": SETTING["FLASHOR"],
            "mac": mac,
            "elements": SETTING["DEV_ELEMENTS"],
            "device_type": SETTING["DEV_TYPE"],
            "net_id": SETTING["NET_ID"]
        }
        print "WAITTING UNICAST...."
        while (self.unicast <= 0 or self.unicast >= 0x7FFF):
            time.sleep(2)
            retries += 1
            if retries > retries_max:
                print "NO RESPONSE FROM CLOUD, CHECK IT"
                break
            self.publish(SETTING["SEVER_RECEIVER_CHANNEL"], req)

        return self.unicast

unicastGetter = UnicastGetter()


def storage_save(data):
    fstorage = open("./device.txt", "a");
    fstorage.write(data)
    time.sleep(0.1)
    fstorage.close()


def jlink_init(jlink, retries, max):
    if retries > max:
        return

    try:
        jlink.open()
        # print("connecting to %s..." % target_device)
        jlink.set_tif(pylink.enums.JLinkInterfaces.SWD)
        jlink.connect(device_type)
        # print("connected, starting RTT...")
        jlink.rtt_start()
    except:
        print "Jlink init failed, retrying...(may u should re-plug)"
        time.sleep(1)
        jlink_init(jlink, retries + 1, max)


def jlink_reconnect(jlink, device_type):
    print "jlink_reconnecting..."

    while jlink.connected() == False:
        try:
            jlink.open()
            jlink.set_tif(pylink.enums.JLinkInterfaces.SWD)
            jlink.connect(device_type)
            jlink.rtt_start()
        except Exception:
            time.sleep(1)

    return True


def jlink_show_debug(en):
    if en:
        print "ENABLE DEBUG"
        SETTING["SHOW_JLINK_DEBUG"] = 1
    else :
        print "DISABLE DEBUG"
        SETTING["SHOW_JLINK_DEBUG"] = 0

def jlink_read_rtt(jlink):

    while True:
        try:
            if jlink.connected():
                terminal_bytes = jlink.rtt_read(0, 1024)
                if terminal_bytes:
                    msg = "".join(map(chr, terminal_bytes))
                    if SETTING["SHOW_JLINK_DEBUG"] == 1:
                        sys.stdout.write(msg)
                        sys.stdout.flush()
                time.sleep(0.1)
        except Exception:
            time.sleep(1)
            # print("IO read thread exception, exiting...")
            # thread.interrupt_main()
            # raise


def jlink_write_rtt(jlink):

    try:
        while jlink.connected():
            bytes = list(bytearray(input(), "utf-8") + b"\x0A\x00")
            bytes_written = jlink.rtt_write(0, bytes)
    except Exception:
        print("IO write thread exception, exiting...")
        thread.interrupt_main()
        raise

def read_dev_mac(jlink):
    
    global DEVICE_TYPE
    try:
        jlink_reconnect(jlink, DEVICE_TYPE)

        mac = jlink.memory_read8(0x100000A4, 6)

        mac[len(mac)-1] |= 0xC0 #static random rule

        smac = list()
        for i in mac:
            smac.append(format(i, '02x'))
        stringmac = ""
        for i in reversed(smac):
            stringmac += i + ":"

        stringmac = stringmac.upper()[:len(stringmac)-1]
        return stringmac

    except:
        print "read dev mac failed"

def jlink_erase_fash(jlink):

    print "ERASING FLASH...."
    jlink.erase()
    print "ERASED"

def jlink_flash_file(jlink, file, mac):

    if SETTING["UNICAST_SOURCE"] == "CLOUD":

        unicast = unicastGetter.wait_unicast(mac)

        if unicast == 0:
            return False

    else:
        unicast = HARD_UNICAST

    try:
        print "CHECKING CODE PATH..."
        hex = open(file, 'r')

    except:
        print "...INVALID, CHECK IT"
        return False

    if ERASE_BEFORE_LOAD:
        flash_addr = SETTING["UNICAST_ROM_ADDR"]
        print "UNICAST TO FLASH [0x{:08x}]".format(flash_addr)
        jlink.memory_write32(flash_addr, [unicast])
        runicast = jlink.memory_read16(flash_addr, 1)
        print "\tWRITE = ", unicast, " READ = ", runicast[0]

    hex_path = SETTING["HEX_PATH"]
    print "LOADING CODE ({})...".format(file)
    jlink.flash_file(file, 0) 
    print "DONE, RESETTING..."
    jlink.reset(0, False)


    store = MESH_DEVICE_TYPE[0:2] + "{:04d}_{:04x}_{}".format(0, unicast, mac)

    storage_save(str(store) + "\n")

    time.sleep(0.1)
    jlink_reconnect(jlink, device_type)

    return True


    
def program_process(jlink):

    global SETTING

    hex_path = SETTING["HEX_PATH"]

    success = False
    jlink_show_debug(False)

    jlink_init(jlink, 0, 1)
    if jlink.connected() == False:
        jlink_init(jlink, 0, 1)

    if jlink.connected():
        print "RESETTING...."
        jlink.reset(0, False)
        time.sleep(0.1)
        jlink.reset(0, False)

        print "\n\nNEW PROGRAM PROCESS"
        print "------------------------------------------"
        try:
            mac = read_dev_mac(jlink)
            
            print "-----------------------------------------"
            print "MAC ADDRESS:", mac
            print "-----------------------------------------"

            if SETTING["ERASE_BEFORE_LOAD"] == 1:
                jlink_erase_fash(jlink)

            print "RESETTING...."
            jlink.reset(0, False)
            time.sleep(0.1)

            success = jlink_flash_file(jlink, hex_path, mac)

        except:
            print "PROCESS ERROR, TRY AGAIN..."

        print "------------------------------------------"

    if success == False:
        # gui_msg("Failed, try again")
    else: 
        did_auto_inc()
    jlink_show_debug(True)

def main(device_type = "", device_id = "", load_file = "", erase=False):
    
    jlink_init(jlink, 0, 4)

    mqtt_init();

    while True:
        try:
            num_up = jlink.rtt_get_num_up_buffers()
            num_down = jlink.rtt_get_num_down_buffers()
            print("RTT started")
            print("----------------------------------------")
            break

        # except pylink.errors.JLinkRTTException:
        except:
            time.sleep(0.1)

    try:
        thread.start_new_thread(read_rtt, (jlink,))
        while(True):
            time.sleep(1)
            if jlink.connected() == False:
                jlink_reconnect(jlink, device_type)                
    except:
        raise
        
        


def print_usage():

    print "DUNGND2 PYLINK (usage):"
    print "\t./excutable [option...]"

    print "\t key command: (press when device is running)"
    print "\t\t ;: dis/enable key function"

    print "\t\t q: to quit, press y/n to confirm"
    print "\t\t r: reset device"
    print "\t\t p: do programing process (get mac, mqtt, load file...)"

    print "\t\t e: enable jlink debug (default)"
    print "\t\t d: disable jlink debug"

    print "\t\t w: erase all flash"

    print "\texample: ./executable"


if __name__ == "__main__":

    print_usage()

    # try:
        # os.system('xhost +')            
    sys.exit(main(device_type, device_id, load_file, erase))
    # except:
    #     print "KILL ITSEFT"
    #     os.system('kill %d' % os.getpid())