
import pylink
import sys
import time
import os
from builtins import input
from pynput import keyboard

import subprocess

try:
    import thread
except ImportError:
    import _thread as thread

import threading

import paho.mqtt.client as mqtt

import json

from Tkinter import *
import Tkinter, Tkconstants, tkFileDialog

FCONFIG = open("./config.txt", "r")
CONFIG  = json.loads(FCONFIG.read())


ERASE_BEFORE_LOAD = 1
DEVICE_ELEMENTS = 1
MESH_DEVICE_TYPE= ""
HARDCODE_UNICAST = 1
UNICAST_MODE = "HARDCODE"

UNICAST_ROM_ADDR = CONFIG["UNICAST_ROM_ADDR"]
BROKER = CONFIG["BROKER"]
DEVICE_RECEIVER_CHANNEL = CONFIG["DEVICE_RECEIVER_CHANNEL"]
SEVER_RECEIVER_CHANNEL = CONFIG["SEVER_RECEIVER_CHANNEL"]

PATH_CODE_HEX = CONFIG["PATH_CODE_HEX"]
FLASHOR = CONFIG["FLASHOR"]
NET_ID = CONFIG["NET_ID"]
CONFIG_DEVICES = CONFIG["MESH_DEVICES"]

UNICAST = 0
DEVICE_ID = 1

SHOW_DEBUG = True


KEY_PRESSED_LAST = ""
KEY_FUNCTION_ENABLE = True


jlink = pylink.JLink()

DEVICE_TYPE = ""

################################
# GUI GLOBAL VAR
################################
TKTHREAD = ""

GUIROOT = ""

GUIDID = ""
GUIDID_VAR = ""
GUIDEVICEID = ""
GUISTART = ""
GUIMSG = ""
GUIUNICASTOPTION = ""
GUIUNICAST = ""
GUIEBL = ""
GUICODEPATH = ""
GUIELEMENTS = ""
GUIDEVICE = ""
GUIDID_AUTO = ""

GUIEXIT = ""

def gui_exit():
    global GUIROOT
    GUIROOT.withdraw()

def show_setting():
    print "SETTING"
    print "\tERASE        ", ERASE_BEFORE_LOAD
    print "\tCODE         ", PATH_CODE_HEX
    print "\tDEVICE     ",   CONFIG_DEVICES.items()[0]
    print "\tDEVICEID     ", DEVICE_ID
    print "\tUNICSAT MODE ", UNICAST_MODE
    print "\tHARDCODE_U   ", HARDCODE_UNICAST
    

def gui_msg(msg):
    global GUIMSG
    GUIMSG.insert(END, msg)
    GUIMSG.insert(END, "\n")



def did_auto_inc():
    global DEVICE_ID
    global GUIDID, GUIDID_VAR
    global GUIDEVICEID
    global GUIDID_AUTO

    auto = int(GUIDID_AUTO.get())
    if auto:
        DEVICE_ID += 1
        GUIDID.set("NEXT DID: {:04d}".format(DEVICE_ID))
        GUIDID_VAR.set("{:04d}".format(DEVICE_ID))


def gui_device_id():
    global DEVICE_ID
    global GUIDEVICEID
    global GUIDID, GUIDID_VAR
    global GUIROOT

    DEVICE_ID  = int(GUIDEVICEID.get())
    GUIDID.set("NEXT DID: {:04d}".format(DEVICE_ID))
    GUIDID_VAR.set("{:04d}".format(DEVICE_ID))

def gui_unicast_option():
    global GUIUNICASTOPTION
    global UNICAST_MODE

    option = int(GUIUNICASTOPTION.get())

    if option != 1:
        print "UNICAST MODE HARDCODE"
        UNICAST_MODE = "HARDCODE"
    else:
        print "UNICAST MODE CLOUD"
        UNICAST_MODE = "CLOUD" 

def gui_unicast():
    global GUIUNICAST
    global HARDCODE_UNICAST

    HARDCODE_UNICAST = int(GUIUNICAST.get())
    print "HARCODE UNICAST 0x{:04x}".format(HARDCODE_UNICAST)

def gui_select_hex():
    global GUICODEPATH
    global PATH_CODE_HEX
    file = tkFileDialog.askopenfilename(initialdir = "./",title = "Select file",filetypes = (("hex files","*.hex"),("all files","*.*")))
    file = str(file)
    PATH_CODE_HEX = file

    if len(file) > 30:
        file = "..." + file[len(file)-30:len(file)]
    GUICODEPATH.set(file)
    
    print "PATH CODE HEX ", PATH_CODE_HEX

def gui_erase_before_load():
    global GUIEBL
    global ERASE_BEFORE_LOAD
    option = int(GUIEBL.get())
    if option == 1:
        ERASE_BEFORE_LOAD = 1
        print "ERASE BEFORE LOAD: YES"
    else:
        ERASE_BEFORE_LOAD = 0
        print "ERASE BEFORE LOAD: NO"

def gui_mesh_device(*args):
    global GUIELEMENTS
    global DEVICE_ELEMENTS
    global MESH_DEVICE_TYPE
    global GUIDEVICE

    device = str(GUIDEVICE.get())
    for dtype, elements in CONFIG_DEVICES.items():
        if str(dtype) == device:
            print (dtype, elements)
            DEVICE_ELEMENTS = int(elements)
            MESH_DEVICE_TYPE = str(dtype)
            GUIELEMENTS.set("ELEMENTS: " + str(elements))

    

def gui_start_process():
    print "Start process..."
    gui_msg("\nProcessing...")
    time.sleep(0.5)
    program_process()


def unicast_valid(unicast):

    if (unicast <= 0) or (unicast >= 0x7FFF):
        return False
    return True


def load_config():
    global UNICAST_ROM_ADDR
    global BROKER
    global DEVICE_RECEIVER_CHANNEL
    global SEVER_RECEIVER_CHANNEL
    global PATH_CODE_HEX
    global DEVICE_ELEMENTS
    global FLASHOR
    global MESH_DEVICE_TYPE
    global HARDCODE_UNICAST
    global NET_ID
    global CONFIG_DEVICES


    UNICAST_ROM_ADDR = int(UNICAST_ROM_ADDR)
    BROKER = str(BROKER)
    DEVICE_RECEIVER_CHANNEL = str(DEVICE_RECEIVER_CHANNEL)
    SEVER_RECEIVER_CHANNEL = str(SEVER_RECEIVER_CHANNEL)
    PATH_CODE_HEX = str(PATH_CODE_HEX)
    FLASHOR = str(FLASHOR)
    NET_ID = int(NET_ID)
    DEVICE_ELEMENTS = int(CONFIG_DEVICES.values()[0])
    MESH_DEVICE_TYPE = str(CONFIG_DEVICES.keys()[0])
    print "-----------------------------------------"
    print "CONFIG:", CONFIG
    print "-----------------------------------------"

def on_connect(client, userdata, flags, rc):
    # print("MQTT connected with result code "+str(rc))
    client.subscribe(DEVICE_RECEIVER_CHANNEL)


def on_message(client, userdata, msg):
    global UNICAST, FLASHOR
    MSG = json.loads(str(msg.payload))    
    if FLASHOR == MSG["by_user"]:
        UNICAST = MSG["address"]




client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

def mqtt_loop():
    global client
    while True:
        client.loop()

def mqtt_init():
    print "dung"
    # global client
    # client.connect(BROKER, 1883, 10)
    # thread.start_new_thread(mqtt_loop, ())

def mqtt_publish(data):
    global client
    # print "MQTT publishing:", data
    client.publish(SEVER_RECEIVER_CHANNEL, data)





def storage_save(data):
    fstorage = open("./device.txt", "a");
    fstorage.write(data)
    time.sleep(0.1)
    fstorage.close()


def jlink_init(jlink, retries, max):
    if retries > max:
        return

    try:
        jlink.open()
        # print("connecting to %s..." % target_device)
        jlink.set_tif(pylink.enums.JLinkInterfaces.SWD)
        jlink.connect(device_type)
        # print("connected, starting RTT...")
        jlink.rtt_start()
    except:
        print "Jlink init failed, retrying...(may u should re-plug)"
        time.sleep(1)
        jlink_init(jlink, retries + 1, max)


def reconnect(jlink, device_type):
    print "Reconnecting..."

    while jlink.connected() == False:
        try:
            jlink.open()
            jlink.set_tif(pylink.enums.JLinkInterfaces.SWD)
            jlink.connect(device_type)
            jlink.rtt_start()
        except Exception:
            time.sleep(1)

    return True


def debug_enable(en):
    global SHOW_DEBUG
    if en:
        print "ENABLE DEBUG"
        SHOW_DEBUG = True
    else :
        print "DISABLE DEBUG"
        SHOW_DEBUG = False

def read_rtt(jlink):

    global SHOW_DEBUG
    while True:
        try:
            if jlink.connected():
                terminal_bytes = jlink.rtt_read(0, 1024)
                if terminal_bytes:
                    msg = "".join(map(chr, terminal_bytes))
                    # unicast_share(jlink, msg)
                    if SHOW_DEBUG:
                        sys.stdout.write(msg)
                        sys.stdout.flush()
                time.sleep(0.1)
        except Exception:
            time.sleep(1)
            # print("IO read thread exception, exiting...")
            # thread.interrupt_main()
            # raise


def write_rtt(jlink):

    try:
        while jlink.connected():
            bytes = list(bytearray(input(), "utf-8") + b"\x0A\x00")
            bytes_written = jlink.rtt_write(0, bytes)
    except Exception:
        print("IO write thread exception, exiting...")
        thread.interrupt_main()
        raise

def get_mac(jlink):
    
    global DEVICE_TYPE
    try:
        reconnect(jlink, DEVICE_TYPE)

        mac = jlink.memory_read8(0x100000A4, 6)

        mac[len(mac)-1] |= 0xC0 #static random rule

        smac = list()
        for i in mac:
            smac.append(format(i, '02x'))
        stringmac = ""
        for i in reversed(smac):
            stringmac += i + ":"

        stringmac = stringmac.upper()[:len(stringmac)-1]
        return stringmac

    except:
        print "get mac error"

def erase_flash():
    global jlink
    print "ERASING FLASH...."
    jlink.erase()
    print "ERASED"

def program_file(jlink, file, mac):

    global UNICAST
    global UNICAST_ROM_ADDR
    global UNICAST_MODE
    global DEVICE_ID
    global ERASE_BEFORE_LOAD
    global DEVICE_ELEMENTS
    global NET_ID

    if file == "":
        return False

    success = True

    if UNICAST_MODE != "HARDCODE":

        waitted = 0

        UNICAST = 0
        print "WAITTING UNICAST...."

        msg = {
            "by_user": FLASHOR,
            "mac": mac,
            "elements": DEVICE_ELEMENTS,
            "device_type": MESH_DEVICE_TYPE,
            "net_id": NET_ID
        }

        msg = json.dumps(msg)

        mqtt_publish(msg)

        while unicast_valid(UNICAST) == False:
            time.sleep(2)
            waitted += 2
            if waitted > 10:
                print "NO RESPONSE FROM CLOUD, CHECK IT"
                success = False
                break
            mqtt_publish(msg)
    else:
        UNICAST = HARDCODE_UNICAST

    if success == False:
        return False;

    gui_msg("DEVICE_ID [{:04d}]".format(DEVICE_ID))
    gui_msg("MAC       [{}]".format(mac))
    gui_msg("UNICAST   [0x{:04x}]".format(UNICAST))

    try:
        print "CHECKING CODE PATH..."
        hex = open(PATH_CODE_HEX, 'r')

    except:
        print "...INVALID, CHECK IT"
        return False

    if ERASE_BEFORE_LOAD:
        print "UNICAST TO FLASH [0x{:08x}]".format(UNICAST_ROM_ADDR)
        jlink.memory_write32(UNICAST_ROM_ADDR, [UNICAST])
        runicast = jlink.memory_read16(UNICAST_ROM_ADDR, 1)
        print "\tWRITE = ", UNICAST, " READ = ", runicast[0]


    print "LOADING CODE ({})...".format(PATH_CODE_HEX)
    jlink.flash_file(file, 0) 
    print "DONE, RESETTING..."
    jlink.reset(0, False)


    store = MESH_DEVICE_TYPE[0:2] + "{:04d}_{:04x}_{}".format(DEVICE_ID, UNICAST, mac)

    storage_save(str(store) + "\n")

    time.sleep(0.1)
    reconnect(jlink, device_type)

    return True



class TkThread(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.start()

    def show(self):
        GUIROOT.deiconify()

    def exit(self):
        global GUIROOT
        GUIROOT.destroy()
        GUIROOT.quit()

    def run(self):
        global GUIROOT
        global GUIDEVICEID
        global GUISTART
        global GUIEXIT
        global GUIMSG
        global GUIDID
        global GUIUNICASTOPTION
        global GUIUNICAST 
        global GUICODEPATH
        global GUIELEMENTS
        global GUIEBL
        global GUIDEVICE
        global GUIDID_AUTO
        global GUIDID_VAR

        global PATH_CODE_HEX
        global CONFIG_DEVICES

        try:
            GUIROOT = Tk()
            
            ROW0 = Frame(GUIROOT, height=10) #exit gui
            ROW0.pack(side = BOTTOM)

            ROW1 = Frame(ROW0, height=10) # erase
            ROW1.pack(side = BOTTOM)

            ROW2 = Frame(ROW1, height=10) # code path
            ROW2.pack(side = BOTTOM)

            ROW3 = Frame(ROW2, height=10) # elements
            ROW3.pack(side = BOTTOM)

            ROW4 = Frame(ROW3, height=10) # unicast
            ROW4.pack(side = BOTTOM)

            ROW5 = Frame(ROW4, height=10) # device-id
            ROW5.pack(side = BOTTOM)

            ROW6 = Frame(ROW5, height=10) # start
            ROW6.pack(side = BOTTOM)

            ROW7 = Frame(ROW6, height=10) # msg
            ROW7.pack(side = BOTTOM)


            # EXIT
            GUIEXIT = Button(ROW0, text ="EXIT GUI", width=30, command = gui_exit)
            GUIEXIT.pack()

            # ERASE
            LEBL = Label(ROW1, text="ERASE BEFORE LOAD", width=30, font=("Courier", 10))
            LEBL.pack(side = LEFT)
            GUIEBL = IntVar()
            REBL1 = Radiobutton(ROW1, text="YES", variable=GUIEBL, value=1,
                              command=gui_erase_before_load)
            REBL1.pack( side = LEFT )

            REBL2 = Radiobutton(ROW1, text="NO", variable=GUIEBL, value=2,
                              command=gui_erase_before_load)
            REBL2.pack( side = LEFT )

            REBL1.select()

            #SELECT CODE
            BSC = Button(ROW2, text ="SELECT HEX", width=25, command = gui_select_hex)
            BSC.pack(side = LEFT)
            GUICODEPATH = StringVar()

            code =  PATH_CODE_HEX
            if len(code) > 30:
                code = "..." + code[len(code)-30:len(code)]
            GUICODEPATH.set(code)
            
            LSC = Label(ROW2, textvariable=GUICODEPATH)
            LSC.pack( side = LEFT)

            # ELEMENTS
            LEL = Label(ROW3, text="DEVICE", width=30, font=("Courier", 10))
            LEL.pack(side = LEFT)
            CHOICES = CONFIG_DEVICES
            GUIDEVICE = StringVar()
            GUIDEVICE.set(str(CHOICES.keys()[0]))
            popupMenu = OptionMenu(ROW3, GUIDEVICE, *CHOICES)
            popupMenu.pack(side = LEFT)
            GUIELEMENTS = StringVar()
            GUIELEMENTS.set("ELEMENTS: " + str(DEVICE_ELEMENTS))
            LELV = Label(ROW3, textvariable=GUIELEMENTS)
            LELV.pack(side = LEFT)
            GUIDEVICE.trace('w', gui_mesh_device)
        

            # UNICAST
            LUO = Label(ROW4, text="NODE UNICAST", width=30, font=("Courier", 10))
            LUO.pack(side = LEFT)
            GUIUNICASTOPTION = IntVar()
            R1 = Radiobutton(ROW4, text="FROM CLOUD", variable=GUIUNICASTOPTION, value=1,
                              command=gui_unicast_option)
            R1.pack( side = LEFT )

            R2 = Radiobutton(ROW4, text="HARD CODE", variable=GUIUNICASTOPTION, value=2,
                              command=gui_unicast_option)
            R2.pack( side = LEFT )
            R2.select()

            GUIUNICAST = Spinbox(ROW4, from_=1, to=32765, command = gui_unicast, wrap=True, width=6,
                font=("Courier", 15)) 
            GUIUNICAST.pack(side =  LEFT)


            #DEVICE ID
            GUIDID = StringVar()
            GUIDID.set("NEXT DID: {:04d}".format(DEVICE_ID))
            GUILDID = Label(ROW5, textvariable=GUIDID, width=20, font=("Courier", 15))
            GUILDID.pack(side = LEFT)
            GUIDID_VAR = StringVar()
            GUIDEVICEID = Spinbox(ROW5, from_=1, to=10000, command = gui_device_id, wrap=True, width=6,
                font=("Courier", 30), textvariable=GUIDID_VAR)
            GUIDID_VAR.set("{:04d}".format(DEVICE_ID))
            GUIDEVICEID.pack(side = LEFT)
            GUIDID_AUTO = IntVar()
            GUIDID_AUTO.set(1)
            CHECKBOX_DID = Checkbutton(ROW5, text="AUTO_INCREMENT", variable=GUIDID_AUTO)
            CHECKBOX_DID.pack(side = LEFT)


            # START
            GUISTART = Button(ROW6, text ="START", width=20, command = gui_start_process, font=("Courier", 15))
            GUISTART.pack(side = LEFT)

            #MSG
            GUIMSG = Text(ROW7)
            GUIMSG.pack(side = LEFT)

            GUIROOT.withdraw()
            GUIROOT.mainloop()

        except:
            raise
            self.exit()

           
TKTHREAD = TkThread()

def gui_program_start():
    global TKTHREAD
    TKTHREAD.show()
    show_setting()
    
    
def program_process():

    global PATH_CODE_HEX
    global ERASE_BEFORE_LOAD
    global jlink

    success = False

    debug_enable(False)

    jlink_init(jlink, 0, 1)
    if jlink.connected() == False:
        jlink_init(jlink, 0, 1)

    if jlink.connected():
        print "RESETTING...."
        jlink.reset(0, False)
        time.sleep(0.1)
        jlink.reset(0, False)

        print "\n\nNEW PROGRAM PROCESS"
        print "------------------------------------------"
        try:
            mac = get_mac(jlink)
            
            print "-----------------------------------------"
            print "MAC ADDRESS:", mac
            print "-----------------------------------------"

            
            if ERASE_BEFORE_LOAD:
                erase_flash()

            print "RESETTING...."
            jlink.reset(0, False)
            time.sleep(0.1)

            success = program_file(jlink, PATH_CODE_HEX, mac)

        except:
            print "PROCESS ERROR, TRY AGAIN..."

        print "------------------------------------------"

    if success == False:
        gui_msg("Failed, try again")
    else: 
        did_auto_inc()
    debug_enable(True)

########################################
# Catching keyboard
########################################

def key_on_press(key):
    global KEY_PRESSED_LAST
    global KEY_FUNCTION_ENABLE

    try:
        if key.char == ';':
            if KEY_FUNCTION_ENABLE:
                print "DISABLE KEY"
                KEY_FUNCTION_ENABLE = False
            else:
                print "ENABLE KEY" 
                KEY_FUNCTION_ENABLE = True

        elif KEY_FUNCTION_ENABLE == False:
            return

        elif key.char == 'r':
            global jlink
            if jlink.connected():
                print "RESETTING..."
                jlink.reset(0, False)
        
        elif key.char == 'p':
            gui_program_start()
            # program_process()

        elif key.char == 'e':
            debug_enable(True)

        elif key.char == 'd':
            debug_enable(False)

        elif key.char == 'w':
            erase_flash()

        elif key.char == 'q':
            os.system('kill %d' % os.getpid())

        elif key.char == 'y':
            if KEY_PRESSED_LAST == 'q':
                os.system('kill %d' % os.getpid())

        KEY_PRESSED_LAST = key.char

    except AttributeError:
        time.sleep(0.1)

def key_on_release(key):
    if key == keyboard.Key.esc:
        # Stop listener
        return False

# Collect events until released
def key_init():
    with keyboard.Listener(
            on_press=key_on_press,
            on_release=key_on_release) as listener:
        listener.join()


def main(device_type = "", device_id = "", load_file = "", erase=False):
    
    global jlink
    global PATH_CODE_HEX, DEVICE_TYPE, MESH_DEVICE_TYPE

    DEVICE_TYPE = device_type

    load_config()
    
    jlink_init(jlink, 0, 4)

    mqtt_init();

    thread.start_new_thread(key_init, ())   

    print "-----------------------------------------"
    print "device type: ", MESH_DEVICE_TYPE
    print "device id:", device_id
    print "code hex:", PATH_CODE_HEX
    print "-----------------------------------------"

    while reconnect(jlink, device_type) == False:
        time.sleep(1)


    mac = get_mac(jlink)

    print "-----------------------------------------"
    print "MAC ADDRESS:", mac
    print "-----------------------------------------"
    
    
    while True:
        try:
            num_up = jlink.rtt_get_num_up_buffers()
            num_down = jlink.rtt_get_num_down_buffers()
            print("RTT started")
            print("----------------------------------------")
            break

        # except pylink.errors.JLinkRTTException:
        except:
            time.sleep(0.1)

    try:
        thread.start_new_thread(read_rtt, (jlink,))
        # thread.start_new_thread(write_rtt, (jlink,))
        while(True):
            time.sleep(1)

            if jlink.connected() == False:
                reconnect(jlink, device_type)                
    
    except:
        raise
        
        


def print_usage():

    print "DUNGND2 PYLINK (usage):"
    print "\t./excutable [option...]"

    print "\t key command: (press when device is running)"
    print "\t\t ;: dis/enable key function"

    print "\t\t q: to quit, press y/n to confirm"
    print "\t\t r: reset device"
    print "\t\t p: do programing process (get mac, mqtt, load file...)"

    print "\t\t e: enable jlink debug (default)"
    print "\t\t d: disable jlink debug"

    print "\t\t w: erase all flash"

    
    print "\texample: ./executable"


if __name__ == "__main__":

    print_usage()

    # print "argument list:"
    device_id = ""
    load_file = ""
    device_type = "NRF52832_XXAA"
    erase = False

    for arg in sys.argv:
        # print arg
        if arg.find("-i=") != -1:
            device_id = arg[3:]

        if arg.find("-f=") != -1:
            load_file = arg[3:]

        if arg.find("-e") != -1:
            erase = True

        if arg.find("-fe=") != -1:
            erase = True
            load_file = arg[4:]

        if arg.find("-d=") != -1:
            device_type = arg[3:]
            if(device_type == "51"):
                device_type = "NRF1822_XXAC"
            else:
                device_type = "NRF52832_XXAA"

    # try:
        # os.system('xhost +')            
    sys.exit(main(device_type, device_id, load_file, erase))
    # except:
    #     print "KILL ITSEFT"
    #     os.system('kill %d' % os.getpid())