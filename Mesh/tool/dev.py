# import asyncio
# import websockets
try:
    import thread
except ImportError:
    import _thread as thread

import time
import socket

import os

import json 

import random

import subprocess
import shlex

# import paho.mqtt.client as mqtt

from simple_websocket_server import WebSocketServer, WebSocket


from _http import HttpCli, SimpleHttpServer



MESH_SERVER_ADDRESS = '/tmp/us_xfr'
BACNET_SERVER_ADDRESS = '/tmp/bacnet'

IPC_MESH_SEND = 0xAA
IPC_MESH_HACK = 0xBB
IPC_MESH_REMOTE_LOG_CLR = 0xEE
IPC_MESH_NETKEY_SET = 0xAB
IPC_MESH_FILTER_SET = 0xAC

GROUP_STATUS_CONTROL = 0xD000
GROUP_VNG_LIGHT_HUB = 0xC001
GROUP_VNG_COMMAND = 0xC002
GROUP_VNG_THREAD_TRACKING = 0xC003
OPCODE_VNG_STATUS_CONTROL = 0x05FF
OPCODE_VNG_DOOR_SET = 0x8202
OPCODE_VNG_COMMAND_OTA = 0x06FF
OPCODE_VNG_COMMAND_RENEW = 0x08FF
OPCODE_VNG_COMMAND_RELAY = 0x09FF
OPCODE_VNG_COMMAND_TTL = 0x0AFF
OPCODE_VNG_COMMAND_NETKEY = 0x0CFF
OPCODE_VNG_COMMAND_THREAD = 0x0EFF

OPCODE_VNG_COMMAND_GROUPS_COUNT = 0x11FF
OPCODE_VNG_COMMAND_GROUPS_LIST = 0x12FF

OPCODE_LEVEL_LINE_GET       = 0x8205
OPCODE_LEVEL_LINE_SET_ACK   = 0x8206
OPCODE_LEVELS_HUB_SET_ACK   = 0x9006
OPCODE_LEVELS_GROUP_ENA_ACK = 0x9007
OPCODE_LEVELS_GROUP_ADD_ACK = 0x9008
OPCODE_LEVELS_GROUP_DEL_ACK = 0x9009
OPCODE_LEVELS_GROUP_CLR_ACK = 0x900A
OPCODE_LEVELS_STATUS_CONFIG = 0x900B
OPCODE_LEVELS_STATUS_STORE  = 0x900C


OPCODE_LEVELS_HUB_SET       = 0x9000
OPCODE_LEVEL_LINE_SET       = 0x8207
OPCODE_LEVELS_GROUP_ENA     = 0x9001
OPCODE_LEVELS_GROUP_ADD     = 0x9002
OPCODE_LEVELS_GROUP_DEL     = 0x9003
OPCODE_LEVELS_GROUP_CLR     = 0x9004

OTA_TIMEOUT_DEFAULT = 60

BAC_OBJ_TYPE_ANALOG_OUTPUT = 1
BAC_OBJ_TYPE_BINARY_OUTPUT = 4


#######
#
# mesh  : {"type": "mesh", "device": "light", "address": 10, "level": 100}
#
# bacnet: {"type": "bacnet", "device": "light", "instance": 10, "level": 100}
#
#
#######

command_list = list({})
command_mesh_list = list({})
command_bacnet_list = list({})


CHOOSE_HUB_LOG = 1
#from meshtool log
unicast_list = list()
hub_list = list() 
other_list = list()

mesh_status_want_send = False
mesh_status_interval  = 5

class CloudCli:
	def __init__(self):
		# self.cli = HttpCli("vng.icbm.vn")
		self.db = []
		with open("../map/db/_lighthub.json", "r") as df:
			db = json.loads(df.read()) 
			self.db = db["lighthub"]

	def find_light(self, unicast):
		for light in self.db:
			if light["dec"] == unicast:
				return (light["db_id"], light["mac"], light["area_id"])
		return (1,1,1)

	def send_ota(self, unicast, timeout, mac):
		path = "https://vng.icbm.vn/iot/be/api/mesh/node?cm=update_node_ota&dt="
		
		data = {"node_info":{"id":1648,"address":1,"elements":8,"features":1,"iv":0,"key":"","mac":"C9:D0:07:D4:75:DD","net":4,"oob":1,"relay":1,"ota":10,"success_sound":0,"success_repeat":0,"fail_sound":0,"fail_repeat":0,"device_type":"light_hub","device_id":0,"unicast_id":0,"area_id":0,"$$hashKey":"object:28583"}}
		light = self.find_light(unicast)
		data["node_info"]["address"] = unicast
		data["node_info"]["id"]      = light[0] 
		data["node_info"]["mac"]     = light[1]
		data["node_info"]["ota"]     = timeout
		

		jdata = str(json.dumps(data, separators=(',', ':')))
		path = path + jdata
		# print path
		try:
			self.cli = HttpCli("vng.icbm.vn")
			response = "res:" + str(self.cli.GET(path))
			ws_send(str(response))
		except:
			ws_send("CLOUD OTA: GET FAILED")
			print "GET FAILED"
	
	def send_relay(self, unicast, relay):

		path = "https://vng.icbm.vn/iot/be/api/mesh/node?cm=update_node_relay&dt="
		
		data = {"node_info":{"id":29,"address":32752,"elements":8,"features":1,"iv":0,"mac":"F6:B9:83:A2:A2:9A","net":4,"oob":1,"relay":1,"ota":0,"device_type":"light_hub","area_id":0}}
		
		data["node_info"]["address"] = unicast
		data["node_info"]["relay"]   = relay

		jdata = str(json.dumps(data, separators=(',', ':')))
		path = path + jdata
		# print path
		try:
			self.cli = HttpCli("vng.icbm.vn")
			self.cli.GET(path)
			response = "res:" + str(self.cli.GET(path))
			ws_send(response)
		except:
			ws_send("CLOUD RELAY: GET FAILED")
			print "GET FAILED"

	def send_dim(self, unicast, level):

		path = "https://vng.icbm.vn/cloud/api/v2/control?cm=dim_light_hubs&office=campus&dt="
		data = {"unicast_ids":[24028],"area_id":144,"brightness":60,"by_user":"dathn"}
		
		light = self.find_light(unicast)
		data["unicast_ids"][0] = unicast
		data["brightness"]     = level 
		data["area_id"]        = light[2]

		jdata = str(json.dumps(data, separators=(',', ':')))
		path = path + jdata

		try:
			self.cli = HttpCli("vng.icbm.vn")
			self.cli.GET(path)
			response = "res:" + str(self.cli.GET(path))
			ws_send(response)
		except:
			ws_send("CLOUD DIM: GET FAILED")
			print "GET FAILED"

cloudCli = CloudCli()
class MeshTid:
	def __init__(self):
		self.tid = 0

	def get(self):
		tid = self.tid
		self.tid += 1
		return tid

	def set(self, tid):
		self.tid = tid
		return 0

Tid = MeshTid()

class MeshDevice:
	VNG_LIGHT_HUB = 0
	VNG_DOOR =   1
	VNG_READER = 2
	VNG_LIGHT_ONOFF = 3
	VNG_ANY = 4

	def __init__(self, type, id, unicast, mac):
		self._type = type
		self._id = id
		self._unicast = unicast
		self._mac = mac


	@staticmethod
	def string_to_mac(mac):
		_mac = list()
		_mac.append("0x" + mac[0:2])
		_mac.append("0x" + mac[3:5])
		_mac.append("0x" + mac[6:8])
		_mac.append("0x" + mac[9:11])
		_mac.append("0x" + mac[12:14])
		_mac.append("0x" + mac[15:17])

		_t_mac = list()

		for i in _mac:
			_t_mac.append(int(i, 16))

		return _t_mac


	def type(self):
		return self._type
	def id(self):
		return self._id
	def unicast(self):
		return self._unicast
	def mac(self):
		return self._mac

	def string_mac(self):
		mac = ""
		for i in self._mac:
			mac += "{:02X}:".format(i)
		return mac


	def tuple(self):
		return (self._type, self._id, self._unicast, self._mac)

	def show(self, prefix = '\t'):

		print prefix, " (type {:d}, id {:04d}, unicast {:04X}, mac {})".format(self._type, self._id, self._unicast, self.string_mac())

class MeshDevices:

	def __init__(self):
		self.devices = list()
		self.log_devices = list()

		self.logsrc = "./zephyr/bin/remote.txt"
		self.logdir = "log"
		subprocess.call(["mkdir", "-p", self.logdir])
		self.logdest = self.logdir + "/devices.txt"
		self.last_stored = list()

		self.excludes = list() #list of exclude hub.unicast
		self.excludes_path = "exclude"
		self.exclude_idx = len("type 0, id 0228, unicast ") + 1
		self.exclude_len = 4 #0010
		subprocess.call(["mkdir", "-p", self.excludes_path])

		self.dims = list()
		self.dims_path = "dim"
		subprocess.call(["mkdir", "-p", self.dims_path])

		self.database_path = "database"

		self.restore_excludes()
		self.restore_log()
		self.restore_database()
		self.restore_dim()

		thread.start_new_thread(MeshDevices.log_sync, (self, 5))


	def restore_database(self):
		print "LIGHT {}".format(MeshDevice.VNG_LIGHT_HUB)
		print "DOOR {}".format(MeshDevice.VNG_DOOR)
		print "READER {}".format(MeshDevice.VNG_READER)
		for r, d, f in os.walk(self.database_path):
			for file in f:
				
				file_path = os.path.join(r, file)
				with open(file_path) as fp:
					for cnt, line in enumerate(fp):
						if len(line) < 20:
							continue
						
						devtype = 10
						if file == "light":
							# print "restore light"
							devtype = MeshDevice.VNG_LIGHT_HUB
						elif file == "door":
							# print "restore door"
							devtype = MeshDevice.VNG_DOOR
						elif file == "reader":
							# print "restore reader"
							devtype = MeshDevice.VNG_READER
						else:
							devtype == 10

						# print "devtype {}".format(devtype)
						linelen = len(line)
						_id = str(line[0:4])  
						_unicast = str(line[5:11])
						_mac = str(line[12:linelen-1])

						id = int(_id, 10)
						unicast = int(_unicast, 16)
						mac = MeshDevice.string_to_mac(_mac)
						
						device = MeshDevice(devtype, id, unicast, mac)
		
						self.devices.append(device)
			

	def _store_file_from_type(self, type):
		if type == MeshDevice.VNG_LIGHT_HUB:
			return self.logdir + "/hubs.txt"
		elif type == MeshDevice.VNG_READER:
			return self.logdir + "/readers.txt"
		elif type == MeshDevice.VNG_DOOR:
			return self.logdir + "/doors.txt"
		else:
			return self.logdir + "/others.txt"

	def restore_log(self):
		try:
			with open(self.logdest) as fp:
				for cnt, line in enumerate(fp):
					ts = eval(line)
					dev = MeshDevice(ts[0], ts[1], ts[2], ts[3])
					if dev.unicast() in self.excludes:
						continue
					self.log_devices.append(dev)
		except:
			print "restore log devices failed"

	def restore_excludes(self):
		self.excludes = list()
		for r, d, f in os.walk(self.excludes_path):
			for file in f:
				file_path = os.path.join(r, file)
				with open(file_path) as fp:
					for cnt, line in enumerate(fp):
						if len(line) < 20:
							continue
						
						str_unicast = "0x" + line[self.exclude_idx : self.exclude_idx + self.exclude_len]
						unicast = int(str_unicast, 16)

						if unicast not in self.excludes:
							self.excludes.append(unicast)
		
	
	def restore_dim(self):
		self.dims = list()
		for r, d, f in os.walk(self.dims_path):
			for file in f:
				file_path = os.path.join(r, file)
				with open(file_path) as fp:
					for cnt, line in enumerate(fp):
						if len(line) < 20:
							continue
						
						str_unicast = "0x" + line[self.exclude_idx : self.exclude_idx + self.exclude_len]
						unicast = int(str_unicast, 16)

						if unicast not in self.dims:
							self.dims.append(unicast)

				

	def _file_clear(self, file):
		_file = open(file, "w")
		_file.close()

	def _file_append(self, file, data):
		_file = open(file, "a")
		_file.write(data)
		_file.close()

	def log_device_clr(self):

		#clear log cache
		self.log_devices = list()
		self.last_stored = [MeshDevice(MeshDevice.VNG_ANY, 0, 0, "")]
		#clear all log files
		self._file_clear(self.logdest)
		self._file_clear(self._store_file_from_type(MeshDevice.VNG_ANY))
		self._file_clear(self._store_file_from_type(MeshDevice.VNG_READER))
		self._file_clear(self._store_file_from_type(MeshDevice.VNG_DOOR))
		self._file_clear(self._store_file_from_type(MeshDevice.VNG_LIGHT_HUB))

		#clear mesh log
		mesh_remote_log_clr()


	def same_list(self, l1, l2):
		if len(l1) != len(l2):
			return False
		for i in l1:
			if i not in l2:
				return False
		return True

	def store_log(self):

		if self.same_list(self.last_stored, self.log_devices):
			# print "Same devices, do not store"
			return

		self.last_stored = list(self.log_devices)

		self._file_clear(self.logdest)
		self._file_clear(self._store_file_from_type(MeshDevice.VNG_ANY))
		self._file_clear(self._store_file_from_type(MeshDevice.VNG_READER))
		self._file_clear(self._store_file_from_type(MeshDevice.VNG_DOOR))
		self._file_clear(self._store_file_from_type(MeshDevice.VNG_LIGHT_HUB))

		for dev in self.log_devices:

			#format for restoring (devices.txt)
			store =  "{}\n".format(dev.tuple())
			self._file_append(self.logdest, store)

			#format for human readable (hubs.txt, others.txt)
			store_p = "(type {:d}, id {:04d}, unicast {:04X}, mac {})\n".format(dev.type(), dev.id(), dev.unicast(), dev.string_mac())
			self._file_append(self._store_file_from_type(dev.type()), store_p)



	def log_device_add(self, unicast):

		insert_idx = -1

		if unicast in self.excludes:
			return

		dev = self.find_with_unicast(unicast)
		if dev is None:
			dev = MeshDevice(MeshDevice.VNG_ANY, 0, unicast, "")

		for idx, d in enumerate(self.log_devices):
			if d.unicast() == dev.unicast():
				return
			if dev.unicast() < d.unicast():
				insert_idx = idx

		if insert_idx == -1:
			self.log_devices.append(dev)
		else:
			self.log_devices.insert(insert_idx, dev)

	def log_sync(self, interval):
		while True:
			# print "Log syn-ing..."
			with open(self.logsrc) as fp:  
				for cnt, line in enumerate(fp):
					unicast = int(line[0:len(line)-1], 16)
					# print "syn, addr {:04X}".format(unicast)
					self.log_device_add(unicast)
			
			self.store_log()
			time.sleep(interval)
				


	def find_with_id(self, type, id):
		for d in self.devices:
			if d.id() == id and type == d.type():
				return d

	def find_with_unicast(self, unicast):
		for d in self.devices:
			if d.unicast() == unicast:
				return d

	def find_with_mac(self, mac):
		for d in self.devices:
			if d.mac() == mac:
				return d

	def show_all(self, type = MeshDevice.VNG_ANY):
		print "All devices:"
		for d in self.log_devices:
			if type == MeshDevice.VNG_ANY or type == d.type():
				d.show()

	def logdevices(self, type):
		devices = list()
		for d in self.log_devices:
			if d.type() == type:
				devices.append(d)
		return devices

	def devices_get(self, type):
		devices = list()
		for d in self.devices:
			if d.type() == type:
				devices.append(d)
		return devices
	
	def dimdevices(self):
		return self.dims

DEVICES = MeshDevices()

def socket_send(msg, srv):

	sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
	try:
		sock.connect(srv)
	except:
		error = "socket failed->(run python with sudo and turn on meshtool)"
		print error
		ws_send(error)
		return -1
	sock.sendall(msg)
	sock.close()

	return 0



def socket_bacnet_send(msg):
	return socket_send(msg, BACNET_SERVER_ADDRESS)

def bacnet_subprocess_send(type, instance, level):

	command = "/home/dung/Documents/GIT_REPO/bacnet-stack-0.8.6/bin/bacwp"

	_type = "{:d}".format(type)
	_instance = "{:d}".format(instance)
	_property = "85"
	_level = "{:d}".format(level)
	_index = "-1" #dont care
	_priority = "0"
	_tag = "4"
	if(type == 1): #analog output
		_tag = "4" #REAL
	else: #binary output
		_tag = "9" #ENUMERATED


	subprocess.call([command, "1240", _type, _instance, _property, _priority, _index, _tag, _level], shell=False)

def socket_mesh_send(msg):
	return socket_send(msg, MESH_SERVER_ADDRESS)


def bacnet_write(obj_type, obj_instance, obj_val):
	message = bytearray(1 + 2 + 2)
	message[0] = obj_type
	message[1] = (obj_instance >> 8) & 0xFF
	message[2] = obj_instance & 0xFF
	message[3] = (obj_val >> 8) & 0xFF
	message[4] = obj_val & 0xFF
	error = socket_bacnet_send(message)
	return error



def mesh_hack(show):
	message = bytearray(2)
	message[0] = IPC_MESH_HACK
	message[1] = show
	socket_mesh_send(message)


def mesh_send(target, opcode, data):
	data_len = len(data)
	message = bytearray(1 + 2 + 2 + 2 + data_len)
	message[0] = IPC_MESH_SEND
	message[1] = (target & 0xFF00) >> 8
	message[2] = target & 0x00FF
	message[3] = (opcode & 0xFF00) >> 8
	message[4] = opcode & 0x00FF
	message[5] = (data_len & 0xFF00) >> 8
	message[6] = data_len & 0x00FF
	message[7:] = data
	socket_mesh_send(message)


def mesh_remote_log_clr():
	message = bytearray(1 + 1)
	message[0] = IPC_MESH_REMOTE_LOG_CLR
	message[1] = 1 #any
	socket_mesh_send(message)

def mesh_netkey_set(netidx):
	message = bytearray(1 + 1)
	message[0] = IPC_MESH_NETKEY_SET
	message[1] = netidx
	socket_mesh_send(message)

def mesh_filter_set(filter):
	_filter = bytearray(filter, 'utf8')
	message = bytearray(1 + len(_filter))
	message[0] = IPC_MESH_FILTER_SET
	idx = 1
	for c in _filter:
		message[idx] = c
		idx = idx + 1

	print "filter : len {:d}".format(len(message))
	socket_mesh_send(message)

def levels_hub_send(hub_unicast, level, ack = False, cloud = False):
	level = int(int(0xFFFF)*level/100)
	tid = Tid.get() 
	time = 0
	delay = 0

	data = bytearray(7)
	data[0] = (level & 0xFF00) >> 8
	data[1] = level & 0x00FF
	data[2] = tid & 0x00FF
	data[3] = time
	data[4] = delay
	data[5] = (tid & 0xFF00) >> 8
	data[6] = tid & 0x00FF
	
	if cloud == False:
		print "dim hub {}, level {} tid {}, ack {}".format(hub_unicast, level, tid, ack)
		if ack == False:
			mesh_send(hub_unicast, OPCODE_LEVELS_HUB_SET, data)
		else:
			mesh_send(hub_unicast, OPCODE_LEVELS_HUB_SET_ACK, data)
	else:
		cloudCli.send_dim(hub_unicast, level)
		
def set_bit(byte, bit):
	
	byte |= 1 << bit
	return byte

def thread_tracking_set(hub_unicast, reset, notify):
	tid = Tid.get() 
	data = bytearray(4)
	data[0] = reset 
	data[1] = notify
	data[2] = (tid & 0xFF00) >> 8
	data[3] = tid & 0x00FF
	print "thread tracking set {:04X}, reset {} notify {} tid {}".format(hub_unicast, reset, notify, tid)
	mesh_send(hub_unicast, OPCODE_VNG_COMMAND_THREAD, data)

def hub_groups_count_get(addr, index, main):
	tid = Tid.get() 
	data = bytearray(6)
	data[0] = (index >> 8) & 0xFF 
	data[1] = (index) & 0xFF 
	data[2] = (main >> 8) & 0xFF 
	data[3] = (main) & 0xFF 
	data[4] = (tid >> 8) & 0xFF 
	data[5] = (tid) & 0xFF 
	print "hub groups count {:04X}, index {} main {} tid {}".format(addr, index, main, tid)
	mesh_send(addr, OPCODE_VNG_COMMAND_GROUPS_COUNT, data)


def hub_groups_list_get(addr, index, main, line, gbits = 8):
	tid = Tid.get() 
	data = bytearray(8)
	data[0] = (index >> 8) & 0xFF 
	data[1] = (index) & 0xFF 
	data[2] = (main >> 8) & 0xFF 
	data[3] = (main) & 0xFF 
	data[4] = line
	data[5] = gbits
	data[6] = (tid >> 8) & 0xFF 
	data[7] = (tid) & 0xFF 
	print "hub groups list {:04X}, index {} main {} line {} gbits {} tid {}".format(addr, index, main, line, gbits, tid)
	mesh_send(addr, OPCODE_VNG_COMMAND_GROUPS_LIST, data)

def levels_hub_enableds(hub_unicast, enableds, ack = False):
	tid = Tid.get()
	data = bytearray(6)
	data[0] = 0
	data[1] = 0
	data[2] = 0
	data[3] = 0
	data[4] = (tid >> 8 ) & 0xFF
	data[5] = (tid ) & 0xFF

	for line, enabled in enumerate(enableds):
		if enabled:
			data[3] = set_bit(data[3], line) 
	print "enable hub {:04X}, data {:02X}".format(hub_unicast, data[3])

	if ack == False:
		mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_ENA, data)
	else:
		mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_ENA_ACK, data)



def level_line_get_send(hub_unicast, timeout):
	tid = Tid.get()
	data = range(4)
	data[0] = (timeout & 0xFF00) >> 8
	data[1] = timeout & 0x00FF
	data[2] = (tid & 0xFF00) >> 8
	data[3] = tid & 0x00FF

	print "get lines status: addr {:04X}, timeout {}, tid {}".format(hub_unicast, timeout, tid)
	mesh_send(hub_unicast , OPCODE_LEVEL_LINE_GET, data)

def level_line_send(hub_unicast, line, level, ack = False):

	level = (int(0xFFFF)*level/100)
	tid = Tid.get()
	time = 0
	delay = 0

	data = range(7)
	data[0] = (level & 0xFF00) >> 8
	data[1] = level & 0x00FF
	data[2] = tid & 0x00FF
	data[3] = time
	data[4] = delay
	data[5] = (tid & 0xFF00) >> 8
	data[6] = tid & 0x00FF

	print "hub {}, line {}, level {}, tid {}, ack {}".format(hub_unicast, line, level, tid, ack)
	if ack == False:
		mesh_send(hub_unicast + line, OPCODE_LEVEL_LINE_SET, data)
	else:
		mesh_send(hub_unicast + line, OPCODE_LEVEL_LINE_SET_ACK, data)

def status_control(idx, mod, duration, period = 0):

	data = bytearray(8)
	data[0] = (idx & 0xFF00) >> 8
	data[1] = (idx & 0x00FF)
	data[2] = (mod & 0xFF00) >> 8
	data[3] = (mod & 0x00FF)
	data[4] = (duration & 0xFF00) >> 8
	data[5] = (duration & 0x00FF)
	data[6] = (period >> 8) & 0x00FF
	data[7] = (period & 0x00FF)
	
	print "MESH STATUS CONTROL: idx {}, mod {}, duration {}".format(idx, mod, duration)
	mesh_send(GROUP_STATUS_CONTROL, OPCODE_VNG_STATUS_CONTROL, data)

def command_ota_send(unicast, timeout, mac):
	smac = ':'.join('{:02X}'.format(x) for x in mac)
	print "Command ota, timeout {:02d}, mac [{}]".format(timeout, smac)
	data = range(7)

	data[0] = (timeout & 0xFF00) >> 8
	data[1] = timeout & 0x00FF
	data[2:7] = mac

	mesh_send(unicast, OPCODE_VNG_COMMAND_OTA, data)

def command_ota_cloud_send(unicast, timeout, mac):
	cloudCli.send_ota(unicast, timeout, mac)
	


def command_relay_send(unicast, enable, retransmit = 3):
	print "Command relay set, unicast 0x{:04x}, enabled [{}]".format(unicast, enable)
	data = range(1)
	data[0] = enable
	# data[1] = retransmit
	mesh_send(unicast, OPCODE_VNG_COMMAND_RELAY, data)

def command_ttl_send(unicast, ttl):
	print "Command ttl set, unicast 0x{:04x}, ttl [{}]".format(unicast, ttl)
	data = range(1)
	data[0] = ttl
	mesh_send(unicast, OPCODE_VNG_COMMAND_TTL, data)


def mac_to_str(mac):
	strmac = ""
	for idx, hex in enumerate(mac):
		strmac += "{:02X}".format(hex)
		if idx < 5:
			strmac += ":"
	return strmac

def ota_and_store(unicast, mac = [], cloud=False):
	
	strmac = mac_to_str(mac)

	if cloud == False:
		
		command_ota_send(unicast, OTA_TIMEOUT_DEFAULT, mac)
		
	else:
		command_ota_cloud_send(unicast, OTA_TIMEOUT_DEFAULT, mac)

	otaed = open("otaed.txt", "a")
	
	store_p = "unicast {:04X}, mac {})\n".format(unicast, strmac)
	otaed.write(store_p)

def door_send(unicast):
	
	tid = random.randrange(255)
	data = range(6)
	data[0] = 1
	data[1] = 0
	data[2] = 3
	data[3] = tid
	data[4] = 0
	data[5] = 0
	mesh_send(unicast, OPCODE_VNG_DOOR_SET, data)
	print "Command door set, unicast 0x{:04x}, msg {}".format(unicast, data)

def turnstile(unicast):
	
	tid = random.randrange(255)
	data = range(6)
	data[0] = 1
	data[1] = 0
	data[2] = 6
	data[3] = tid
	data[4] = 0
	data[5] = 0
	mesh_send(unicast, OPCODE_VNG_DOOR_SET, data)
	print "Command door set, unicast 0x{:04x}, msg {}".format(unicast, data)

def command_netkey_send(unicast, idx):

    MESH_TID = Tid.get()
    print "Command netkey set, unicast 0x{:04x}, idx {} tid {}".format(unicast, idx, MESH_TID)

    data = range(4)

    data[0] = 0 
    data[1] = idx
    data[2] = (MESH_TID >> 8) & 0xFF
    data[3] = MESH_TID & 0xFF

    mesh_send(unicast, OPCODE_VNG_COMMAND_NETKEY, data)


def levels_group_add(hub_unicast, line, group, ack =  False):
	
	tid = Tid.get()
	data = range(5)
	data[0] = set_bit(0, line)
	data[1] = (group >> 8) & 0xFF
	data[2] = group & 0xFF
	data[3] = (tid >> 8) & 0xFF
	data[4] = tid & 0xFF

	
	print "add hub {:04X} line {:d}, group {:04X}, tid {:d}, ack {}".format(hub_unicast, line, group, tid, ack)
	if ack == False:
		mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_ADD, data)
	else:
		mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_ADD_ACK, data)

def levels_group_hub_add(hub_unicast, group, ack = False):
	for line in range(8):
		levels_group_add(hub_unicast, line, group, ack)
		time.sleep(0.5)


def levels_group_del(hub_unicast, line, group, ack = False):
	tid = Tid.get()
	data = range(5)
	data[0] = set_bit(0, line)
	data[1] = (group >> 8) & 0xFF
	data[2] = group & 0xFF
	data[3] = (tid >> 8) & 0xFF
	data[4] = tid & 0xFF

	print "del hub {:04X} line {:d}, group {:04X}, tid {:d}, ack {}".format(hub_unicast, line, group, tid, ack)
	if ack == False:
		mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_DEL, data)
	else:
		mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_DEL_ACK, data)

def levels_group_hub_del(hub_unicast, group, ack = False):
	for line in range(8):
		levels_group_del(hub_unicast, line, group, ack)
		time.sleep(0.5)

def levels_group_clr(hub_unicast, _line, ack = False):

	lines = [0,0,0,0,0,0,0,0]
	lines[_line] = 1

	tid = Tid.get()
	data = range(3)
	data[0] = 0

	for idx, line in enumerate(lines):
		if line != 0:
			data[0] = set_bit(data[0], idx)
	
	data[1] = (tid >> 8) & 0xFF
	data[2] = tid & 0xFF

	
	print "clr hub {:04X} line  {} ack {}".format(hub_unicast, line, ack)

	if ack == False:
		mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_CLR, data)
	else:
		mesh_send(hub_unicast, OPCODE_LEVELS_GROUP_CLR_ACK, data)

def levels_group_hub_clr(hub_unicast, ack = False):
	
	for i in range(8):
		levels_group_clr(hub_unicast, i, ack)
		time.sleep(0.5)


		
def hub_status_config(hub_unicast, status, warn, check_period, notify_period):

	tid = Tid.get()
	data = range(7) 
	data[0] = 0
	if status:
		data[0] = set_bit(data[0], 0)
	if warn:
		data[0] = set_bit(data[0], 1)
	data[1] = (check_period >> 8) & 0xFF
	data[2] = check_period & 0xFF
	data[3] = (notify_period >> 8) & 0xFF
	data[4] = notify_period & 0xFF

	data[5] = (tid >> 8) & 0xFF
	data[6] = tid & 0xFF

	
	print "status config addr {:04X} status_en {}, warn_en {}, check {}(s), notify{}(s) tid {}".format(hub_unicast, status, warn, check_period, notify_period, tid)

	mesh_send(hub_unicast, OPCODE_LEVELS_STATUS_CONFIG, data)


def hub_status_store(hub_unicast):

	tid = Tid.get()
	data = range(3) 
	data[0] = 1
	data[1] = (tid >> 8) & 0xFF
	data[2] = tid & 0xFF

	print "status store addr {:04X} tid {}".format(hub_unicast, tid)

	mesh_send(hub_unicast, OPCODE_LEVELS_STATUS_STORE, data)

def door_send_new(unicast, tid):
	data = range(6)
	data[0] = 1
	data[1] = 0
	data[2] = 3
	data[3] = tid >> 8
	data[4] = tid & 0xFF
	data[5] = 1
	mesh_send(unicast, OPCODE_VNG_DOOR_SET, data)
	print "Command door set, unicast 0x{:04x}, msg {}".format(unicast, data)

def Diff(li1, li2): 
	dif =  list(set(li1) - set(li2))
	# print "list dif [{}]".format(dif)
	return dif

def mesh_status_thread():
	while True:
		while mesh_status_want_send != 0:
			idx = 0
			mod = 1
			duration = 1
			period = mesh_status_interval
			status_control(idx, mod, duration, 0)
			idx = (idx + 1)%mod
			time.sleep(period)
		time.sleep(0.5)

def mesh_thread():
	global command_mesh_list
	
	while True:

		while len(command_mesh_list) > 0:
			
			command = json.loads(command_mesh_list.pop(0))

			print("mesh command:", command)
			
			action   = command["action"]
			unicast  = command["address"]

			if action == "dim":

				want_all = command["all"]
				level    = command["level"]
				line     = command["line"]
				ack      = command["ack"]
				cloud    = command["cloud"]

				if line == 0: # mean hub
					if want_all:
						# DEVICES.show_all(MeshDevice.VNG_LIGHT_HUB)
						print "not support dimming all, level {}".format(level)
						# levels_hub_send(GROUP_VNG_LIGHT_HUB, level, ack)
					else:
						print "\tdimming {:04X}, level {}".format(unicast, level)
						if unicast < GROUP_VNG_LIGHT_HUB:
							
							levels_hub_send(unicast, level, ack, cloud)
		
						else:
							
							level_line_send(unicast, 0, level, ack)
	
				elif line >= 1 and line <= 8:

					print "\tdimming {:04X}, line {} level {}".format(unicast, line, level)
					level_line_send(unicast, line - 1, level, ack)

				elif line == 22: #testline
					print "test hub {:04X} lines".format(unicast)
					levels_hub_send(unicast, 0, 0, False)
					for l in range(8):
						time.sleep(1)
						level_line_send(unicast, l, level, ack)
				
				elif line == 33: #find hub
					for h in range(3):
						levels_hub_send(unicast, 0, 0, cloud)
						time.sleep(0.5)
						levels_hub_send(unicast, 100, 0, cloud)
						time.sleep(0.5)

				else:
					
					stt = 0
					# devices = DEVICES.logdevices(MeshDevice.VNG_LIGHT_HUB)
					DEVICES.restore_dim()
					devices = DEVICES.dimdevices()
					print "\tdimming all, level {}, devices".format(level, len(devices))
					for d in devices:
						stt += 1
						print "\tdimming {:04X}, level {} stt {}".format(d, level, stt)
						levels_hub_send(d, level, ack = 0)
						time.sleep(1.5)
						
	

			elif action == "enable":
				enableds = command["enableds"]
				ack = command["ack"]

				print "\tenable {:04X}, enableds {}".format(unicast, enableds)
				levels_hub_enableds(unicast, enableds, ack)


			elif action == "status":
				global mesh_status_interval
				global mesh_status_want_send
				mesh_status_want_send = command["send"]
				mesh_status_interval  = command["interval"]

				# hub_statuts_config(unicast, mesh_status_interval % 2 == 0, mesh_status_interval % 2 == 0, mesh_status_interval)

				# thread_tracking_set(GROUP_VNG_THREAD_TRACKING, 1, mesh_status_interval % 2 == 0)


			elif action == "ota":
				want_all = command["all"]
				cloud    = command["cloud"]
				mac = []
				if cloud == False:
					dev = DEVICES.find_with_unicast(unicast)
					if dev is None:
						print "device {} not found".format(unicast)
						ws_send("device {} not found".format(unicast))
						continue
					mac = dev.mac()

				print "OTA device {}".format(unicast)

				ota_and_store(unicast, mac, cloud)

			elif action == "log_clr":
				print "Clear all logs"
				DEVICES.log_device_clr()

			elif action == "reset":
				cloud = command["cloud"]

				dev = DEVICES.find_with_unicast(unicast)

				if cloud == 0:
					command_ota_send(dev.unicast(), 5, dev.mac())
				else:
					cloudCli.send_ota(dev.unicast(), 5, dev.mac())
				
			elif action == "log_exclude":
				print "Restore excludes"
				DEVICES.restore_excludes()

			elif action == "relay":
				
				enabled = command["enabled"]
				want_all = command["all"]
				cloud = command["cloud"]

				# command_relay_send(GROUP_VNG_COMMAND, enabled)
				if cloud == 0:
					command_relay_send(unicast, enabled)
				else:
					cloudCli.send_relay(unicast, enabled)
			elif action == "ttl":
				
				ttl = command["ttl"]
				want_all = command["all"]
				if want_all:
					command_ttl_send(GROUP_VNG_COMMAND, ttl)
				else:
					command_ttl_send(unicast, ttl)

			elif action == "door":
				want_all = command["all"]
				old = command["old"]
				tid = Tid.get()
				door_send_new(unicast, tid)
				time.sleep(0.7)
				door_send_new(unicast, 1)

			elif action == "switch_netkey":
				
				netidx = command["netidx"]
				mesh_netkey_set(netidx)

			elif action == "dev_netkey":
				netidx = command["netidx"]
				command_netkey_send(unicast, netidx)
				try:
					netkeyfile = open("netkey/netkey", "a")
					record = "dev {:04X} net_idx {}\n".format(unicast, netidx)
					netkeyfile.write(record)
				except:
					print "failed"

			elif action == "add_group":
				group = command["group"]
				line = command["line"]
				ack  = command["ack"]
				if line == 0:
					levels_group_hub_add(unicast, group, ack)
				else:
					levels_group_add(unicast, line - 1, group, ack)
			elif action == "del_group":
				group = command["group"]
				line = command["line"]
				ack  = command["ack"]
				if line == 0:
					levels_group_hub_del(unicast, group, ack)
				else:
					levels_group_del(unicast, line - 1, group, ack)
			elif action == "clr_group":
				ack  = command["ack"]
				line = command["line"]

				if line == 0:
					levels_group_hub_clr(unicast, ack)
				else:
					levels_group_clr(unicast, line - 1 , ack)

					
			elif action == "set_tid":
				global Tid
				tid = command["tid"]
				print "set tid {}".format(tid)
				Tid.set(tid)
			elif action == "groups_count":
				hub_groups_count_get(unicast, 0, 1)

			elif action == "groups_list":
				line = command["line"]
				gbits= command["gbits"]
				line = line - 1
				hub_groups_list_get(unicast, 0, 1, line, gbits)
			
			elif action == "status_store":
				hub_status_store(unicast)

			elif action == "status_config":
				status_en = command["status_en"]
				warn_en   = command["warn_en"]
				warn_period = command["warn_period"]
				hub_status_config(unicast, status_en, warn_en, 5, warn_period)

			elif action == "get_status":
				level_line_get_send(unicast, 5)
			
			elif action == "set_filter":
				filter = command["filter"]
				print "filter: {:d} ({})".format(len(filter), filter)
				mesh_filter_set(filter)

			elif action == "zephyr":
				start = command["start"]
				if start:
					command = "systemctl restart meshtool"
					print command
					subprocess.call(shlex.split(command))
				else:
					command = "systemctl stop meshtool"
					print command
					subprocess.call(shlex.split(command))


				

			time.sleep(0.2)			
		time.sleep(0.2)


def bacnet_thread():
	global command_bacnet_list

	while True:
		while len(command_bacnet_list) > 0:
			command = json.loads(command_bacnet_list.pop(0))
			print("bacnet command: ", command)
			time.sleep(0.2)
			device = command["device"]
			instance = command["instance"]
			level = command["level"]
			if device == "light":
	
				bacnet_subprocess_send(BAC_OBJ_TYPE_ANALOG_OUTPUT, instance, level)
			else:


				bacnet_subprocess_send(BAC_OBJ_TYPE_BINARY_OUTPUT, instance, (level > 0))
				

			time.sleep(0.2)

		time.sleep(0.2)


def command_handle(command):
	print("command:", command)
	obj = json.loads(command) 

	if obj["type"] == "mesh":
		# print("new command mesh");
		command_mesh_list.append(command)
	if obj["type"] == "bacnet":
		
		command_bacnet_list.append(command)
		print("command bacnet list ", command_bacnet_list);



#######################################
#	MQTT
#######################################



# def on_connect(client, userdata, flags, rc):
#     # print("MQTT connected with result code "+str(rc))
#     print ("MQTT CONNECTED")
#     client.subscribe("dungtopic")

# def on_message(client, userdata, msg):
# 	payload = json.loads(msg.payload)
# 	print("payload:", payload)
	

# mqtt_client = mqtt.Client()
# mqtt_client.on_connect = on_connect
# mqtt_client.on_message = on_message


# def mqtt_publish(topic, data):
#     global mqtt_client
#     mqtt_client.publish(topic, data)

# def mqtt_thread():
# 	BROKER = "localhost"
# 	global mqtt_client
# 	mqtt_client.connect(BROKER, 1883, 10)
# 	while True:
# 		mqtt_client.loop()


#######################################
#	WEBSOCKET SERVER
#######################################


wsclients = []
class SimpleEcho(WebSocket):
	def handle(self):
		# self.send_message(self.data)
		print "received {}".format(self.data)
		message = self.data
		command_handle(message)

	def connected(self):
		global socket_client
		print(self.address, 'connected')
		if len(wsclients) == 0:
			wsclients.append(self)
		else:
			wsclients[0] = self

	def handle_close(self):
		print(self.address, 'closed')


def server_thread():
	print "WebSocket server started, port {}".format(8765)
	server = WebSocketServer('', 8765, SimpleEcho)
	server.serve_forever()



def ws_send(mess):
    if len(wsclients) > 0:
		wsclients[0].send_message(mess)
		
#######################################
#	SYSTEMD LOG
#######################################

def run_command(command):
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    def read():
        while True:
            output = process.stdout.readline()
            if output == '' and process.poll() is not None:
                break
            if output:
            #     print output.strip()
				ws_send(str(output.strip()))
				

    thread.start_new_thread(read, ())
    rc = process.poll()
    return rc


#######################################
#	MAIN
#######################################
if __name__ == "__main__":

	# thread.start_new_thread(mqtt_thread, ())

	print "__main__ start"


	thread.start_new_thread(mesh_thread, ())
	thread.start_new_thread(bacnet_thread, ())
	thread.start_new_thread(server_thread, ())
	thread.start_new_thread(mesh_status_thread, ())

	httpSrv = SimpleHttpServer()
	httpSrv.start()

	run_command("journalctl -u meshtool -f")

	while True:
		time.sleep(100)

