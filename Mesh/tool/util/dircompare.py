import os


LINE = "(type 0, id 0379, unicast 6FA5, mac EC:9E:19:F7:28:71:)"
LINE_LEN = len(LINE)

DIR1_PATH = "../log"
DIR2_PATH = "../otaeds"
RESULT = "dircompare"

DIR1_LINES = list()
DIR2_LINES = list()


def restore_dir(dir):
    dirlist= list()
    for r, d, f in os.walk(dir):
        for file in f:
            file_path = os.path.join(r, file)
            with open(file_path) as fp:
                for cnt, line in enumerate(fp):
                    if len(line) < LINE_LEN:
                        continue
                    _line = line[:LINE_LEN]
                    dirlist.append((file, _line))

    return dirlist


DIR1_LINES = restore_dir(DIR1_PATH)
DIR2_LINES = restore_dir(DIR2_PATH)

# print DIR2_LINES



result = open(RESULT, "w")

for file, line in DIR1_LINES:
    for file2, line2 in DIR2_LINES:
        if line == line2:
            result.write(line + " : " + file2 + '\n')
            break


