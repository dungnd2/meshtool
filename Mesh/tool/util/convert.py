import subprocess
import os

excludes = list() #list of exclude hub.unicast
excludes_path = "../otaeds"
exclude_idx = len("type 0, id 0228, unicast ") + 1
exclude_len = 4 #0010

convert_path = "../otaeds/convert/"
subprocess.call(["mkdir", "-p", convert_path])

opened = list()


def convert():
    global convert_path
    for r, d, f in os.walk(excludes_path):
        for file in f:
            file_path = os.path.join(r, file)
            f_convert_path = convert_path + file
            if f_convert_path in opened:
                continue
            opened.append(f_convert_path)
            print "open ", f_convert_path
            f_convert = open(f_convert_path, "w")
            with open(file_path) as fp:

                for cnt, line in enumerate(fp):
                    if len(line) < 20:
                        continue
                    
                    str_unicast = "0x" + line[exclude_idx : exclude_idx + exclude_len]
                    unicast = int(str_unicast, 16)
                    _line  = line[:len(line) -1 ]
                    convert_line = "{} {:d}\n".format(_line, unicast)
                    print convert_line
                    f_convert.write(convert_line)
            
            print "close ", f_convert_path
            f_convert.close()

convert()