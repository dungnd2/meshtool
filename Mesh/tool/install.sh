

#!/bin/sh

echo $PWD

#change this
ZEPHYR_EXECUTEABLE=zep18


cp systemd/meshtoolpy.service.in meshtoolpy.service
cp systemd/meshtool.service.in zephyr/bin/meshtool.service

sed -i 's|SERVICE_PATH|'$PWD'|g' meshtoolpy.service
sed -i 's|SERVICE_PATH|'$PWD'|g' zephyr/bin/meshtool.service
sed -i 's|zep18|'$ZEPHYR_EXECUTEABLE'|g' zephyr/bin/meshtool.service

echo "install systemd services"
cp meshtoolpy.service /etc/systemd/system 
cp zephyr/bin/meshtool.service  /etc/systemd/system

systemctl daemon-reload
# systemctl enable meshtool
systemctl enable meshtoolpy 
# systemctl start meshtool 
systemctl start meshtoolpy

