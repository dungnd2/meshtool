#include "file.h"
#include "common/log.h"

FILE* log_file;


void log_init()
{
	log_file = fopen(
		"/home/dung/Documents/GIT_REPO/iot/device/zephyr/apps/v13/helper/sender/nrf5/board/log.txt", "w");
}

void log_hex(u8_t* buf, u16_t len)
{	
	char end = '\n';
	char* log = bt_hex(buf, len);
	fwrite(log, 1, len*2, log_file);
	fwrite(&end, 1, 1, log_file);
}

void log_str(u8_t* buf, u16_t len)
{
	char end = '\n';

	fwrite(buf, 1, len, log_file);
	fwrite(&end,1, 1, log_file);
}