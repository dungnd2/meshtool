#ifndef FILE_H
#define FILE_H


#include <stdio.h>
#include <zephyr.h>

// void file_init(const char* file);

// void file_write(FILE *fp, u8_t* buf, u16_t len);

// void file_write_line(FILE *fp, u8_t* buf, u16_t len);

// void file_read_line(File* fp, u8_t* buf, u16_t* len);


void log_init();
void log_hex(u8_t* buf, u16_t len);
void log_str(u8_t* buf, u16_t len);

#endif