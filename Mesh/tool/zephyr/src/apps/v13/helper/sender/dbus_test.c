#include <dbus_lib.h>
#include <stdio.h>
#include <unistd.h>
#include "print.h"
#include <zephyr.h>
#include <common/log.h>

#define TO_MASTER_CHANNEL "dung.dbus.mesh.master"
#define TO_SLAVE_CHANNEL  "dung.dbus.mesh.slave"

int dbus_on_message(const char* data, int data_len)
{
    FPRINTF(stdout, "on message len %d: [%s]\n", data_len, data);
    return 0;
}

int main()
{
    int error;
    dbus_obj_id_t pid;
    dbus_obj_id_t sid;

    error = dbus_init();

    if (error) {
        SYS_LOG_DBG("dbus_init() failed\n");
        return error;
    }

    error = dbus_publisher_create(&pid);

    if (error) {
        SYS_LOG_DBG("dbus_publisher_create() failed\n");
        return error;
    }


    error = dbus_subscriber_create(&sid);

    if (error) {
        SYS_LOG_DBG("dbus_subscriber_create() failed\n");
        return error;
    }

    error = dbus_subscribe(sid, TO_MASTER_CHANNEL, dbus_on_message);

    if (error) {
        SYS_LOG_DBG("dbus_subscribe() failed\n");
    }

    // dbus_start_listening();

    char data[] = "hello dung 0123456";
    int seq = 0;

    while(1) {

        sprintf(data, "hello dung %04d", seq++ );
        SYS_LOG_DBG("sending message len %d: [%s]\n", sizeof(data), data);
        error =  dbus_pushlish(pid, TO_SLAVE_CHANNEL, data, sizeof(data));
        if (error) {
            return error;
        }

        sleep(1);
    }

    return 0;
}