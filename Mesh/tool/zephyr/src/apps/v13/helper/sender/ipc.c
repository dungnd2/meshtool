#include <ipc.h>

#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <limits.h>
#include <libgen.h>
#include <pthread.h>

#include "common/log.h"

#include <logging/sys_log.h>



#define BUF_SIZE 500
#define MAX_CLI_BUF 500
#define MAX_CLI 5


struct socket_srv
{
	int fd;
	ipc_received_t received_cb;
};

struct socket_cli
{	
	int fd;
    char buf[MAX_CLI_BUF];
    uint32_t len;

};

static struct socket_srv socket_srv = {
	.received_cb = NULL,
};

static struct socket_cli socket_cli = {
	.len = 0,
	
};


static BT_STACK_NOINIT(ipc_thread_stack, 1024);
static struct k_thread ipc_thread_handle;




void show_info()
{   
    char buf[PATH_MAX];

    getcwd(buf, PATH_MAX);

    SYS_LOG_DBG("current env dir: %s", buf);

    readlink("/proc/self/exe", buf, PATH_MAX);

    SYS_LOG_DBG("executable path: %s", buf);
    SYS_LOG_DBG("exe dir path: %s", dirname(buf));
}



// static void ipc_thread(void *p1, void *p2, void *p3)
static void ipc_thread(void *p1)
{
	
	char buf[BUF_SIZE];
	int numRead;

    while(1) {

     
        socket_cli.fd = accept(socket_srv.fd, NULL, NULL);
        if (socket_cli.fd == -1) {
            SYS_LOG_ERR("accept");
            continue;
        }

        /* Transfer data from connected socket to stdout until EOF */
        socket_cli.len = 0;
        memset(socket_cli.buf, 0, BUF_SIZE);

        socket_cli.len = read(socket_cli.fd, socket_cli.buf, BUF_SIZE);
           
        // SYS_LOG_DBG("client msg: %s", socket_cli.buf);
        if(socket_srv.received_cb) {
        	socket_srv.received_cb(socket_cli.buf, socket_cli.len);
        } 

        close(socket_cli.fd);

    }
}



int socket_init(ipc_received_t received_cb)
{
	struct sockaddr_un addr;
    pthread_t pthread;
    int error;

    // show_info();

    if(received_cb != NULL) {
    	socket_srv.received_cb = received_cb;
    }

    socket_srv.fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (socket_srv.fd == -1) {
        SYS_LOG_ERR("socket");
        return -1;
    }

    /* Construct server socket address, bind socket to it,
       and make this a listening socket */

    if (remove(SOCKET_SERVER_PATH) == -1 && errno != ENOENT)
        SYS_LOG_DBG("remove-%s", SOCKET_SERVER_PATH);

    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SOCKET_SERVER_PATH, sizeof(addr.sun_path) - 1);

    if (bind(socket_srv.fd, (struct sockaddr *) &addr, sizeof(struct sockaddr_un)) == -1) {
        SYS_LOG_ERR("bind");
        return -1;
    }

    if (listen(socket_srv.fd, MAX_CLI) == -1) {
        SYS_LOG_ERR("listen");
    	return -1;
    }

    
    error = pthread_create(&pthread, NULL, ipc_thread, NULL);
    if (error != 0) {
        SYS_LOG_ERR("pthread_create");
        return error;
    }

    return 0;
}
