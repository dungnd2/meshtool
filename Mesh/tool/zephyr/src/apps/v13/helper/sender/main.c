
#include <device.h>
#include <uart.h>
#include <gpio.h>
#include <board.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>
#include <crc16.h>
#include <misc/byteorder.h>
#include <watchdog.h>
#include "common/log.h"

#include <logging/sys_log.h>

#include <host/mesh/net.h>
#include <host/mesh/adv.h>
#include <host/mesh/access.h>
#include <settings/settings.h>
#include <kernel.h>
#include <stdio.h>

#include <uart.h>

#include <file.h>

#include <ipc.h>

#include <pthread.h>

#include <bluetooth/hci.h>

#define NEW_KEY 1

#undef SYS_LOG_DOMAIN
#define SYS_LOG_DOMAIN "app"

#define CID_VNG 0x00FF
#define MOD_VND_VNG_GATEWAY 0x0000
#define MOD_VNG_GATEWAY 0x0004
#define OP_VENDOR_EVENT                       BT_MESH_MODEL_OP_3(0x00, CID_VNG)
#define BT_MESH_MODEL_OP_VNG_STATUS_SRV       BT_MESH_MODEL_OP_3(0x04, CID_VNG)

#define BT_MESH_MODEL_OP_VND_STATUS           BT_MESH_MODEL_OP_2(0x82, 0x07)
#define BT_MESH_MODEL_OP_VND_LEVELS           BT_MESH_MODEL_OP_2(0x82, 0x08)
#define BT_MESH_MODEL_OP_VND_WARNING          BT_MESH_MODEL_OP_2(0x82, 0x09)

#define GATEWAY_GROUP_ADDR 0xC000
#define STATUS_GROUP_ADDR 0xE000

enum {
    IPC_MESH_SEND = 0xAA,
    IPC_MESH_HACK = 0xBB,
    IPC_MESH_PROV = 0xCC,
    IPC_MESH_SEQ = 0xDD,
    IPC_MESH_CLR_REMOTE_LOG = 0xEE,
    IPC_MESH_NETKEY_SET = 0xAB,
    IPC_MESH_FILTER_SET = 0xAC,
};


#define MODELS_BIND(models, is_vnd, net_idx, app_idx) \
do { \
    size_t size = ARRAY_SIZE(models);\
    size_t idx; \
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    for(idx = 0; idx < size; idx++) { \
        addr = elems[models[idx].elem_idx].addr;\
        if(is_vnd) {\
            SYS_LOG_DBG("vnd bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind_vnd(net_idx, primary_addr, addr, app_idx, models[idx].vnd.id, \
                models[idx].vnd.company, NULL);\
        } else {\
            SYS_LOG_DBG("bind addr [0x%04x]", addr);\
            bt_mesh_cfg_mod_app_bind(net_idx, primary_addr, addr,\
            app_idx, models[idx].id, NULL);\
        }\
    } \
} while(0)


#define MODEL_PUB(_model, _vnd_, _net_idx, _pub)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = _model->elem_idx + primary_addr;\
    if(_vnd_) {\
        BT_DBG("vnd pub, elem [0x%04x], model [0x%04x], group [0x%04x]",\
            addr, _model->vnd.id, _pub->addr);\
        bt_mesh_cfg_mod_pub_set_vnd(_net_idx, primary_addr, addr,\
            _model->vnd.id,_model->vnd.company, _pub, NULL);\
    } else {\
        BT_DBG("vnd pub, elem [0x%04x], model [0x%04x], group [0x%04x]",\
            addr, _model->vnd.id, _pub->addr);\
        bt_mesh_cfg_mod_pub_set(_net_idx, primary_addr, addr,\
            _model->id, _pub, NULL);\
    }\
}

#define MODEL_SUB(_model, _vnd_, _net_idx, _subaddr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = _model->elem_idx + primary_addr;\
    if(_vnd_) {\
        BT_DBG("vnd sub overwrite, elem [0x%04x], model [0x%04x], group [0x%04x]",\
                addr, _model->vnd.id, _subaddr);\
        bt_mesh_cfg_mod_sub_add_vnd(_net_idx, primary_addr, addr,\
                _subaddr, _model->vnd.id, CID_VNG, NULL);\
    } else {\
        BT_DBG("sig sub overwrite, elem [0x%04x], model [0x%04x], group [0x%04x]",\
                addr, _model->id, _subaddr);\
        bt_mesh_cfg_mod_sub_add(_net_idx, primary_addr, addr,\
                _subaddr, _model->id, NULL);\
    }\
}


#define BT_MESH_BUF_SIZE 32
#define BT_MESH_USER_DATA_MIN 4
#define MESH_BUF_COUNT 32

NET_BUF_POOL_DEFINE(command_pool,
                    MESH_BUF_COUNT,
                    BT_MESH_BUF_SIZE,
                    BT_MESH_USER_DATA_MIN,
                    NULL);


NET_BUF_SIMPLE_DEFINE(mesh_data_buf, 15);

NET_BUF_SIMPLE_DEFINE(inc_pl_0, 10);
NET_BUF_SIMPLE_DEFINE(inc_pl_1, 10);
NET_BUF_SIMPLE_DEFINE(inc_pl_2, 10);
NET_BUF_SIMPLE_DEFINE(inc_pl_3, 10);
NET_BUF_SIMPLE_DEFINE(inc_pl_4, 10);

extern void _app_key_del(u16_t key_idx);
extern void bt_mesh_remote_log_clr();

struct mesh_config
{
    struct k_delayed_work config_timer;
    u8_t config_tasks;
    struct k_delayed_work switch_timer;
    u8_t switch_task;
    u8_t new_idx;
};
struct mesh_renew
{
	
	struct k_delayed_work timer;

	struct {
		u16_t addr;
		u16_t net_idx;
		u8_t flags;
		u8_t net_key[16];
		u32_t iv_index;
		u8_t dev_key[16];
		u32_t seq;
		
	} ctx;

};
static struct package_filter pkg_filter = {
    .min_rssi = -100,
    .inc_op_len = 0,
    .inc_pl_len = 0,
    .min_ttl = 4,
    .max_ttl = 7,
    .inc_pls = {
        &inc_pl_0, &inc_pl_1, &inc_pl_2, &inc_pl_3, &inc_pl_4 
    },
    .ac = 0,
};

struct mesh_transmit {
    struct k_delayed_work timer;
    u16_t target;
    u32_t opcode;
    struct net_buf_simple* data;

    u32_t interval;
};

struct msg_ctx
{
    u16_t target;
    u16_t opcode;
    u16_t len;
    u8_t data[];
};

static struct mesh_renew mesh_renew;
static struct mesh_transmit mesh_transmit = {
    .data = &mesh_data_buf,
};

static struct mesh_config mesh_config = {

};

struct k_delayed_work file_timer;

#define NETKEYS 15
static const u8_t net_keys[NETKEYS][16] = {
    {0x7a, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x7b, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x7c, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,}, 
    {0x7d, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x7e, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,}, 
    {0x7f, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,}, 
    {0x80, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x81, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,}, 
    {0x82, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x83, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x84, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,}, 
    {0x85, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x86, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x87, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
    {0x88, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b,},
};

static int set_filter(const char* buf);

static void prov_complete(u16_t net_idx, u16_t addr);

static void prov_reset(void);
static void bt_ready(int err);
 

static int string_to_hex(const u8_t *string,
                                  int length,
                                  u8_t *hex,
                                  u8_t *size);

static int to_hex(u8_t character, u8_t *hex);



static const u8_t app_idx = 0;

#ifdef NEW_KEY

    static const u8_t app_key[16] = {
       0x4d, 0x48, 0x63, 0x43, 0x41, 0x51, 0x45, 0x45, 0x49, 0x4c, 0x79, 0x53, 0x4a, 0x69, 0x4c, 0x2f,
    };

    static const u8_t dev_key[16] = {
        0x55, 0x65, 0x41, 0x58, 0x64, 0x65, 0x78, 0x4d, 0x76, 0x74, 0x4c, 0x65, 0x56, 0x31, 0x70, 0x52, 
    };

    static const u8_t net_key[16] = {
        0x7a, 0x53, 0x54, 0x50, 0x32, 0x46, 0x45, 0x65, 0x59, 0x4e, 0x59, 0x47, 0x52, 0x79, 0x45, 0x6b, 
    };

#else

    static const u8_t app_key[16] = {
        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
    };

    static const u8_t dev_key[16] = {
        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
    };

    static const u8_t net_key[16] = {
        0x41, 0x4e, 0xf3, 0x9c, 0x1a, 0xf7, 0x51, 0x5d,
        0x17, 0xce, 0x78, 0xf1, 0x77, 0x92, 0xc7, 0x4c,
    };

#endif



static u16_t net_idx = NET_INDEX;
static const u32_t iv_index;
static u8_t flags;

static u16_t addr = HARD_ADDR;


static bool setting_loading = false;

static u8_t uuid[16] = {0xFF, 0xFF};

static struct bt_mesh_prov prov = {
    .uuid = uuid,
    .output_size = 0,
    .complete = prov_complete,
    .reset = prov_reset

};


static struct bt_mesh_cfg_srv cfg_srv = {
    .relay = BT_MESH_RELAY_ENABLED,
    // .beacon = BT_MESH_BEACON_ENABLED,
    .beacon = BT_MESH_BEACON_DISABLED,
#if defined(CONFIG_BT_MESH_FRIEND)
    .frnd = BT_MESH_FRIEND_ENABLED,
#else
    .frnd = BT_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BT_MESH_GATT_PROXY)
    .gatt_proxy = BT_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = BT_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = DEFAULT_TTL,

    /* 3 transmissions with 20ms interval */
    .net_transmit = BT_MESH_TRANSMIT(5, 20),
    .relay_retransmit = BT_MESH_TRANSMIT(5, 20),
};


static struct bt_mesh_health_srv health_srv = {
};

BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);

static struct bt_mesh_cfg_cli cfg_cli = {
};


static struct bt_mesh_vng_device_srv vng_device_srvs[] = {
    {.user_data = NULL},
};


static struct bt_mesh_model models0[] = {
    BT_MESH_MODEL_CFG_CLI(&cfg_cli),
    BT_MESH_MODEL_CFG_SRV(&cfg_srv),
    BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub),
};


static struct bt_mesh_model vnd_models0[] = {
    BT_MESH_MODEL_VNG_DEVICE_SRV(&vng_device_srvs[0], NULL),
 
};



static struct bt_mesh_elem elems[] = {
    BT_MESH_ELEM(0, models0, vnd_models0),
};

static const struct bt_mesh_comp comp = {
    .cid = CID_VNG,
    .elem = elems,
    .elem_count = ARRAY_SIZE(elems),
};


static void switch_work(struct k_work* work)
{
    SYS_LOG_INF("add app key idx %u, net idx %u", 0, mesh_config.new_idx);

    bt_mesh_cfg_app_key_add(0, 
                        bt_mesh_primary_addr(),
                        mesh_config.new_idx, 
                        app_idx, 
                        app_key, 
                        NULL);

    // MODELS_BIND(models0, false , mesh_config.new_idx, app_idx);
    MODELS_BIND(vnd_models0, true, mesh_config.new_idx, app_idx);

    net_idx = mesh_config.new_idx;
}

static void switch_netkey(u16_t new_idx)
{
     SYS_LOG_INF("new net key idx %u", new_idx);

#ifdef MULTI_NETKEYS
    // rebind current appkey to new net key
    if(new_idx == net_idx)
        return;
    _app_key_del(app_idx);
    mesh_config.new_idx = new_idx;
    k_delayed_work_submit(&mesh_config.switch_timer, 100);
    
#endif
}


static void config_work(struct k_work* work)
{

    if(mesh_config.config_tasks > 0) {

        SYS_LOG_INF("add net key idx %u", mesh_config.config_tasks);
        bt_mesh_cfg_net_key_add(net_idx, 
                                addr, 
                                mesh_config.config_tasks,
                                net_keys[mesh_config.config_tasks], 
                                NULL);
        mesh_config.config_tasks--;
        k_delayed_work_submit(&mesh_config.config_timer, 10);
    }

}
static void prov_complete(u16_t net_idx, u16_t addr)
{
    int i;
    if(setting_loading)
        return;
    
    SYS_LOG_INF("Provisioned completed");

    SYS_LOG_INF("add app key idx %u, net index %u", app_idx, net_idx);

    bt_mesh_cfg_app_key_add(net_idx, 
                            bt_mesh_primary_addr(),
                            net_idx, 
                            app_idx, 
                            app_key, 
                            NULL);

    // MODELS_BIND(models0, false , net_idx, app_idx);
    MODELS_BIND(vnd_models0, true, net_idx, app_idx);

#ifdef MULTI_NETKEYS
    
    k_delayed_work_init(&mesh_config.config_timer, config_work);
    k_delayed_work_init(&mesh_config.switch_timer, switch_work);
    
    mesh_config.config_tasks = NETKEYS - 1;
    k_delayed_work_submit(&mesh_config.config_timer, 10);

#endif

}


static void prov_reset(void) 
{
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}


static int to_hex(u8_t character, u8_t *hex)
{
    if ('0' <= character && character <= '9') {
        *hex = character - '0';
        return 0;
    }
    if ('A' <= character && character <= 'F') {
        *hex = character - 'A' + 10;
        return 0;
    }
    if ('a' <= character && character <= 'f') {
        *hex = character - 'a' + 10;
        return 0;
    }
    // SYS_LOG_ERR("toHex failed");
    return -EINVAL;
}

static int string_to_hex(const u8_t *string,
                                  int length,
                                  u8_t *hex,
                                  u8_t *size)
{

    // char s[length+1];
    // memcpy(s, string, length);
    // s[length] = '\0';

    // SYS_LOG_DBG("%d:%s", length, bt_hex(string, length));

    if (length % 2 != 0) {
        SYS_LOG_ERR("Length of string: %d not even number", (int)length);
        return -EINVAL;
    }

    *size = 0;
    u8_t position = 0;
    int error;
    for(u8_t index = 0; index < length; position += 1) {
        u8_t first;
        error = to_hex(string[index], &first);
        if (error != 0)
            return error;

        u8_t second;
        error = to_hex(string[index + 1], &second);
        if (error != 0)
            return error;

        hex[position] = (first << 4) + second;
        index += 2;
        *size += 1;
    }

    return 0;
}

static void transmit(struct k_work* work)
{
    
    // SYS_LOG_INF("STARTTTTTTTTTTTTT");
    int error;


    if(!bt_mesh_is_provisioned())
        return;
    
    NET_BUF_SIMPLE_DEFINE(msg, 2 + 12 + 4);

    struct bt_mesh_msg_ctx ctx = {
        .net_idx = net_idx,
        .app_idx = 0,
        .addr = mesh_transmit.target,
        .send_ttl = DEFAULT_TTL,
    };


    bt_mesh_model_msg_init(&msg, mesh_transmit.opcode);
    net_buf_simple_add_mem(&msg, mesh_transmit.data->data, mesh_transmit.data->len);
    
    SYS_LOG_INF("target 0x%04X, opcode %08X data: %u [%s]", 
                mesh_transmit.target,
                mesh_transmit.opcode,
                mesh_transmit.data->len,
                bt_hex(mesh_transmit.data->data, mesh_transmit.data->len));

   
    error = bt_mesh_model_send(&vnd_models0[0], &ctx, &msg, NULL, NULL);
    if (error != 0) {
        SYS_LOG_ERR("Send failed with error: %d", error);
    } 

    SYS_LOG_DBG("bt_mesh.seq 0x%08x", bt_mesh.seq);
}

int field_find(char* buf, u16_t buf_len , char* field, u8_t* field_len)
{
    // SYS_LOG_DBG("%s", buf);

    u8_t str_field[buf_len];
    u8_t str_len;

    int begin = - 1 , end = -1;
    for(int i = 0; i < buf_len; i++) {
        if(buf[i] == '[')
            begin = i+1;
        else if(buf[i] == ']') {
            end  = i;
            break;
        }
    }

    if(begin == -1 || end == -1)
        return -1;

    str_len = end - begin;

    // SYS_LOG_DBG("begin: %u, end: %u", begin, end);

    for(int i = 0; i < str_len; i++) {
        str_field[i] = buf[i+begin];
    }

    return string_to_hex(str_field, str_len, field, field_len);

}

static void do_provision()
{
    int err;

    err = bt_mesh_provision(net_keys[net_idx], net_idx, flags, iv_index, addr,
                dev_key);

    if (err == -EALREADY) {
        printk("Using stored settings\n");
    } else if (err) {
        printk("Provisioning failed (err %d)\n", err);
        return;
    }
}

static void bt_ready(int err)
{
 
    struct bt_le_oob oob;

    if (err) {
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }

    SYS_LOG_INF("Bluetooth initialized");

    err = bt_le_oob_get_local(BT_ID_DEFAULT, &oob);

    if (err != 0) {
        SYS_LOG_ERR("get local oob failed with error: %d", err);
        return;
    }

    memset(uuid, 0, sizeof(uuid));
    memcpy(uuid, oob.addr.a.val, 6);

    err = bt_mesh_init(&prov, &comp);

    if (err) {
        SYS_LOG_ERR("Initializing mesh failed (err %d)", err);
        return;
    }

    SYS_LOG_DBG("Mesh initialized\n");


    if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
        SYS_LOG_DBG("Loading stored settings\n");
        setting_loading = true;
        settings_load();
        setting_loading = false;
    }

    do_provision();

    
}


static void process_credentials_init()
{
    int err;
#if CONFIG_BT_MESH_DBUS_SLAVE
    return;
#endif
    SYS_LOG_INF("hciconfig hci0 down");
    err = system("sudo hciconfig hci0 down");
    if(err) {
        perror("hciconfig failed:");
        return;
    }
}


static void ipc_mesh_hack(u8_t show)
{
    if (show != 0) {
        SYS_LOG_DBG("enable");
        bt_mesh_hack_set(true);
    }
    else {
        SYS_LOG_DBG("disable");
        bt_mesh_hack_set(false);
    }
}


static void ipc_mesh_send(char *buf, int len)
{
  
    u8_t op_0, op_1;
    u16_t opcode;
    u16_t msg_len;
    u16_t target;

    struct msg_ctx* msg = (struct msg_ctx*)(buf);

    target = sys_cpu_to_be16(msg->target);
    opcode = sys_cpu_to_be16(msg->opcode);
    msg_len = sys_cpu_to_be16(msg->len);


    if(msg_len + 6 != len) {
        SYS_LOG_DBG("invalid");
        return;
    }
    // SYS_LOG_DBG("target 0x%04x", target);
    // SYS_LOG_DBG("opcode 0x%04x", opcode);
    // SYS_LOG_DBG("len %u", msg_len);
    // SYS_LOG_DBG("data %s", bt_hex(msg->data, msg_len));

    
    op_0 = (opcode & 0xFF00) >> 8;
    op_1 = (opcode & 0x00FF);

    mesh_transmit.target = target;

    if(op_1 == CID_VNG) {
        mesh_transmit.opcode = BT_MESH_MODEL_OP_3(op_0, CID_VNG);
    } else {
        mesh_transmit.opcode = BT_MESH_MODEL_OP_2(op_0, op_1);
    }

    net_buf_simple_reset(mesh_transmit.data);
    net_buf_simple_add_mem(mesh_transmit.data, msg->data, msg_len);
    k_delayed_work_cancel(&mesh_transmit.timer);
    k_delayed_work_submit(&mesh_transmit.timer, 50);
    
}

static void ipc_mesh_seq(const u8_t* buf, u16_t len)
{
    if(len < 4 ) {
        SYS_LOG_DBG("len %d is too short", len);
        return;
    }

    u32_t seq = *(u32_t*)(buf);
    seq = sys_cpu_to_be32(seq);

    SYS_LOG_DBG("seq 0x%08x", seq);

    bt_mesh.seq = seq;


}

static void ipc_mesh_prov(const u8_t* buf, u16_t len)
{
    //|unicast|seq| = 6bytesSYS_LOG_DBG

    if(len < 6 ) {
        SYS_LOG_DBG("len %d is too short", len);
        return;
    }

    k_sleep(1000);

    u16_t unicast = *(u16_t*)(buf);
    unicast = sys_cpu_to_be16(unicast);

    u32_t seq = *(u32_t*)(buf + 2);
    seq = sys_cpu_to_be32(seq);

    addr = unicast;

    SYS_LOG_DBG("addr 0x%04x, seq 0x%08x", addr, seq);


    bt_mesh_comp_provision(addr);

    bt_mesh.seq = seq;
    

}

static void ipc_mesh_remote_log_clr(const u8_t* buf, u16_t len)
{
    bt_mesh_remote_log_clr();
}


static void mesh_renew_work(struct k_work* work)
{
	bt_mesh_provision(
		mesh_renew.ctx.net_key, 
		mesh_renew.ctx.net_idx, 
		mesh_renew.ctx.flags,
		mesh_renew.ctx.iv_index,
		mesh_renew.ctx.addr,
		mesh_renew.ctx.dev_key);

	bt_mesh.seq = mesh_renew.ctx.seq;
}

static void ipc_mesh_netkey_set(const u8_t* buf, u16_t len)
{
    
    switch_netkey(buf[0]);
}

static void ipc_mesh_filter_set(const u8_t* buf, u16_t len)
{
    
    char* data;

    pkg_filter.inc_op_len = 0;
    pkg_filter.inc_pl_len = 0;
    pkg_filter.ac = 0;

    // SYS_LOG_INF("filter: %d [%s]", len, buf);
    set_filter(buf);

    for (int i = 0; i < len - 1; i++) {

        if (buf[i] == '\n') {
            set_filter(&buf[i + 1]);
        }
    }

    SYS_LOG_INF("opcode %u, data %u", pkg_filter.inc_op_len, pkg_filter.inc_pl_len);
}


static void ipc_received(char *buf, int len)
{
    u8_t command = buf[0];
    // SYS_LOG_DBG("ipc command 0x%02x", command);

    if(command == IPC_MESH_SEND) {

        ipc_mesh_send(buf + 1, len-1);

    } else if (command == IPC_MESH_HACK) {

        ipc_mesh_hack(buf[1]);

    } else if (command == IPC_MESH_PROV) {

        ipc_mesh_prov(buf + 1, len-1);

    } else if (command == IPC_MESH_SEQ) {

        ipc_mesh_seq(buf + 1, len-1);
    } else if (command == IPC_MESH_CLR_REMOTE_LOG) {

        ipc_mesh_remote_log_clr(buf+1, len-1);

    } else if (command == IPC_MESH_NETKEY_SET) {
        ipc_mesh_netkey_set(buf+1, len-1);

    } else if (command == IPC_MESH_FILTER_SET) {
        ipc_mesh_filter_set(buf+1, len-1);
    }
}


static int set_filter(const char* buf)
{

    u8_t data[40];
    u8_t size;

    int err = field_find(buf, 20, data, &size);
        
    if(err) {
        return err;
    }

    if(buf[0] == 'o') {
        if(data[1] == CID_VNG) {
            pkg_filter.inc_ops[pkg_filter.inc_op_len] = BT_MESH_MODEL_OP_3(data[0], CID_VNG);
        } else {
            pkg_filter.inc_ops[pkg_filter.inc_op_len] = BT_MESH_MODEL_OP_2(data[0], data[1]);
        }

        SYS_LOG_INF("FILTER OPCODE [0x%08X]", pkg_filter.inc_ops[pkg_filter.inc_op_len]);

        pkg_filter.inc_op_len++;

    } else if(buf[0] == 'r') {

        pkg_filter.min_rssi = -data[0];
        SYS_LOG_INF("FILTER MIN_RSSI [%d]", pkg_filter.min_rssi);

    } else if(buf[0] == 'd') {
        net_buf_simple_reset(pkg_filter.inc_pls[pkg_filter.inc_pl_len]);
        net_buf_simple_add_mem(pkg_filter.inc_pls[pkg_filter.inc_pl_len], data, size);
        SYS_LOG_INF("FILTER DATA [%d:%s]", pkg_filter.inc_pls[pkg_filter.inc_pl_len]->len, 
                bt_hex(pkg_filter.inc_pls[pkg_filter.inc_pl_len]->data, pkg_filter.inc_pls[pkg_filter.inc_pl_len]->len));
        pkg_filter.inc_pl_len++;
    } else if(buf[0] == 't') {
        pkg_filter.min_ttl = data[0];
        pkg_filter.max_ttl = data[1];
        SYS_LOG_INF("FILTER TTL [%d-%d]", pkg_filter.min_ttl, pkg_filter.max_ttl);
    } else if(buf[0] == 'a') {
        pkg_filter.ac = data[0];

        SYS_LOG_INF("TRACK ACCESS CONTROL [%d]", pkg_filter.ac);
    }
    return 0;
}

void read_filter()
{
    
    /* 
        [o:8002]
        [d:0002]
        [r:80]
    */
    FILE *fp;

    int err;

    fp = fopen("filter.txt", "r");

    if(!fp)
        return;

    char buf[100];

    pkg_filter.inc_op_len = 0;
    pkg_filter.inc_pl_len = 0;
    pkg_filter.ac = 0;

    while(fgets(buf, 30 , fp)) {
        err = set_filter(buf);
        memset(buf, 0, 30);
    }
    
}



struct astruct 
{
    int a;
};


void main(void)
{
    int err;

    set_package_filter(&pkg_filter);

    read_filter();

    process_credentials_init();

    log_init();

    k_delayed_work_init(&mesh_transmit.timer, transmit);

    k_delayed_work_init(&mesh_renew.timer, mesh_renew_work);


    err = bt_enable(bt_ready);

    if (err) {
        
        SYS_LOG_ERR("Bluetooth init failed (err %d)", err);
        return;
    }

    bt_mesh_hack_set(true); 

    k_delayed_work_submit(&file_timer, 1000);

    socket_init(ipc_received);

}

