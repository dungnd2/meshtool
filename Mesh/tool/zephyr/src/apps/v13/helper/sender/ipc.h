#ifndef ZIPC_H
#define ZIPC_H

#define SOCKET_SERVER_PATH "/tmp/us_xfr"

typedef void (*ipc_received_t)(char* msg, int len);

int socket_init(ipc_received_t received_cb);


#endif //ZIPC_H