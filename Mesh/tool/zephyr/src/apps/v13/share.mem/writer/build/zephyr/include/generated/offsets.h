/* THIS FILE IS AUTO GENERATED.  PLEASE DO NOT EDIT.
 *
 * This header file provides macros for the offsets of various structure
 * members.  These offset macros are primarily intended to be used in
 * assembly code.
 */

#ifndef __GEN_OFFSETS_H__
#define __GEN_OFFSETS_H__

#define ___kernel_t_current_OFFSET 0x8
#define ___kernel_t_nested_OFFSET 0x0
#define ___kernel_t_irq_stack_OFFSET 0x4
#define ___cpu_t_current_OFFSET 0x8
#define ___cpu_t_nested_OFFSET 0x0
#define ___cpu_t_irq_stack_OFFSET 0x4
#define ___kernel_t_threads_OFFSET 0x28
#define ___kernel_t_ready_q_OFFSET 0x1c
#define ___kernel_t_arch_OFFSET 0x2c
#define ___ready_q_t_cache_OFFSET 0x0
#define ___thread_base_t_user_options_OFFSET 0x8
#define ___thread_base_t_thread_state_OFFSET 0x9
#define ___thread_base_t_prio_OFFSET 0xa
#define ___thread_base_t_sched_locked_OFFSET 0xb
#define ___thread_base_t_preempt_OFFSET 0xa
#define ___thread_base_t_swap_data_OFFSET 0x10
#define ___thread_t_base_OFFSET 0x0
#define ___thread_t_caller_saved_OFFSET 0x2c
#define ___thread_t_callee_saved_OFFSET 0x2c
#define ___thread_t_arch_OFFSET 0x5c
#define ___thread_t_next_thread_OFFSET 0x50
#define K_THREAD_SIZEOF 0x5c

#endif /* __GEN_OFFSETS_H__ */
