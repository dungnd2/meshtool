cmake_minimum_required(VERSION 3.8.2)



set(BOARD native_posix)

include($ENV{ZEPHYR_BASE}/cmake/app/boilerplate.cmake NO_POLICY_SCOPE)
project(NONE)

target_sources(app PRIVATE ../main.c)
