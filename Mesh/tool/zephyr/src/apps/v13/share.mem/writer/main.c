
#include <sys/ipc.h> 
#include <sys/shm.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>

#define MESSGAE_SEQ_OFF 0  
#define MESSAGE_SEQ_LEN 1

#define MESSAGE_DATA_LEN_OFF 1
#define MESSAGE_DATA_LEN_LEN 2

#define MESSAGE_DATA_OFF 3

#define MESSAGE_DATA_MAX_LEN 200


#define MESSAGE_SEQ(share_mem_ptr) (share_mem_ptr->mem[MESSGAE_SEQ_OFF])

// big endian datalen
#define MESSAGE_DATA_LEN(share_mem_ptr) (share_mem_ptr->mem[MESSAGE_DATA_LEN_OFF] << 8 | share_mem_ptr->mem[MESSAGE_DATA_LEN_OFF + 1])

#define SHARE_MEM_DEFINE(__name, __size) \
    struct share_mem share_mem##__name = {\
        .size = __size,\
    };\

struct share_mem
{
    char* mem;
    u8_t seq;
    u16_t datalen;
    u8_t *data;
    const u16_t size;

};


int share_mem_create(struct share_mem* share_mem)
{
    key_t key = ftok("shmfile",65); 
  
    // shmget returns an identifier in shmid 
    int shmid = shmget(key,share_mem->size,0666|IPC_CREAT); 

    if (shmid < 0)
        return shmid;
  
    // shmat to attach to shared memory 
    char *str = (char*) shmat(shmid,(void*)0,0);
    share_mem->mem = str;

    return 0;

}


int share_mem_get(struct share_mem* share_mem)
{
    u8_t seq = MESSAGE_SEQ(share_mem);
    u16_t datalen = MESSAGE_DATA_LEN(share_mem);

    if (seq == share_mem->seq)
        return -1; // old message
    
    share_mem->seq = seq;
    share_mem->datalen = datalen;

    
}


int main() 
{ 
    // ftok to generate unique key 
    key_t key = ftok("shmfile",65); 
  
    // shmget returns an identifier in shmid 
    int shmid = shmget(key,1024,0666|IPC_CREAT); 
  
    // shmat to attach to shared memory 
    char *str = (char*) shmat(shmid,(void*)0,0); 
  

    printf("writing data \n");

    int id;
    while (1) {
        // printf("Data written in memory: %s\n", str);
        sprintf(str , "hello %d", id++);
        sleep(1);
    }
      
    //detach from shared memory  
    shmdt(str); 
  
    return 0; 
} 