#ifndef GEN_ONOFF_SRV_H
#define GEN_ONOFF_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_gen_onoff_srv {
    struct bt_mesh_model *model;
    void (*const get_func)(struct net_buf_simple* data, void *user_data);
    void (*const set_func)(struct net_buf_simple* data, void *user_data);
    void *user_data;
};

int bt_mesh_gen_onoff_srv_init(struct bt_mesh_model *model, bool primary);

extern const struct bt_mesh_model_op bt_mesh_gen_onoff_srv_op[];

#define BT_MESH_MODEL_GEN_ONOFF_SRV(srv_data, pub) \
        BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_ONOFF_SRV, \
                  bt_mesh_gen_onoff_srv_op, pub, srv_data)

#ifdef __cplusplus
}
#endif

#endif // LIGHTNESS_SRV_H
