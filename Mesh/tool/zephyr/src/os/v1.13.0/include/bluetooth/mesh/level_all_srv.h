#ifndef LEVEL_ALL_SRV_H
#define LEVEL_ALL_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_level_all_srv {
    struct bt_mesh_model *model;
    void (*const ports_set_func)(u32_t porst_enable, u8_t tid, void *user_data);
    void (*const set_func)(u16_t level, u8_t tid, u8_t time, u8_t delay, void *user_data);
    void *user_data;
};

int bt_mesh_level_all_srv_init(struct bt_mesh_model *model, bool primary);

extern const struct bt_mesh_model_op bt_mesh_level_all_srv_op[];

#define BT_MESH_MODEL_LEVEL_ALL_SRV(srv_data) \
        BT_MESH_MODEL(BT_MESH_MODEL_ID_LEVEL_ALL_SRV, \
                  bt_mesh_level_all_srv_op, NULL, srv_data)

#ifdef __cplusplus
}
#endif

#endif // LEVEL_ALL_SRV_H
