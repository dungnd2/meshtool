#ifndef VNG_MESH_CONFIG_SRV_H
#define VNG_MESH_CONFIG_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

#define CID_VNG 								0x00FF
#define GROUP_GATEWAY_CTRL_DEFAULT				0xC000
#define GROUP_STATUS_CTRL_DEFAULT				0xC001
#define GROUP_STATUS_DEST_DEFAULT				0xC002
#define GROUP_LOW_PRITORITY_MIN_DEFAULT			0xC003
#define GROUP_LOW_PRITORITY_MAX_DEFAULT			0xC004


struct bt_mesh_vng_config_srv {
    struct bt_mesh_model *model;
    void (*const notify_func)(struct net_buf_simple* data, void *user_data);
    void *user_data;

    struct __packed {

		struct __packed {
			u16_t min;
			u16_t max;
		} priorities;

		u16_t gateway_ctrl;

		struct __packed {
			u16_t ctrl;
			u16_t dest;
		} status;
	} groups;

};


/**
 * @brief Inititialize vng configuation server.
 *
 * @param [modeo] model ctx.
 *
 * @retval 0 If successful.
 * @retval Negative errno code if failure.
 */

int bt_mesh_vng_config_srv_init(struct bt_mesh_model *model);



/**
 * @brief Commit setting restored from flash.
 *		  Should be called after setting_load().
 *
 * @retval 0 If successful.
 * @retval Negative errno code if failure.
 */

int bt_mesh_vng_config_setting_commit();




/**
 * @brief Trigger to do configuration registered models.
 *
 * @retval 0 If successful.
 * @retval Negative errno code if failure.
 */

int bt_mesh_vng_config_do_config();




/**
 * @brief Register configuration model-publishing.
 *
 * @param [group] group address.
 * @param [model] model ctx.
 *
 * @retval 0 If successful.
 * @retval Negative errno code if failure.
 */

int bt_mesh_vng_group_publish_register(u16_t group, struct bt_mesh_model *model);


/**
 * @brief Register configuration model-subcribing.
 *		  
 * @param [group] group address.
 * @param [model] model ctx.
 * @param [only] if this model just subcribe to only 1 group.
 *
 * @retval 0 If successful.
 * @retval Negative errno code if failure.
 */

int bt_mesh_vng_group_subcribe_register(u16_t group, struct bt_mesh_model *model, bool only);



/**
 * @brief Get mesh low prioritied groups.
 *		  
 * @param [min] the smallest group.
 * @param [max] the greatest group.
 *
 * @retval 0 If successful.
 * @retval Negative errno code if failure.
 */

int bt_mesh_vng_prioritied_groups_get(u16_t* min, u16_t* max);



extern const struct bt_mesh_model_op bt_mesh_vng_config_srv_op[];

#define BT_MESH_MODEL_VNG_CONFIG_SRV(srv_data, pub) \
        BT_MESH_MODEL_VND(CID_VNG, BT_MESH_MODEL_ID_VNG_CONFIG_SRV, \
                  bt_mesh_vng_config_srv_op, pub, srv_data)

#ifdef __cplusplus
}
#endif

#endif // VNG_MESH_CONFIG_SRV_H