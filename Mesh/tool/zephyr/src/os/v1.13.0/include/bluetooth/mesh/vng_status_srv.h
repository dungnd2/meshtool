#ifndef VNG_STATUS_SRV_H
#define VNG_STATUS_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

#define CID_VNG 0x00FF

struct bt_mesh_vng_status_srv {
    struct bt_mesh_model *model;
    void (*const group_set_func)(u16_t ctrl_group, u16_t dest_group, void *user_data);
    void (*const set_func)(struct net_buf_simple* vng_data, void *user_data);
    void *user_data;
};

int bt_mesh_vng_status_srv_init(struct bt_mesh_model *model);

extern const struct bt_mesh_model_op bt_mesh_vng_status_srv_op[];

#define BT_MESH_MODEL_VNG_STATUS_SRV(srv_data, pub) \
        BT_MESH_MODEL_VND(CID_VNG, BT_MESH_MODEL_ID_VNG_STATUS_SRV, \
                  bt_mesh_vng_status_srv_op, pub, srv_data)

#ifdef __cplusplus
}
#endif

#endif // VNG_STATUS_SRV_H