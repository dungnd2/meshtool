#ifndef LEVEL_SRV_H
#define LEVEL_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_gen_level_srv {
    struct bt_mesh_model *model;
    void (*const get_func)(u16_t *present, u16_t *target, u8_t *remain, void *user_data);
    void (*const set_func)(u16_t level, u8_t tid, u8_t time, u8_t delay, void *user_data);
    void *user_data;
};

int bt_mesh_gen_level_srv_init(struct bt_mesh_model *model, bool primary);

extern const struct bt_mesh_model_op bt_mesh_gen_level_srv_op[];

#define BT_MESH_MODEL_GEN_LEVEL_SRV(srv_data, pub) \
        BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_LEVEL_SRV, \
                  bt_mesh_gen_level_srv_op, pub, srv_data)

#ifdef __cplusplus
}
#endif

#endif // LEVEL_SRV_H
