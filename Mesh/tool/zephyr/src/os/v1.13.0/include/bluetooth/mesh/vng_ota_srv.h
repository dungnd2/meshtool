#ifndef VNG_OTA_SRV_H
#define VNG_OTA_SRV_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_vng_ota_srv {
    struct bt_mesh_model *model;
    void (*const set_func)(struct net_buf_simple* vng_data, void *user_data);
    void (*const get_func)(struct net_buf_simple* vng_data, void *user_data);
    void *user_data;
};

int bt_mesh_vng_ota_srv_init(struct bt_mesh_model *model, bool primary);


extern const struct bt_mesh_model_op bt_mesh_vng_ota_srv_op[];

#define BT_MESH_MODEL_VNG_OTA_SRV(srv_data, pub) \
        BT_MESH_MODEL(BT_MESH_MODEL_ID_VNG_OTA_SRV, \
                  bt_mesh_vng_ota_srv_op, pub, srv_data)

#ifdef __cplusplus
}
#endif

#endif // VNG_OTA_SRV_H