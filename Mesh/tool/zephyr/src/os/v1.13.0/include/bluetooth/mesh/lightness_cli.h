#ifndef LIGHTNESS_CLI_H
#define LIGHTNESS_CLI_H

#ifdef __cplusplus
extern "C" {
#endif

struct bt_mesh_lightness_cli {
    struct bt_mesh_model *model;
    void (*const func)(u16_t addr, u16_t lightness, void *user_data);
    void *user_data;
};

int bt_mesh_light_lightness_cli_init(struct bt_mesh_model *model, bool primary);

extern const struct bt_mesh_model_op bt_mesh_light_lightness_cli_op[];

#define BT_MESH_MODEL_LIGHT_LIGHTNESS_CLI(cli_data) \
        BT_MESH_MODEL(BT_MESH_MODEL_ID_LIGHT_LIGHTNESS_CLI, \
                  bt_mesh_light_lightness_cli_op, NULL, cli_data)

#ifdef __cplusplus
}
#endif

#endif // LIGHTNESS_CLI_H
