/* userchan.c - HCI User Channel based Bluetooth driver */

/*
 * Copyright (c) 2018 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <init.h>
#include <board.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <errno.h>
#include <stddef.h>
#include <stdlib.h>
#include <poll.h>
#include <errno.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

#include "soc.h"
#include "cmdline.h" /* native_posix command line options header */

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_driver.h>
#include <bluetooth/mesh/main.h>

#include <host/mesh/dbus.h>

// #include <file.h>

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_DEBUG_HCI_DRIVER)
#include "common/log.h"

#define BTPROTO_HCI      1
struct sockaddr_hci {
	sa_family_t     hci_family;
	unsigned short  hci_dev;
	unsigned short  hci_channel;
};
#define HCI_CHANNEL_USER 1

#define SOL_HCI          0

#define H4_CMD           0x01
#define H4_ACL           0x02
#define H4_SCO           0x03
#define H4_EVT           0x04

static BT_STACK_NOINIT(rx_thread_stack,
		       CONFIG_ARCH_POSIX_RECOMMENDED_STACK_SIZE);
static struct k_thread rx_thread_data;

static int uc_fd = -1;

// static int bt_dev_index = -1;
static int bt_dev_index = 0;

static struct net_buf *get_rx(const u8_t *buf)
{
	if (buf[0] == H4_EVT && (buf[1] == BT_HCI_EVT_CMD_COMPLETE ||
				 buf[1] == BT_HCI_EVT_CMD_STATUS)) {
		// BT_ERR("bt_buf_get_cmd_complete()");
		return bt_buf_get_cmd_complete(K_FOREVER);
	}

	if (buf[0] == H4_ACL) {
		// BT_ERR("bt_buf_get_rx(H4_ACL)");
		return bt_buf_get_rx(BT_BUF_ACL_IN, K_FOREVER);
	} else {
		// BT_ERR("bt_buf_get_rx(else)");
		return bt_buf_get_rx(BT_BUF_EVT, K_FOREVER);
	}
}

static bool uc_ready(void)
{
	struct pollfd pollfd = { .fd = uc_fd, .events = POLLIN };

	return (poll(&pollfd, 1, 0) == 1);
}



// slaves handle master-broadcast message
extern void uc_slave_handle_frame(const u8_t* frame, size_t len)
{
	struct net_buf *buf;

	buf = get_rx(frame);
	net_buf_add_mem(buf, &frame[1], len - 1);

	
	if (frame[0] == H4_EVT && bt_hci_evt_is_prio(frame[1])) {
		// BT_ERR("bt_recv_prio(%p)", buf);
		bt_recv_prio(buf);
	} else {
		// BT_ERR("bt_recv(%p)", buf);
		bt_recv(buf);
	}
}

// master handle (tosend-message) from slaves
extern int uc_master_handle_frame(const u8_t* data, int len)
{
	if (uc_fd < 0) {
		BT_ERR("User channel not open");
		return -EIO;
	}

	if (write(uc_fd, data, len) < 0) {
		return -errno;
	}

	return 0;
}


static void rx_thread(void *p1, void *p2, void *p3)
{
	ARG_UNUSED(p1);
	ARG_UNUSED(p2);
	ARG_UNUSED(p3);

	BT_DBG("started");

	while (1) {
		static u8_t frame[512];
		struct net_buf *buf;
		ssize_t len;

		if (!uc_ready()) {
			k_sleep(K_MSEC(20));
			continue;
		}

		BT_DBG("calling read()");

		len = read(uc_fd, frame, sizeof(frame));

		if (len < 0) {
			if (errno == EINTR) {
				k_yield();
				continue;
			}

			BT_ERR("Reading socket failed, errno %d", errno);
			close(uc_fd);
			uc_fd = -1;
			return;
		}

#ifdef CONFIG_BT_MESH_DBUS_MASTER
		dbus_mesh_master_broadcast(frame, len);
#endif

		buf = get_rx(frame);
		net_buf_add_mem(buf, &frame[1], len - 1);

		BT_DBG("Calling bt_recv(%p)", buf);

		if (frame[0] == H4_EVT && bt_hci_evt_is_prio(frame[1])) {
			bt_recv_prio(buf);
		} else {
			bt_recv(buf);
		}

		k_yield();
	}
}


static int uc_send(struct net_buf *buf)
{

	int error = 0;
	
	BT_DBG("buf %p type %u len %u", buf, bt_buf_get_type(buf), buf->len);

#ifndef CONFIG_BT_MESH_DBUS_SLAVE
	if (uc_fd < 0) {
		BT_ERR("User channel not open");
		return -EIO;
	}
#endif

	switch (bt_buf_get_type(buf)) {
	case BT_BUF_ACL_OUT:
		net_buf_push_u8(buf, H4_ACL);
		break;
	case BT_BUF_CMD:
		net_buf_push_u8(buf, H4_CMD);
		break;
	default:
		BT_ERR("Unknown buffer type");
		return -EINVAL;
	}

#ifdef CONFIG_BT_USERCHAN_FILE_LOG
	BT_ERR("HCI WRITE:[%s]", bt_hex(buf->data, buf->len));
	log_hex(buf->data, buf->len);
#endif


#ifdef CONFIG_BT_MESH_DBUS_SLAVE

	error =  dbus_mesh_slave_forward(buf);
#else
	if (write(uc_fd, buf->data, buf->len) < 0) {
		return -errno;
	}

	// uc_dbus_echo(buf->data, buf->len, true);
#endif
	net_buf_unref(buf);
	return error;
}

static int user_chan_open(u16_t index)
{

	BT_DBG("User chan opening...");
	struct sockaddr_hci addr;
	int fd;

	fd = socket(PF_BLUETOOTH, SOCK_RAW | SOCK_CLOEXEC | SOCK_NONBLOCK,
		    BTPROTO_HCI);
	if (fd < 0) {
		BT_ERR("Socket open failed, err: %d", -errno);
		return -errno;
	}

	memset(&addr, 0, sizeof(addr));
	addr.hci_family = AF_BLUETOOTH;
	addr.hci_dev = index;
	addr.hci_channel = HCI_CHANNEL_USER;

	if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		int err = -errno;

		close(fd);
		// BT_ERR("bind failed, err: %d", err);
		perror("bind failed. Error");
		BT_DBG("->fix: sudo hciconfig hci0 down");
		return err;
	}

	return fd;
}

static int uc_open(void)
{

	
#ifdef CONFIG_BT_MESH_DBUS	

	int error = 0;
	
	error = dbus_mesh_init();
	if (error) {
		return error;
	}
	error = dbus_mesh_start_listen();

	if (error)
		return error;

#endif 

#ifdef CONFIG_BT_MESH_DBUS_SLAVE
	// slave to not need hci driver
	return 0;

#else  // MASTER OR NOT DBUS

	if (bt_dev_index < 0) {
		BT_ERR("No Bluetooth device specified");
		return -ENODEV;
	}

	BT_DBG("hci%d", bt_dev_index);

	uc_fd = user_chan_open(bt_dev_index);
	if (uc_fd < 0) {
		return uc_fd;
	}

	BT_DBG("User Channel opened as fd %d", uc_fd);

	k_thread_create(&rx_thread_data, rx_thread_stack,
			K_THREAD_STACK_SIZEOF(rx_thread_stack),
			rx_thread, NULL, NULL, NULL,
			K_PRIO_COOP(CONFIG_BT_RX_PRIO - 1),
			0, K_NO_WAIT);

	BT_DBG("returning");

	return 0;

#endif 

	
}

static const struct bt_hci_driver drv = {
	.name		= "HCI User Channel",
	.bus		= BT_HCI_DRIVER_BUS_UART,
	.open		= uc_open,
	.send		= uc_send,
};

static int _bt_uc_init(struct device *unused)
{
	ARG_UNUSED(unused);

	bt_hci_driver_register(&drv);

	return 0;
}

SYS_INIT(_bt_uc_init, POST_KERNEL, CONFIG_KERNEL_INIT_PRIORITY_DEVICE);

static void cmd_bt_dev_found(char *argv, int offset)
{
	if (strncmp(&argv[offset], "hci", 3) || strlen(&argv[offset]) < 4) {
		posix_print_error_and_exit("Error: Invalid Bluetooth device "
					   "name '%s' (should be e.g. hci0)\n",
					   &argv[offset]);
		// return;
		bt_dev_index = 0;
	} else {

		bt_dev_index = strtol(&argv[offset + 3], NULL, 10);
	}

	
}

static void cmd_bt_dbg_found(char *argv, int offset)
{

	static const u8_t ARGVS_LEN[] = {3, 7, 11, 15};
	static const char* argvs_name[] = {"adv", "net", "tra", "acc"};

	bool valid_len = false;
	u8_t argv_len = strlen(&argv[offset]);
	u8_t args = 0;
	char* options = &argv[offset];
	bool layers[ARRAY_SIZE(ARGVS_LEN)] = {false, false, false, false};

	printf("__________________________________\n\n");
	printf("Arg otion bt-dbg:[%s], len: %u\n", &argv[offset], argv_len);
	
	for(int i = 0; i < ARRAY_SIZE(ARGVS_LEN); i++) {
		if(argv_len == ARGVS_LEN[i]) {
			valid_len = true;
			args = i + 1;
			break;
		}
	}

	if(!valid_len) {
		perror("Arg bt-dbg invalid len");
		return;
	}

	for(int i = 0; i < args; i++) {
		
		for(int k = 0; k < ARRAY_SIZE(argvs_name); k++) {
			if(strncmp(options + ARGVS_LEN[i] - 3, argvs_name[k], 3) == 0) {
				printf("Has %s, layer %u\n", argvs_name[k], k);
				layers[k] = true;
			}
		}
	}

	if(layers[0]) {
		bt_mesh_debug_set(MESH_DEBUG_ADV, true);
	}

	if(layers[1]) {
		bt_mesh_debug_set(MESH_DEBUG_NET, true);
	}

	if(layers[2]) {
		bt_mesh_debug_set(MESH_DEBUG_TRAN, true);
	}

	if(layers[3]) {
		bt_mesh_debug_set(MESH_DEBUG_ACCESS, true);
	}

	printf("__________________________________\n");

}

static void cmd_mesh_addr_found(char *argv, int offset)
{
	u8_t argv_len = strlen(&argv[offset]);

	printf("\n__________________________________\n\n");
	printf("Arg otion addr:[%s], len: %u\n", &argv[offset], argv_len);
	printf("\n__________________________________\n\n");

}

static void add_btuserchan_arg(void)
{
	static struct args_struct_t btuserchan_args[] = {
		/*
		 * Fields:
		 * manual, mandatory, switch,
		 * option_name, var_name ,type,
		 * destination, callback,
		 * description
		 */
		{ false, true, false,
		"bt-dev", "hciX", 's',
		NULL, cmd_bt_dev_found,
		"A local HCI device to be used for Bluetooth (e.g. hci0)" },

		{ false, true, false,
		"bt-dbg", "adv|net|tra|acc", 's',
		NULL, cmd_bt_dbg_found,
		"Specify to enable debug for mesh layers" },

		{ false, true, false,
		"bt-addr", "7ea0", 's',
		NULL, cmd_mesh_addr_found,
		"Specify the mesh unicast address" },

		ARG_TABLE_ENDMARKER
	};

	native_add_command_line_opts(btuserchan_args);
}

static void btuserchan_check_arg(void)
{
	if (bt_dev_index < 0) {
		posix_print_error_and_exit("Error: Bluetooth device missing. "
					   "Specify one using --bt-dev=hciN\n");
	}
}

NATIVE_TASK(add_btuserchan_arg, PRE_BOOT_1, 10);
NATIVE_TASK(btuserchan_check_arg, PRE_BOOT_2, 10);
