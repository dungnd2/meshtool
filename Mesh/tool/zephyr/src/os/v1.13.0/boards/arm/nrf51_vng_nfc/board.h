/*
 * Copyright (c) 2017 VNG IoT Lab Limited.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>

#define PIN_BYPASS          16
#define PIN_BLE_LNA_EN 		17
#define PIN_LED_RED_CTRL   9
#define PIN_LED_GREEN_CTRL 10
#define PIN_BUZZER_CTRL    8


#endif /* __INC_BOARD_H */
