/*
 * Copyright (c) 2017 VNG IoT Lab Limited.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>

/* Button*/
#define PIN_BYPASS 18
#define PIN_SWITCH 17

#endif /* __INC_BOARD_H */
