/*
 * Copyright (c) 2016-2018 Nordic Semiconductor ASA.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>

#define NFC_WAKE_UP_PIN 1
#define PIN_BYPASS 16 
#define PIN_BLE_LNA_EN 17

#endif /* __INC_BOARD_H */
