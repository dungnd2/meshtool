/*
 * Copyright (c) 2017 VNG IoT Lab Limited.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>


/* hub pins */

#define HUB_LINE01_PIN   11
#define HUB_LINE02_PIN   10
#define HUB_LINE03_PIN   6
#define HUB_LINE04_PIN   9
#define HUB_LINE05_PIN   8
#define HUB_LINE06_PIN   27
#define HUB_LINE07_PIN   26
#define HUB_LINE08_PIN   25

// #define HUB_LINE01_EN   10
// #define HUB_LINE02_EN   8
// #define HUB_LINE03_EN   6
// #define HUB_LINE04_EN   4
// #define HUB_LINE05_EN   2
// #define HUB_LINE06_EN   30
// #define HUB_LINE07_EN   28
// #define HUB_LINE08_EN   26

#define HUB_LINE01_ADC	3
#define HUB_LINE02_ADC	2	
#define HUB_LINE03_ADC	1	
#define HUB_LINE04_ADC	0
#define HUB_LINE05_ADC	7
#define HUB_LINE06_ADC	6
#define HUB_LINE07_ADC	5
#define HUB_LINE08_ADC	4

#define PIN_BYPASS      22

#define HW_WDT_TOGGLE	20
#define SW_MODE_EN_L	7

#endif /* __INC_BOARD_H */
