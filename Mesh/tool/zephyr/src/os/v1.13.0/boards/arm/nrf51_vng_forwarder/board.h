/*
 * Copyright (c) 2017 VNG IoT Lab Limited.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>

#define SUPPLY_USB 0

#if SUPPLY_USB
	#define PIN_BYPASS          19
	#define PIN_BLE_LNA_EN 		20
#else 
	#define PIN_BYPASS          16
	#define PIN_BLE_LNA_EN 		27
#endif


#endif /* __INC_BOARD_H */
