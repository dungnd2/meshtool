# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/posix/soc/inf_clock/linker.ld" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/scan_adv/build/zephyr/linker.cmd"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/posix/include"
  "../../../../arch/posix/soc/inf_clock"
  "../../../../arch/posix/soc/inf_clock/include"
  "../../../../arch/posix/soc/include"
  "../../../../boards/posix/native_posix"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
