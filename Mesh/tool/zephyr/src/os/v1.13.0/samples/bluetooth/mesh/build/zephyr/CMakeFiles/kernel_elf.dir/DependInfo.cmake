# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/isr_tables.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/CMakeFiles/kernel_elf.dir/isr_tables.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/misc/empty_file.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/CMakeFiles/kernel_elf.dir/misc/empty_file.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "NRF51"
  "NRF51822"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf/nrf51"
  "../../../../arch/arm/soc/nordic_nrf/nrf51/include"
  "../../../../arch/arm/soc/nordic_nrf/include"
  "../../../../boards/arm/nrf51_vbluno51"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/settings/include"
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/CMakeFiles/app.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/CMakeFiles/zephyr.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/arch/arm/core/cortex_m/CMakeFiles/arch__arm__core__cortex_m.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/common/CMakeFiles/subsys__bluetooth__common.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/host/mesh/CMakeFiles/subsys__bluetooth__host__mesh.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/controller/CMakeFiles/subsys__bluetooth__controller.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/net/CMakeFiles/subsys__net.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/drivers/gpio/CMakeFiles/drivers__gpio.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/drivers/flash/CMakeFiles/drivers__flash.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/drivers/serial/CMakeFiles/drivers__serial.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/drivers/entropy/CMakeFiles/drivers__entropy.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/kernel/CMakeFiles/kernel.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
