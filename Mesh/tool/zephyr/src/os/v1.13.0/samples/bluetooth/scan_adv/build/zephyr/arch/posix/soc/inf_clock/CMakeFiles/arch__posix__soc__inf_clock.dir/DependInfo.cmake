# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/posix/soc/inf_clock/soc.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/scan_adv/build/zephyr/arch/posix/soc/inf_clock/CMakeFiles/arch__posix__soc__inf_clock.dir/soc.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "_FORTIFY_SOURCE=2"
  "_POSIX_CHEATS_H"
  "_POSIX_C_SOURCE=200809"
  "_XOPEN_SOURCE"
  "_XOPEN_SOURCE_EXTENDED"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/posix/include"
  "../../../../arch/posix/soc/inf_clock"
  "../../../../arch/posix/soc/inf_clock/include"
  "../../../../arch/posix/soc/include"
  "../../../../boards/posix/native_posix"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
