/* main.c - Application main entry point */

/*
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <misc/printk.h>
#include <misc/util.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <gpio.h>
#include <board.h>
#include "common/log.h"
#include <net/buf.h>

#define BUTTON_PIN 13

#define ADV_DATA_SIZE 8

#define BT_MESH_BUF_SIZE 36
#define BT_MESH_USER_DATA_MIN 4
#if defined(CONFIG_BT_MESH_BUFFERS)
#define MESH_BUF_COUNT CONFIG_BT_MESH_BUFFERS
#else
#define MESH_BUF_COUNT 20
#endif

NET_BUF_POOL_DEFINE(mesh_pool,
                    MESH_BUF_COUNT,
                    BT_MESH_BUF_SIZE,
                    BT_MESH_USER_DATA_MIN,
                    NULL);

static BT_STACK_NOINIT(tx_thread_stack, 1024);
static struct k_thread tx_thread_handle;
static K_FIFO_DEFINE(tx_queue);


static u8_t adv_data[ADV_DATA_SIZE] = {};

static char dev_name[] = "GATEWAY_UART";
static const struct bt_data ad[] = {
	BT_DATA(BT_DATA_MANUFACTURER_DATA, adv_data, ADV_DATA_SIZE),
	BT_DATA(BT_DATA_NAME_COMPLETE, dev_name, sizeof(dev_name)),
};

static struct bt_le_adv_param adv_param = {
		.options = BT_LE_ADV_OPT_USE_NAME | BT_LE_ADV_OPT_USE_IDENTITY,
		// .options = BT_LE_ADV_OPT_USE_NAME,
		.interval_min = BT_GAP_ADV_FAST_INT_MIN_1,
		.interval_max = BT_GAP_ADV_FAST_INT_MAX_1,
	};

static struct gpio_callback gpio_cb;



static void scan_cb(const bt_addr_le_t *addr, s8_t rssi, u8_t adv_type,
		    struct net_buf_simple *buf)
{
	//do nothing;
	printk("Scan cb\n");

}

static void set_adv_data(const u8_t* payload, u16_t len)
{
	if(len > ADV_DATA_SIZE) {
		printk("Data len is too big\n");
		return;
	}
	memcpy(adv_data, payload, len);
}




static void mesh_transmit(struct net_buf* buf)
{
	printk("Mesh transmitting...\n");
	int err;


	// Set payload here

	//set_adv_data(payload, len);

	err = bt_le_adv_start(&adv_param, ad, ARRAY_SIZE(ad),
			      NULL, 0);
	if (err) {
		printk("Advertising failed to start 000 (err %d)\n", err);
		return;
	}
	k_sleep(K_MSEC(400));
	err = bt_le_adv_stop();
	if (err) {
		printk("Advertising failed to stop (err %d)\n", err);
		return;
	}
}


static void tx_thread(void *p1, void *p2, void *p3)
{
    while (1) {
        struct net_buf *buf;
        /* Wait until a buffer is available */
        buf = net_buf_get(&tx_queue, K_FOREVER);

        mesh_transmit(buf);

        net_buf_unref(buf);

        /* Give other threads a chance to run if _tx_queue keeps getting
         * new data all the time.
         */
        k_yield();
    }
}



static void button_pressed(struct device *gpiob, struct gpio_callback *cb,
		    u32_t pins)
{
	printk("Button pressed at %d\n", k_cycle_get_32());
	struct net_buf* buf = net_buf_alloc(&mesh_pool, K_NO_WAIT);
	if(buf == NULL) {
		printk("Out of pool\n");
		return;
	}

	net_buf_add_u8(buf, 1);
	net_buf_put(&tx_queue, buf);
}

// static void button_init()
// {
// 	printk("Button %u initializing...\n", BUTTON_PIN);
// 	struct device* gpiob;
// 	gpiob = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);
// 	if (!gpiob) {
// 		printk("error\n");
// 		return;
// 	}

// 	gpio_pin_configure(gpiob, BUTTON_PIN,
// 			   GPIO_DIR_IN | GPIO_PUD_PULL_UP | GPIO_INT | GPIO_INT_EDGE | GPIO_INT_ACTIVE_LOW);

// 	gpio_init_callback(&gpio_cb, button_pressed, BIT(BUTTON_PIN));

// 	gpio_add_callback(gpiob, &gpio_cb);
// 	gpio_pin_enable_callback(gpiob, BUTTON_PIN);
// }



// struct k_delayed_work wdt_timer;
// struct device* gpio;

// static void hw_wdt_feed(struct k_work* work)
// {
// 	static u32_t state = 0;

//     gpio_pin_write(gpio, 20, state);
//     state = !state;
//     k_delayed_work_submit(&wdt_timer, 500);
// }

// void hw_wdt_init()
// {
// 	gpio = device_get_binding(CONFIG_GPIO_P0_DEV_NAME);
// 	if(!gpio) {
// 		return;
// 	}
// 	gpio_pin_configure(gpio, 20, GPIO_DIR_OUT);

// 	k_delayed_work_init(&wdt_timer, hw_wdt_feed);
// 	k_delayed_work_submit(&wdt_timer, 500);
// }

static void scan_init()
{
	int err;
	struct bt_le_scan_param scan_param = {
		.type       = BT_HCI_LE_SCAN_PASSIVE,
		.filter_dup = BT_HCI_LE_SCAN_FILTER_DUP_DISABLE,
		.interval   = 0x0010,
		.window     = 0x0010,
	};

	err = bt_le_scan_start(&scan_param, scan_cb);
	if (err) {
		printk("Starting scanning failed (err %d)\n", err);
		return;
	}

}

void main(void)
{

	int err;

	// hw_wdt_init();

	printk("Starting Scanner/Advertiser Demo\n");

	/* Initialize the Bluetooth Subsystem */
	err = bt_enable(NULL);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	scan_init();

	// button_init();

	printk("Bluetooth initialized\n");

	k_thread_create(&tx_thread_handle,
                    tx_thread_stack,
                    K_THREAD_STACK_SIZEOF(tx_thread_stack),
                    tx_thread,
                    NULL,
                    NULL,
                    NULL,
                    K_PRIO_COOP(7),
                    0,
                    K_NO_WAIT);


	while(1) {
		button_pressed(NULL, NULL, 20);
		k_sleep(1000);
	}

}