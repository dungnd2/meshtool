# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/subsys/bluetooth/host/att.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/att.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/subsys/bluetooth/host/conn.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/conn.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/subsys/bluetooth/host/gatt.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/gatt.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/subsys/bluetooth/host/hci_core.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/hci_core.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/subsys/bluetooth/host/hci_ecc.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/hci_ecc.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/subsys/bluetooth/host/l2cap.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/l2cap.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/subsys/bluetooth/host/smp_null.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/smp_null.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/subsys/bluetooth/host/uuid.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/bluetooth/mesh/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/uuid.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "NRF51"
  "NRF51822"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf/nrf51"
  "../../../../arch/arm/soc/nordic_nrf/nrf51/include"
  "../../../../arch/arm/soc/nordic_nrf/include"
  "../../../../boards/arm/nrf51_vbluno51"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/settings/include"
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
