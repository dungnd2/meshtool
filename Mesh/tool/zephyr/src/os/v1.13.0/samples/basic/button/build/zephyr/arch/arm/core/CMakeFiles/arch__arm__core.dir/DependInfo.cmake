# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/cpu_idle.S" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/cpu_idle.S.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/exc_exit.S" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/exc_exit.S.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/fault_s.S" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/fault_s.S.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/irq_relay.S" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/irq_relay.S.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/isr_wrapper.S" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/isr_wrapper.S.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/swap_helper.S" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/swap_helper.S.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "KERNEL"
  "NRF51"
  "NRF51822"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf/nrf51"
  "../../../../arch/arm/soc/nordic_nrf/nrf51/include"
  "../../../../arch/arm/soc/nordic_nrf/include"
  "../../../../boards/arm/nrf51_vbluno51"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/fatal.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/fatal.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/fault.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/fault.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/irq_init.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/irq_init.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/irq_manage.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/irq_manage.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/swap.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/swap.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/sys_fatal_error_handler.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/sys_fatal_error_handler.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/thread.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/thread.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/arch/arm/core/thread_abort.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/thread_abort.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "NRF51"
  "NRF51822"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf/nrf51"
  "../../../../arch/arm/soc/nordic_nrf/nrf51/include"
  "../../../../arch/arm/soc/nordic_nrf/include"
  "../../../../boards/arm/nrf51_vbluno51"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
