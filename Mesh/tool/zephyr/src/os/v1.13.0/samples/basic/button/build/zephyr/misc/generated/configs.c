/* file is auto-generated, do not modify ! */

#include <toolchain.h>

GEN_ABS_SYM_BEGIN (_ConfigAbsSyms)

GEN_ABSOLUTE_SYM(CONFIG_BOARD, 1);
GEN_ABSOLUTE_SYM(CONFIG_UART_0_NRF_TX_PIN, 10);
GEN_ABSOLUTE_SYM(CONFIG_UART_0_NRF_RX_PIN, 11);
GEN_ABSOLUTE_SYM(CONFIG_UART_0_NRF_RTS_PIN, 12);
GEN_ABSOLUTE_SYM(CONFIG_UART_0_NRF_CTS_PIN, 13);
GEN_ABSOLUTE_SYM(CONFIG_SOC, 1);
GEN_ABSOLUTE_SYM(CONFIG_SOC_SERIES, 1);
GEN_ABSOLUTE_SYM(CONFIG_NUM_IRQS, 26);
GEN_ABSOLUTE_SYM(CONFIG_SYS_CLOCK_HW_CYCLES_PER_SEC, 32768);
GEN_ABSOLUTE_SYM(CONFIG_ISR_STACK_SIZE, 2048);
GEN_ABSOLUTE_SYM(CONFIG_SYS_POWER_MANAGEMENT, 1);
GEN_ABSOLUTE_SYM(CONFIG_BUILD_OUTPUT_HEX, 1);
GEN_ABSOLUTE_SYM(CONFIG_GPIO, 1);
GEN_ABSOLUTE_SYM(CONFIG_GPIO_NRFX, 1);
GEN_ABSOLUTE_SYM(CONFIG_SYS_CLOCK_TICKS_PER_SEC, 100);
GEN_ABSOLUTE_SYM(CONFIG_TEXT_SECTION_OFFSET, 0x0);
GEN_ABSOLUTE_SYM(CONFIG_BOARD_NRF51_VBLUNO51, 1);
GEN_ABSOLUTE_SYM(CONFIG_SOC_SERIES_NRF51X, 1);
GEN_ABSOLUTE_SYM(CONFIG_SOC_FAMILY, 1);
GEN_ABSOLUTE_SYM(CONFIG_SOC_FAMILY_NRF, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_ADC, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_CCM, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_ECB, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_GPIO0, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_GPIOTE, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_LPCOMP, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_PPI, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_QDEC, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_RNG, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_RTC0, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_RTC1, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_SPI0, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_SPI1, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_SPIS1, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_TEMP, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_TIMER0, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_TIMER1, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_TIMER2, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_TWI0, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_TWI1, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_UART0, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_HW_NRF_WDT, 1);
GEN_ABSOLUTE_SYM(CONFIG_SOC_NRF51822_QFAC, 1);
GEN_ABSOLUTE_SYM(CONFIG_CPU_CORTEX, 1);
GEN_ABSOLUTE_SYM(CONFIG_CPU_CORTEX_M, 1);
GEN_ABSOLUTE_SYM(CONFIG_CPU_CORTEX_M0, 1);
GEN_ABSOLUTE_SYM(CONFIG_ISA_THUMB2, 1);
GEN_ABSOLUTE_SYM(CONFIG_ARMV6_M_ARMV8_M_BASELINE, 1);
GEN_ABSOLUTE_SYM(CONFIG_LDREX_STREX_AVAILABLE, 1);
GEN_ABSOLUTE_SYM(CONFIG_DATA_ENDIANNESS_LITTLE, 1);
GEN_ABSOLUTE_SYM(CONFIG_STACK_ALIGN_DOUBLE_WORD, 1);
GEN_ABSOLUTE_SYM(CONFIG_FAULT_DUMP, 2);
GEN_ABSOLUTE_SYM(CONFIG_XIP, 1);
GEN_ABSOLUTE_SYM(CONFIG_GEN_ISR_TABLES, 1);
GEN_ABSOLUTE_SYM(CONFIG_ARCH, 1);
GEN_ABSOLUTE_SYM(CONFIG_ARCH_DEFCONFIG, 1);
GEN_ABSOLUTE_SYM(CONFIG_ARM, 1);
GEN_ABSOLUTE_SYM(CONFIG_GEN_IRQ_VECTOR_TABLE, 1);
GEN_ABSOLUTE_SYM(CONFIG_GEN_SW_ISR_TABLE, 1);
GEN_ABSOLUTE_SYM(CONFIG_GEN_IRQ_START_VECTOR, 0);
GEN_ABSOLUTE_SYM(CONFIG_ARCH_HAS_THREAD_ABORT, 1);
GEN_ABSOLUTE_SYM(CONFIG_SYS_POWER_LOW_POWER_STATE_SUPPORTED, 1);
GEN_ABSOLUTE_SYM(CONFIG_MULTITHREADING, 1);
GEN_ABSOLUTE_SYM(CONFIG_NUM_COOP_PRIORITIES, 16);
GEN_ABSOLUTE_SYM(CONFIG_NUM_PREEMPT_PRIORITIES, 15);
GEN_ABSOLUTE_SYM(CONFIG_MAIN_THREAD_PRIORITY, 0);
GEN_ABSOLUTE_SYM(CONFIG_COOP_ENABLED, 1);
GEN_ABSOLUTE_SYM(CONFIG_PREEMPT_ENABLED, 1);
GEN_ABSOLUTE_SYM(CONFIG_PRIORITY_CEILING, 0);
GEN_ABSOLUTE_SYM(CONFIG_NUM_METAIRQ_PRIORITIES, 0);
GEN_ABSOLUTE_SYM(CONFIG_MAIN_STACK_SIZE, 1024);
GEN_ABSOLUTE_SYM(CONFIG_IDLE_STACK_SIZE, 256);
GEN_ABSOLUTE_SYM(CONFIG_ERRNO, 1);
GEN_ABSOLUTE_SYM(CONFIG_SCHED_DUMB, 1);
GEN_ABSOLUTE_SYM(CONFIG_WAITQ_DUMB, 1);
GEN_ABSOLUTE_SYM(CONFIG_BOOT_BANNER, 1);
GEN_ABSOLUTE_SYM(CONFIG_BOOT_DELAY, 0);
GEN_ABSOLUTE_SYM(CONFIG_SYSTEM_WORKQUEUE_STACK_SIZE, 1024);
GEN_ABSOLUTE_SYM(CONFIG_SYSTEM_WORKQUEUE_PRIORITY, -1);
GEN_ABSOLUTE_SYM(CONFIG_OFFLOAD_WORKQUEUE_STACK_SIZE, 1024);
GEN_ABSOLUTE_SYM(CONFIG_OFFLOAD_WORKQUEUE_PRIORITY, -1);
GEN_ABSOLUTE_SYM(CONFIG_ATOMIC_OPERATIONS_C, 1);
GEN_ABSOLUTE_SYM(CONFIG_TIMESLICING, 1);
GEN_ABSOLUTE_SYM(CONFIG_TIMESLICE_SIZE, 0);
GEN_ABSOLUTE_SYM(CONFIG_TIMESLICE_PRIORITY, 0);
GEN_ABSOLUTE_SYM(CONFIG_POLL, 1);
GEN_ABSOLUTE_SYM(CONFIG_NUM_MBOX_ASYNC_MSGS, 10);
GEN_ABSOLUTE_SYM(CONFIG_NUM_PIPE_ASYNC_MSGS, 10);
GEN_ABSOLUTE_SYM(CONFIG_HEAP_MEM_POOL_SIZE, 0);
GEN_ABSOLUTE_SYM(CONFIG_ARCH_HAS_CUSTOM_SWAP_TO_MAIN, 1);
GEN_ABSOLUTE_SYM(CONFIG_SYS_CLOCK_EXISTS, 1);
GEN_ABSOLUTE_SYM(CONFIG_KERNEL_INIT_PRIORITY_OBJECTS, 30);
GEN_ABSOLUTE_SYM(CONFIG_KERNEL_INIT_PRIORITY_DEFAULT, 40);
GEN_ABSOLUTE_SYM(CONFIG_KERNEL_INIT_PRIORITY_DEVICE, 50);
GEN_ABSOLUTE_SYM(CONFIG_APPLICATION_INIT_PRIORITY, 90);
GEN_ABSOLUTE_SYM(CONFIG_RETPOLINE, 1);
GEN_ABSOLUTE_SYM(CONFIG_STACK_POINTER_RANDOM, 0);
GEN_ABSOLUTE_SYM(CONFIG_MP_NUM_CPUS, 1);
GEN_ABSOLUTE_SYM(CONFIG_PM_CONTROL_APP, 1);
GEN_ABSOLUTE_SYM(CONFIG_TICKLESS_IDLE, 1);
GEN_ABSOLUTE_SYM(CONFIG_TICKLESS_IDLE_THRESH, 3);
GEN_ABSOLUTE_SYM(CONFIG_HAS_DTS, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_DTS_GPIO, 1);
GEN_ABSOLUTE_SYM(CONFIG_CONSOLE, 1);
GEN_ABSOLUTE_SYM(CONFIG_CONSOLE_INPUT_MAX_LINE_LEN, 128);
GEN_ABSOLUTE_SYM(CONFIG_CONSOLE_HAS_DRIVER, 1);
GEN_ABSOLUTE_SYM(CONFIG_UART_CONSOLE, 1);
GEN_ABSOLUTE_SYM(CONFIG_UART_CONSOLE_INIT_PRIORITY, 60);
GEN_ABSOLUTE_SYM(CONFIG_SERIAL, 1);
GEN_ABSOLUTE_SYM(CONFIG_SERIAL_HAS_DRIVER, 1);
GEN_ABSOLUTE_SYM(CONFIG_SERIAL_SUPPORT_INTERRUPT, 1);
GEN_ABSOLUTE_SYM(CONFIG_UART_NRFX, 1);
GEN_ABSOLUTE_SYM(CONFIG_UART_0_NRF_UART, 1);
GEN_ABSOLUTE_SYM(CONFIG_NRF_UART_PERIPHERAL, 1);
GEN_ABSOLUTE_SYM(CONFIG_NRF_RTC_TIMER, 1);
GEN_ABSOLUTE_SYM(CONFIG_SYSTEM_CLOCK_INIT_PRIORITY, 0);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_GENERATOR, 1);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_NRF5_RNG, 1);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_NRF5_BIAS_CORRECTION, 1);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_NRF5_THR_THRESHOLD, 4);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_NRF5_ISR_THRESHOLD, 12);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_NRF5_THR_BUF_LEN, 4);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_NRF5_ISR_BUF_LEN, 12);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_NRF5_PRI, 2);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_HAS_DRIVER, 1);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_NAME, 1);
GEN_ABSOLUTE_SYM(CONFIG_GPIO_NRF_INIT_PRIORITY, 40);
GEN_ABSOLUTE_SYM(CONFIG_GPIO_NRF_P0, 1);
GEN_ABSOLUTE_SYM(CONFIG_CLOCK_CONTROL, 1);
GEN_ABSOLUTE_SYM(CONFIG_CLOCK_CONTROL_NRF5, 1);
GEN_ABSOLUTE_SYM(CONFIG_CLOCK_CONTROL_NRF5_IRQ_PRIORITY, 1);
GEN_ABSOLUTE_SYM(CONFIG_CLOCK_CONTROL_NRF5_M16SRC_DRV_NAME, 1);
GEN_ABSOLUTE_SYM(CONFIG_CLOCK_CONTROL_NRF5_K32SRC_DRV_NAME, 1);
GEN_ABSOLUTE_SYM(CONFIG_CLOCK_CONTROL_NRF5_K32SRC_XTAL, 1);
GEN_ABSOLUTE_SYM(CONFIG_CLOCK_CONTROL_NRF5_K32SRC_20PPM, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_FLASH_LOAD_OFFSET, 1);
GEN_ABSOLUTE_SYM(CONFIG_KERNEL_ENTRY, 1);
GEN_ABSOLUTE_SYM(CONFIG_CHECK_LINK_MAP, 1);
GEN_ABSOLUTE_SYM(CONFIG_CROSS_COMPILE, 1);
GEN_ABSOLUTE_SYM(CONFIG_SIZE_OPTIMIZATIONS, 1);
GEN_ABSOLUTE_SYM(CONFIG_COMPILER_OPT, 1);
GEN_ABSOLUTE_SYM(CONFIG_KERNEL_BIN_NAME, 1);
GEN_ABSOLUTE_SYM(CONFIG_OUTPUT_STAT, 1);
GEN_ABSOLUTE_SYM(CONFIG_OUTPUT_DISASSEMBLY, 1);
GEN_ABSOLUTE_SYM(CONFIG_OUTPUT_PRINT_MEMORY_USAGE, 1);
GEN_ABSOLUTE_SYM(CONFIG_BUILD_OUTPUT_BIN, 1);
GEN_ABSOLUTE_SYM(CONFIG_MINIMAL_LIBC_MALLOC_ARENA_SIZE, 0);
GEN_ABSOLUTE_SYM(CONFIG_BT, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_HCI, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_BROADCASTER, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_HCI_VS_EXT, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_DEBUG_NONE, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_HCI_HOST, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_HCI_CMD_COUNT, 2);
GEN_ABSOLUTE_SYM(CONFIG_BT_RX_BUF_COUNT, 3);
GEN_ABSOLUTE_SYM(CONFIG_BT_RX_BUF_LEN, 76);
GEN_ABSOLUTE_SYM(CONFIG_BT_HCI_TX_STACK_SIZE, 640);
GEN_ABSOLUTE_SYM(CONFIG_BT_HCI_TX_PRIO, 7);
GEN_ABSOLUTE_SYM(CONFIG_BT_HCI_RESERVE, 0);
GEN_ABSOLUTE_SYM(CONFIG_BT_RECV_IS_RX_THREAD, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_RX_STACK_SIZE, 1024);
GEN_ABSOLUTE_SYM(CONFIG_BT_RX_PRIO, 8);
GEN_ABSOLUTE_SYM(CONFIG_BT_DEVICE_NAME, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_DEVICE_APPEARANCE, 0);
GEN_ABSOLUTE_SYM(CONFIG_BT_ID_MAX, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_LL_SW, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_CRYPTO, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_RX_PRIO_STACK_SIZE, 448);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_RX_PRIO, 6);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_FILTER, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_HCI_VS_BUILD_INFO, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_RX_BUFFERS, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_TX_BUFFERS, 2);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_TX_BUFFER_SIZE, 27);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_TX_PWR_0, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_COMPANY_ID, 0x05F1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_SUBVERSION_NUMBER, 0xFFFF);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_CHAN_SEL_2, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_MIN_USED_CHAN, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_ADV_EXT, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_WORKER_PRIO, 0);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_JOB_PRIO, 0);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_XTAL_ADVANCED, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_XTAL_THRESHOLD, 1500);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_SCHED_ADVANCED, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_TIFS_HW, 1);
GEN_ABSOLUTE_SYM(CONFIG_BT_CTLR_SCAN_REQ_NOTIFY, 1);
GEN_ABSOLUTE_SYM(CONFIG_PRINTK, 1);
GEN_ABSOLUTE_SYM(CONFIG_EARLY_CONSOLE, 1);
GEN_ABSOLUTE_SYM(CONFIG_DISK_ACCESS_MAX_VOLUMES, 8);
GEN_ABSOLUTE_SYM(CONFIG_MCUMGR_BUF_COUNT, 4);
GEN_ABSOLUTE_SYM(CONFIG_MCUMGR_BUF_SIZE, 384);
GEN_ABSOLUTE_SYM(CONFIG_MCUMGR_BUF_USER_DATA_SIZE, 4);
GEN_ABSOLUTE_SYM(CONFIG_NET_BUF, 1);
GEN_ABSOLUTE_SYM(CONFIG_NET_BUF_USER_DATA_SIZE, 4);
GEN_ABSOLUTE_SYM(CONFIG_ENTROPY_DEVICE_RANDOM_GENERATOR, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_CMSIS, 1);
GEN_ABSOLUTE_SYM(CONFIG_LIBMETAL_SRC_PATH, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_NRFX, 1);
GEN_ABSOLUTE_SYM(CONFIG_OPENAMP_SRC_PATH, 1);
GEN_ABSOLUTE_SYM(CONFIG_HAS_SEGGER_RTT, 1);
GEN_ABSOLUTE_SYM(CONFIG_SEGGER_RTT_MAX_NUM_UP_BUFFERS, 3);
GEN_ABSOLUTE_SYM(CONFIG_SEGGER_RTT_MAX_NUM_DOWN_BUFFERS, 3);
GEN_ABSOLUTE_SYM(CONFIG_SEGGER_RTT_BUFFER_SIZE_UP, 1024);
GEN_ABSOLUTE_SYM(CONFIG_SEGGER_RTT_BUFFER_SIZE_DOWN, 16);
GEN_ABSOLUTE_SYM(CONFIG_SEGGER_RTT_PRINTF_BUFFER_SIZE, 64);
GEN_ABSOLUTE_SYM(CONFIG_SEGGER_RTT_MODE_NO_BLOCK_SKIP, 1);
GEN_ABSOLUTE_SYM(CONFIG_SEGGER_RTT_MODE, 0);
GEN_ABSOLUTE_SYM(CONFIG_TEST_EXTRA_STACKSIZE, 0);

GEN_ABS_SYM_END
