/dts-v1/;

/ {
	#address-cells = <0x1>;
	#size-cells = <0x1>;
	model = "VNG VBLUno51 BLE board";
	compatible = "vng,vbluno51", "nordic,nrf51822-qfac", "nordic,nrf51822";

	chosen {
		zephyr,console = "/soc/uart@40002000";
		zephyr,sram = "/memory@20000000";
		zephyr,flash = "/flash-controller@4001E000/flash@0";
	};

	aliases {
	};

	soc {
		#address-cells = <0x1>;
		#size-cells = <0x1>;
		compatible = "simple-bus";
		interrupt-parent = <0x1>;
		ranges;

		nvic: interrupt-controller@e000e100 {
			compatible = "arm,v6m-nvic";
			reg = <0xe000e100 0xc00>;
			interrupt-controller;
			#interrupt-cells = <0x2>;
			arm,num-irq-priority-bits = <0x2>;
			phandle = <0x1>;
		};

		systick: timer@e000e010 {
			compatible = "arm,armv6m-systick";
			reg = <0xe000e010 0x10>;
			status = "disabled";
		};

		adc: adc@40007000 {
			compatible = "nordic,nrf-adc";
			reg = <0x40007000 0x1000>;
			interrupts = <0x7 0x1>;
			status = "disabled";
			label = "ADC_0";
		};

		uart0: uart@40002000 {
			compatible = "nordic,nrf-uart";
			reg = <0x40002000 0x1000>;
			interrupts = <0x2 0x1>;
			status = "ok";
			label = "UART_0";
			current-speed = <0x1c200>;
		};

		gpiote: gpiote@40006000 {
			compatible = "nordic,nrf-gpiote";
			reg = <0x40006000 0x1000>;
			interrupts = <0x6 0x1>;
			interrupt-names = "gpiote";
			status = "ok";
			label = "GPIOTE_0";
		};

		gpio0: gpio@50000000 {
			compatible = "nordic,nrf-gpio";
			gpio-controller;
			reg = <0x50000000 0x800>;
			#gpio-cells = <0x2>;
			label = "GPIO_0";
			status = "ok";
		};

		i2c0: i2c@40003000 {
			compatible = "nordic,nrf5-i2c";
			#address-cells = <0x1>;
			#size-cells = <0x0>;
			reg = <0x40003000 0x1000>;
			clock-frequency = <0x61a80>;
			interrupts = <0x3 0x1>;
			status = "ok";
			label = "I2C_0";
			sda-pin = <0x1d>;
			scl-pin = <0x1e>;
		};

		i2c1: i2c@40004000 {
			compatible = "nordic,nrf5-i2c";
			#address-cells = <0x1>;
			#size-cells = <0x0>;
			reg = <0x40004000 0x1000>;
			clock-frequency = <0x186a0>;
			interrupts = <0x4 0x1>;
			status = "disabled";
			label = "I2C_1";
		};

		wdt: watchdog@40010000 {
			compatible = "nordic,nrf-watchdog";
			reg = <0x40010000 0x1000>;
			interrupts = <0x10 0x1>;
			interrupt-names = "wdt";
			label = "WDT";
		};
	};

	cpus {
		#address-cells = <0x1>;
		#size-cells = <0x0>;

		cpu@0 {
			device_type = "cpu";
			compatible = "arm,cortex-m0";
			reg = <0x0>;
		};
	};

	flash-controller@4001E000 {
		compatible = "nrf,nrf51-flash-controller";
		reg = <0x4001e000 0x518>;
		#address-cells = <0x1>;
		#size-cells = <0x1>;
		label = "NRF_FLASH_DRV_NAME";

		flash0: flash@0 {
			compatible = "soc-nv-flash";
			label = "NRF_FLASH";
			reg = <0x0 0x40000>;
			erase-block-size = <0x400>;
			write-block-size = <0x4>;
		};
	};

	sram0: memory@20000000 {
		device_type = "memory";
		compatible = "mmio-sram";
		reg = <0x20000000 0x8000>;
	};
};
