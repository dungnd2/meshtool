# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/stdlib/atoi.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdlib/atoi.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/stdlib/malloc.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdlib/malloc.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/stdlib/strtol.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdlib/strtol.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/stdlib/strtoul.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdlib/strtoul.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/stdout/fprintf.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdout/fprintf.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/stdout/prf.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdout/prf.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/stdout/sprintf.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdout/sprintf.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/stdout/stdout_console.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdout/stdout_console.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/string/string.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/string/string.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/string/strncasecmp.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/string/strncasecmp.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/libc/minimal/source/string/strstr.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/basic/button/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/string/strstr.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "NRF51"
  "NRF51822"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../arch/arm/soc/nordic_nrf/nrf51"
  "../../../../arch/arm/soc/nordic_nrf/nrf51/include"
  "../../../../arch/arm/soc/nordic_nrf/include"
  "../../../../boards/arm/nrf51_vbluno51"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include"
  "/opt/zephyr-sdk/sysroots/x86_64-pokysdk-linux/usr/lib/arm-zephyr-eabi/gcc/arm-zephyr-eabi/6.2.0/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
