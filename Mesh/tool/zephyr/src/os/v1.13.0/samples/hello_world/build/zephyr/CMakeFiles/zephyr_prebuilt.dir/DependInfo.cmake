# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/misc/empty_file.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr_prebuilt.dir/misc/empty_file.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "_FORTIFY_SOURCE=2"
  "_POSIX_C_SOURCE=200809"
  "_XOPEN_SOURCE"
  "_XOPEN_SOURCE_EXTENDED"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../kernel/include"
  "../../../arch/posix/include"
  "../../../arch/posix/soc/inf_clock"
  "../../../arch/posix/soc/inf_clock/include"
  "../../../arch/posix/soc/include"
  "../../../boards/posix/native_posix"
  "../../../include"
  "../../../include/drivers"
  "zephyr/include/generated"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/CMakeFiles/app.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/arch/posix/soc/inf_clock/CMakeFiles/arch__posix__soc__inf_clock.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/arch/posix/core/CMakeFiles/arch__posix__core.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/boards/boards/posix/native_posix/CMakeFiles/boards__posix__native_posix.dir/DependInfo.cmake"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/kernel/CMakeFiles/kernel.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
