# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/drivers/console/native_posix_console.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/drivers/console/native_posix_console.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/drivers/timer/native_posix_timer.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/drivers/timer/native_posix_timer.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/drivers/timer/sys_clock_init.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/drivers/timer/sys_clock_init.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/crc/crc16_sw.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/lib/crc/crc16_sw.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/crc/crc32_sw.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/lib/crc/crc32_sw.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/crc/crc8_sw.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/lib/crc/crc8_sw.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/mempool/mempool.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/lib/mempool/mempool.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/rbtree/rb.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/lib/rbtree/rb.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/lib/thread_entry.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/lib/thread_entry.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/misc/generated/configs.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/misc/generated/configs.c.obj"
  "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/misc/printk.c" "/home/dung/Documents/GIT_REPO/iot/device/zephyr/os/v1.13.0/samples/hello_world/build/zephyr/CMakeFiles/zephyr.dir/misc/printk.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "KERNEL"
  "_FORTIFY_SOURCE=2"
  "_POSIX_C_SOURCE=200809"
  "_XOPEN_SOURCE"
  "_XOPEN_SOURCE_EXTENDED"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../kernel/include"
  "../../../arch/posix/include"
  "../../../arch/posix/soc/inf_clock"
  "../../../arch/posix/soc/inf_clock/include"
  "../../../arch/posix/soc/include"
  "../../../boards/posix/native_posix"
  "../../../include"
  "../../../include/drivers"
  "zephyr/include/generated"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
