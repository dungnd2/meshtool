/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/gen_onoff_srv.h>

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_GEN_ONOFF_SRV)
#include "common/log.h"

#define BT_MESH_MODEL_OP_GEN_ONOFF_GET		    BT_MESH_MODEL_OP_2(0x82, 0x01)
#define BT_MESH_MODEL_OP_GEN_ONOFF_SET		    BT_MESH_MODEL_OP_2(0x82, 0x02)
#define BT_MESH_MODEL_OP_GEN_ONOFF_SET_UNACK	BT_MESH_MODEL_OP_2(0x82, 0x03)
#define BT_MESH_MODEL_OP_GEN_ONOFF_STATUS     BT_MESH_MODEL_OP_2(0x82, 0x04)


int bt_mesh_gen_onoff_srv_init(struct bt_mesh_model *model, bool primary)
{
    struct bt_mesh_gen_onoff_srv *srv = model->user_data;
    srv->model = model;
    return 0;
}


static void gen_onoff_status_send(struct bt_mesh_model *model,
                                  struct bt_mesh_msg_ctx *ctx)
{
    
    struct bt_mesh_gen_onoff_srv *srv = model->user_data;
    NET_BUF_SIMPLE_DEFINE(msg, 2 + 3 + 4);

    if (srv->get_func == NULL) {
        BT_WARN("get function not avaiabled");
        return;
    }

    bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_GEN_ONOFF_STATUS);
    srv->get_func(&msg, srv->user_data);
    BT_DBG("status msg [%s]", bt_hex(msg.data, msg.len));

    if (bt_mesh_model_send(model, ctx, &msg, NULL, NULL)) {
        BT_ERR("Unable to send Onoff Status");
    }

}

static void gen_onoff_data_get(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    struct bt_mesh_gen_onoff_srv *srv = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));
    if(srv->get_func) {
        srv->get_func(buf, srv->user_data);
    }

    gen_onoff_status_send(model, ctx);
}

static void gen_onoff_data_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    struct bt_mesh_gen_onoff_srv *srv = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    if(srv->set_func) {
        srv->set_func(buf, srv->user_data);
    }

    gen_onoff_status_send(model, ctx);


}

static void gen_onoff_data_set_unack(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    struct bt_mesh_gen_onoff_srv *srv = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));
    if(srv->set_func) {
        srv->set_func(buf, srv->user_data);
    } 
}

const struct bt_mesh_model_op bt_mesh_gen_onoff_srv_op[] = {
    { BT_MESH_MODEL_OP_GEN_ONOFF_GET, 0, gen_onoff_data_get },
    { BT_MESH_MODEL_OP_GEN_ONOFF_SET, 2, gen_onoff_data_set },
    { BT_MESH_MODEL_OP_GEN_ONOFF_SET_UNACK, 2, gen_onoff_data_set_unack },
    BT_MESH_MODEL_OP_END,
};
