/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/gen_onoff_cli.h>

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_GEN_ONOFF_CLI)
#include "common/log.h"

#define BT_MESH_MODEL_OP_GEN_ONOFF_GET          BT_MESH_MODEL_OP_2(0x82, 0x01)
#define BT_MESH_MODEL_OP_GEN_ONOFF_SET          BT_MESH_MODEL_OP_2(0x82, 0x02)
#define BT_MESH_MODEL_OP_GEN_ONOFF_SET_UNACK    BT_MESH_MODEL_OP_2(0x82, 0x03)
#define BT_MESH_MODEL_OP_GEN_ONOFF_STATUS       BT_MESH_MODEL_OP_2(0x82, 0x04)


int bt_mesh_gen_onoff_cli_init(struct bt_mesh_model *model, bool primary)
{
    struct bt_mesh_gen_onoff_cli *cli = model->user_data;
    cli->model = model;
    return 0;
}


int bt_mesh_gen_onoff_get(struct bt_mesh_model *model, u16_t addr)
{

}

int bt_mesh_gen_onoff_set(struct bt_mesh_model *model, u16_t addr, struct net_buf_simple* data)
{
    BT_DBG("");
    NET_BUF_SIMPLE_DEFINE(msg, 3 + 5 + 4);

    struct bt_mesh_msg_ctx ctx = {
        .net_idx = 0,
        .app_idx = 0,
        .addr = addr,
        .send_ttl = BT_MESH_TTL_DEFAULT,
    };
    int err;

    struct bt_mesh_gen_onoff_cli *cli = model->user_data;
    if(!cli)
        return;


    bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_GEN_ONOFF_SET);
    net_buf_simple_add_mem(&msg, data->data, data->len);
    

    err = bt_mesh_model_send(model, &ctx, &msg, NULL, NULL);
    if (err) {
        BT_ERR("model_send() failed (err %d)", err);
        return err;
    }
}

int bt_mesh_gen_onoff_set_unack(struct bt_mesh_model *model, u16_t addr, struct net_buf_simple* data)
{
    
}


static void gen_onoff_data_status(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    struct bt_mesh_gen_onoff_cli *cli = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));


    if (cli->status_func != NULL)
        cli->status_func(ctx->addr, buf , cli->user_data);

}

const struct bt_mesh_model_op bt_mesh_gen_onoff_cli_op[] = {
    { BT_MESH_MODEL_OP_GEN_ONOFF_STATUS, 2, gen_onoff_data_status },
    BT_MESH_MODEL_OP_END,
};
