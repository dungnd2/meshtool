/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/level_srv.h>

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_GEN_LEVEL_SRV)
#include "common/log.h"

#define BT_MESH_MODEL_OP_GEN_LEVEL_GET       BT_MESH_MODEL_OP_2(0x82, 0x05)
#define BT_MESH_MODEL_OP_GEN_LEVEL_SET       BT_MESH_MODEL_OP_2(0x82, 0x06)
#define BT_MESH_MODEL_OP_GEN_LEVEL_SET_UNACK BT_MESH_MODEL_OP_2(0x82, 0x07)
#define BT_MESH_MODEL_OP_GEN_LEVEL_STATUS    BT_MESH_MODEL_OP_2(0x82, 0x08)

int bt_mesh_gen_level_srv_init(struct bt_mesh_model *model, bool primary)
{

    BT_DBG("");
    struct bt_mesh_gen_level_srv *srv = model->user_data;
    srv->model = model;

    return 0;
}

static void gen_level_status_send(struct bt_mesh_model *model,
                                  struct bt_mesh_msg_ctx *ctx)
{
    u16_t present;
    u16_t target;
    u8_t remain;
    struct bt_mesh_gen_level_srv *srv = model->user_data;
    NET_BUF_SIMPLE_DEFINE(msg, 2 + 5 + 4);

    if (srv->get_func == NULL) {
        BT_WARN("get function not avaiabled");
        return;
    }

    srv->get_func(&present, &target, &remain, srv->user_data);

    bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_GEN_LEVEL_STATUS);

    net_buf_simple_add_le16(&msg, present);
    net_buf_simple_add_le16(&msg, target);
    net_buf_simple_add_u8(&msg, remain);

    if (bt_mesh_model_send(model, ctx, &msg, NULL, NULL)) {
        BT_ERR("Unable to send Level Status");
    }

}

static void gen_level_data_get(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    gen_level_status_send(model, ctx);

}

static void gen_level_data_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    u16_t level;
    u8_t tid;
    u8_t time;
    u8_t delay;

    struct bt_mesh_gen_level_srv *srv = model->user_data;

    level = net_buf_simple_pull_be16(buf);
    tid = net_buf_simple_pull_u8(buf);
    time = net_buf_simple_pull_u8(buf);
    delay = net_buf_simple_pull_u8(buf);

    BT_DBG("level: %d, tid: 0x%02x, time: 0x%02x, delay 0x%02x",
           level, tid, time, delay);

    if (srv->set_func != NULL)
        srv->set_func(level, tid, time, delay, srv->user_data);

    gen_level_status_send(model, ctx);

}

static void gen_level_data_set_unack(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    u16_t level;
    u8_t tid;
    u8_t time;
    u8_t delay;

    struct bt_mesh_gen_level_srv *srv = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    level = net_buf_simple_pull_be16(buf);
    tid = net_buf_simple_pull_u8(buf);
    time = net_buf_simple_pull_u8(buf);
    delay = net_buf_simple_pull_u8(buf);

    BT_DBG("level: %d, tid: 0x%02x, time: 0x%02x, delay 0x%02x",
           level, tid, time, delay);

    if (srv->set_func != NULL)
        srv->set_func(level, tid, time, delay, srv->user_data);

}

const struct bt_mesh_model_op bt_mesh_gen_level_srv_op[] = {
    { BT_MESH_MODEL_OP_GEN_LEVEL_GET, 0, gen_level_data_get },
    { BT_MESH_MODEL_OP_GEN_LEVEL_SET, 5, gen_level_data_set },
    { BT_MESH_MODEL_OP_GEN_LEVEL_SET_UNACK, 5, gen_level_data_set_unack },
    BT_MESH_MODEL_OP_END,
};
