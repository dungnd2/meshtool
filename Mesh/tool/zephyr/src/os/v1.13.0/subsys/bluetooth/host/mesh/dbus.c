
#include <zephyr.h>
#include <misc/stack.h>
#include <misc/util.h>

#include <net/buf.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/hci.h>


#include <dbus_lib.h>

#include "common/log.h"
#include "net.h"
#include "adv.h"

#include "dbus.h"

struct _dbus_internal
{

    dbus_obj_id_t pub; // broadcast to slaves
    dbus_obj_id_t sub; // listen to slaves

};

static struct _dbus_internal _dbus = {
};


#ifdef CONFIG_BT_MESH_DBUS_SLAVE

extern void uc_slave_handle_frame(const u8_t* frame, size_t len);

static int _on_master_message(const char* data, int len)
{
    uc_slave_handle_frame(data, len);
    return 0;
}

#endif //CONFIG_BT_MESH_DBUS_SLAVE

#ifdef CONFIG_BT_MESH_DBUS_MASTER
extern int uc_master_handle_frame(const u8_t* data, int len);

static int _on_slave_message(const char* data, int data_len)
{
    return uc_master_handle_frame(data, data_len);
}
#endif //CONFIG_BT_MESH_DBUS_MASTER

#ifdef CONFIG_BT_MESH_DBUS_LOCAL

static int _on_local_message(const char* data, int len)
{
   
    NET_BUF_SIMPLE_DEFINE_STATIC(buf, DBUS_MESH_DATA_LEN_MAX);
    // static bt_addr_le_t addr;

    net_buf_simple_reset(&buf);
    net_buf_simple_add_mem(&buf, data, len);

    BT_DBG("len %d", len);

    bt_mesh_net_recv(&buf, 0, BT_MESH_NET_IF_ADV);

    return 0;
}

#endif //CONFIG_BT_MESH_DBUS_LOCAL


int dbus_mesh_init()
{
    int error;
 
    error = dbus_init();

    if (error) {
        BT_ERR("dbus_init() failed\n");
        return error;
    }


    // both master and slave need pub-er, sub-er.

    error = dbus_publisher_create(&_dbus.pub);

    if (error) {
        BT_ERR("dbus_publisher_create() failed\n");
        return error;
    }

    error = dbus_subscriber_create(&_dbus.sub);

    if (error) {
        BT_ERR("dbus_subscriber_create() failed\n");
        return error;
    }


#ifdef CONFIG_BT_MESH_DBUS_MASTER
    error = dbus_subscribe(_dbus.sub, TO_MASTER_CHANNEL, _on_slave_message);

    if (error) {
        BT_ERR("dbus_subscribe() failed\n");
    }
#endif 


#ifdef CONFIG_BT_MESH_DBUS_SLAVE
    error = dbus_subscribe(_dbus.sub, TO_SLAVE_CHANNEL, _on_master_message);

    if (error) {
        BT_ERR("dbus_subscribe() failed\n");
    }    
#endif


#ifdef CONFIG_BT_MESH_DBUS_LOCAL

    error = dbus_subscribe(_dbus.sub, TO_LOCAL_CHANNEL, _on_local_message);

    if (error) {
        BT_ERR("dbus_subscribe() failed\n");
    }  

#endif 

    return error;

}


#ifdef CONFIG_BT_MESH_DBUS_MASTER
int dbus_mesh_master_broadcast(const u8_t* data, size_t len)
{
    return dbus_pushlish(_dbus.pub, TO_SLAVE_CHANNEL, data, len); 

}
#endif 


#ifdef CONFIG_BT_MESH_DBUS_LOCAL
int dbus_mesh_local_broadcast(const u8_t* data, size_t len)
{
    return dbus_pushlish(_dbus.pub, TO_LOCAL_CHANNEL, data, len); 
}
#endif


#ifdef CONFIG_BT_MESH_DBUS_SLAVE
int dbus_mesh_slave_forward(struct net_buf *buf)
{
    return dbus_pushlish(_dbus.pub, TO_MASTER_CHANNEL, buf->data, buf->len);
}
#endif 

int dbus_mesh_start_listen()
{
    return dbus_start_listening();
}
