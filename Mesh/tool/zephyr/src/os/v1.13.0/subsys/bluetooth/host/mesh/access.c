/*  Bluetooth Mesh */

/*
 * Copyright (c) 2017 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <errno.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <net/buf.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/mesh.h>


#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_ACCESS)
#include "common/log.h"

#include "mesh.h"
#include "adv.h"
#include "net.h"
#include "lpn.h"
#include "transport.h"
#include "access.h"
#include "foundation.h"
#include "settings.h"

#include <stdio.h>

#undef BT_DBG
#define BT_DBG(fmt, ...) \
	if (debug_access_enabled) { \
		SYS_LOG_DBG("(%p) " fmt, k_current_get(), \
			    ##__VA_ARGS__); \
	}
bool debug_access_enabled = false;
static s8_t last_received_rssi;


#ifdef CONFIG_BOARD_NATIVE_POSIX

#define ADDRS_MAX 200
#define ACCESS_CONTROL_SLOTS 20
#define ACC_ORIGIN_MIN_TTL 3

#define BUTTON_TID_MSB_IDX 1
#define BUTTON_TID_LSB_IDX 2
#define READER_TID_MSB_IDX 6
#define READER_TID_LSB_IDX 7


#define HUB_LINE_STATUS_NORMAL 0
#define HUB_LINE_STATUS_DISABLED 1
#define HUB_LINE_STATUS_GPIO_FAILED 2
#define HUB_LINE_STATUS_LED_FAILED 3
#define HUB_LINE_STATUS_DRIVER_FAILED 4
#define HUB_LINE_STATUS_UNDEFINED 5
#define HUB_LINES 8



#define ACC_TID(cmd, buf) ((cmd == 0x12) ? buf[READER_TID_MSB_IDX] << 8 | buf[READER_TID_LSB_IDX] : \
										   buf[BUTTON_TID_MSB_IDX] << 8 | buf[BUTTON_TID_LSB_IDX])
 

#define HUB_FAULTS_OPCODE 0x00008209
const char* hub_faults(u8_t* status)
{	
	static char faults[17];

	for (int i = 0; i < 8; ++i) {

		if (status[i] == HUB_LINE_STATUS_NORMAL) {
			// sprintf(&faults[i*2], "%dN", i);
			faults[i*2] = '_';
			faults[i*2+1] = '_';
		} else if (status[i] == HUB_LINE_STATUS_GPIO_FAILED) {
			sprintf(&faults[i*2], "%dG", i);
		} else if (status[i] == HUB_LINE_STATUS_LED_FAILED) {
			sprintf(&faults[i*2], "%dL", i);
		} else if (status[i] == HUB_LINE_STATUS_DRIVER_FAILED) {
			sprintf(&faults[i*2], "%dD", i);
		} else if (status[i] == HUB_LINE_STATUS_UNDEFINED) {
			sprintf(&faults[i*2], "%dU", i);
		}
	}

	return faults;
}

struct package_filter* pkg_filter = NULL;

struct acctr_module
{
	u16_t dev;
	u8_t  dev_ttl;
	u16_t dev_tid;
	u64_t dev_ts;
	u32_t dev_seq;

	int8_t gw_rssi;
	
	bool is_button;
	bool show;
};

struct access_control {
	struct acctr_module acc[ACCESS_CONTROL_SLOTS];
	u8_t index;

	struct k_delayed_work timer;
};

static struct access_control acctr = {
	.index = 0,
};

static FILE* fsetting;

struct remote {
	u8_t index;
	u16_t addr[ADDRS_MAX];
};

static struct remote _remote = {
	.index = 0
};

static void remote_log(u16_t addr)
{
	int i;

	for(i = 0; i < ADDRS_MAX; i++) {
		if(addr == _remote.addr[i])
			return;
	}

	fsetting = fopen("./remote.txt", "a");
	if(!fsetting) {
		BT_ERR("file open failed");
		return;
	}

	// BT_ERR("remote 0x%04x", addr);

	_remote.index = (_remote.index + 1) % ADDRS_MAX;
	_remote.addr[_remote.index] = addr;

	fprintf(fsetting, "%04x\n", addr);
	fclose(fsetting);

}


extern void bt_mesh_remote_log_clr()
{
	memset(_remote.addr, 0, ADDRS_MAX * sizeof(u16_t));
	_remote.index = 0;	
	fsetting = fopen("./remote.txt", "w");
	if(!fsetting) {
		BT_ERR("file open failed");
		return;
	}
	fprintf(fsetting, "%04x\n", 0);
	fclose(fsetting);

}

static bool buf_in_buf(struct net_buf_simple *buf1, struct net_buf_simple *buf2)
{	
	int i;
	if(buf1->len > buf2->len)
		return false;

	for(i = 0; i < buf1->len; i++) {
		if (buf1->data[i] != buf2->data[i])
			return false;
	}

	return true;
}	

// return index of 

static inline bool is_gw_to_door_data(const u8_t* buf)
{
	return (buf[0] == 0x01 && buf[1] == 0x00);
}


static int check_access(struct bt_mesh_net_rx *rx, struct net_buf_simple *buf)
{

	if(pkg_filter->ac == 0)
		return -1;


	if(rx->rssi < pkg_filter->min_rssi)
		return 0;


	//010006007801       door
	//12003ad184c40071 reader
	u8_t cmd = buf->data[0];

	if((cmd == 0x12 || cmd == 0x14) && rx->ctx.recv_ttl >= ACC_ORIGIN_MIN_TTL) {

		u8_t *_buf = buf->data;
		u16_t tid = ACC_TID(cmd, _buf);
		u8_t idx;

		for (int i = 0; i < ACCESS_CONTROL_SLOTS; i++) {

			if (acctr.acc[i].dev == rx->ctx.addr && 
							acctr.acc[i].dev_tid == tid) {
				return 0;
			}
		}
		
		idx = acctr.index;

		acctr.acc[idx].is_button = (cmd == 0x12) ? false : true;

		acctr.acc[idx].dev =  rx->ctx.addr;
		acctr.acc[idx].dev_tid =  tid;
		acctr.acc[idx].dev_ttl = rx->ctx.recv_ttl;
		acctr.acc[idx].dev_ts = k_uptime_get_32();
		acctr.acc[idx].gw_rssi = -100;
		acctr.acc[idx].show = false;
		acctr.acc[idx].dev_seq = rx->seq;

		printk("\n\n\nr[%04u]: %s 0x%04x tid %u ttl %u seq 0x%08x\n", 
				acctr.acc[idx].dev_tid,
				(acctr.acc[idx].is_button) ? "button" : "reader", 
				acctr.acc[idx].dev, 
				acctr.acc[idx].dev_tid, 
				acctr.acc[idx].dev_ttl,
				acctr.acc[idx].dev_seq);

		acctr.index = (acctr.index + 1) % ACCESS_CONTROL_SLOTS;

	} else if (is_gw_to_door_data(buf->data)) {

		u16_t tid = buf->data[3] << 8 | buf->data[4];

		for(int i = 0; i < ACCESS_CONTROL_SLOTS; i++) {

			if(acctr.acc[i].dev_tid == tid) {
			
				u64_t lastime = acctr.acc[i].dev_ts;
				u64_t delayed = k_uptime_delta_32(&lastime);

				if (delayed > K_SECONDS(3)) 
					continue;

				if (acctr.acc[i].gw_rssi != -100 && acctr.acc[i].gw_rssi >= rx->rssi)
					continue;
				
				acctr.acc[i].gw_rssi = rx->rssi;
				acctr.acc[i].show = true;
				
				printk("\n\nr[%04u]:\n", acctr.acc[i].dev_tid);
				printk("\t %s 0x%04x tid %u ttl %u\n", 
					(acctr.acc[i].is_button) ? "button" : "reader", 
					acctr.acc[i].dev,
					acctr.acc[i].dev_tid, 
					acctr.acc[i].dev_ttl);

				printk("\t door 0x%04x, delayed %llu (ms)\n", rx->ctx.recv_dst, delayed);
				printk("\t gw 0x%04x ttl %u rssi %d seq 0x%08x\n",
					rx->ctx.addr, 
					rx->ctx.recv_ttl, 
					acctr.acc[i].gw_rssi,
					rx->seq);	

			}
		}
	}

	return 0;

}

void set_package_filter(struct package_filter* filter)
{
	pkg_filter = filter;
}



static bool valid_package(struct bt_mesh_net_rx *rx, u32_t opcode, struct net_buf_simple *buf)
{
	// BT_ERR("1");
	int i;
	bool has_op = pkg_filter->inc_op_len > 0 ? false : true;
	bool has_pl = pkg_filter->inc_pl_len > 0 ? false : true;

	if(rx->rssi < pkg_filter->min_rssi)
		return false;

	if(rx->ctx.recv_ttl < pkg_filter->min_ttl || rx->ctx.recv_ttl > pkg_filter->max_ttl)
		return false;	

	for(i = 0; i < pkg_filter->inc_op_len; i++) {
		if(pkg_filter->inc_ops[i] == opcode) {

			if (opcode == 0x00008207) {
				
#ifdef TRACK_GATEWAY_LIGHT
				// data of gateway dim line 
				has_op = (buf->len < 8) ? true : false;
#else 
				has_op = true;
#endif
			} else {
				has_op = true;
			}

			
			break;
		}
	}

	if(has_op == true)
		return true;
	// BT_ERR("3");
	for(i = 0; i < pkg_filter->inc_pl_len; i++) {
		if(buf_in_buf(pkg_filter->inc_pls[i], buf)) {
			has_pl = true;
			break;
		}
	}

	// BT_ERR("4");
	if(has_pl == false)
		return false;

	return true;

}
#endif

static const struct bt_mesh_comp *dev_comp;
static u16_t dev_primary_addr;

static const struct {
	const u16_t id;
	int (*const init)(struct bt_mesh_model *model, bool primary);
} model_init[] = {
	{ BT_MESH_MODEL_ID_CFG_SRV, bt_mesh_cfg_srv_init },
	{ BT_MESH_MODEL_ID_HEALTH_SRV, bt_mesh_health_srv_init },
#if defined(CONFIG_BT_MESH_CFG_CLI)
	{ BT_MESH_MODEL_ID_CFG_CLI, bt_mesh_cfg_cli_init },
#endif
#if defined(CONFIG_BT_MESH_HEALTH_CLI)
	{ BT_MESH_MODEL_ID_HEALTH_CLI, bt_mesh_health_cli_init },
#endif
};

void bt_mesh_model_foreach(void (*func)(struct bt_mesh_model *mod,
					struct bt_mesh_elem *elem,
					bool vnd, bool primary,
					void *user_data),
			   void *user_data)
{
	int i, j;

	for (i = 0; i < dev_comp->elem_count; i++) {
		struct bt_mesh_elem *elem = &dev_comp->elem[i];

		for (j = 0; j < elem->model_count; j++) {
			struct bt_mesh_model *model = &elem->models[j];

			func(model, elem, false, i == 0, user_data);
		}

		for (j = 0; j < elem->vnd_model_count; j++) {
			struct bt_mesh_model *model = &elem->vnd_models[j];

			func(model, elem, true, i == 0, user_data);
		}
	}
}

s32_t bt_mesh_model_pub_period_get(struct bt_mesh_model *mod)
{
	int period;

	if (!mod->pub) {
		return 0;
	}

	switch (mod->pub->period >> 6) {
	case 0x00:
		/* 1 step is 100 ms */
		period = K_MSEC((mod->pub->period & BIT_MASK(6)) * 100);
		break;
	case 0x01:
		/* 1 step is 1 second */
		period = K_SECONDS(mod->pub->period & BIT_MASK(6));
		break;
	case 0x02:
		/* 1 step is 10 seconds */
		period = K_SECONDS((mod->pub->period & BIT_MASK(6)) * 10);
		break;
	case 0x03:
		/* 1 step is 10 minutes */
		period = K_MINUTES((mod->pub->period & BIT_MASK(6)) * 10);
		break;
	default:
		CODE_UNREACHABLE;
	}

	return period >> mod->pub->period_div;
}

static s32_t next_period(struct bt_mesh_model *mod)
{
	struct bt_mesh_model_pub *pub = mod->pub;
	u32_t elapsed, period;

	period = bt_mesh_model_pub_period_get(mod);
	if (!period) {
		return 0;
	}

	elapsed = k_uptime_get_32() - pub->period_start;

	BT_DBG("Publishing took %ums", elapsed);

	if (elapsed > period) {
		BT_WARN("Publication sending took longer than the period");
		/* Return smallest positive number since 0 means disabled */
		return K_MSEC(1);
	}

	return period - elapsed;
}

static void publish_sent(int err, void *user_data)
{
	struct bt_mesh_model *mod = user_data;
	s32_t delay;

	BT_DBG("err %d", err);

	if (mod->pub->count) {
		delay = BT_MESH_PUB_TRANSMIT_INT(mod->pub->retransmit);
	} else {
		delay = next_period(mod);
	}

	if (delay) {
		BT_DBG("Publishing next time in %dms", delay);
		k_delayed_work_submit(&mod->pub->timer, delay);
	}
}

static const struct bt_mesh_send_cb pub_sent_cb = {
	.end = publish_sent,
};

static int publish_retransmit(struct bt_mesh_model *mod)
{
	NET_BUF_SIMPLE_DEFINE(sdu, BT_MESH_TX_SDU_MAX);
	struct bt_mesh_model_pub *pub = mod->pub;
	struct bt_mesh_app_key *key;
	struct bt_mesh_msg_ctx ctx = {
		.addr = pub->addr,
		.send_ttl = pub->ttl,
	};
	struct bt_mesh_net_tx tx = {
		.ctx = &ctx,
		.src = bt_mesh_model_elem(mod)->addr,
		.xmit = bt_mesh_net_transmit_get(),
		.friend_cred = pub->cred,
	};

	key = bt_mesh_app_key_find(pub->key);
	if (!key) {
		return -EADDRNOTAVAIL;
	}

	tx.sub = bt_mesh_subnet_get(key->net_idx);

	ctx.net_idx = key->net_idx;
	ctx.app_idx = key->app_idx;

	net_buf_simple_add_mem(&sdu, pub->msg->data, pub->msg->len);

	pub->count--;

	return bt_mesh_trans_send(&tx, &sdu, &pub_sent_cb, mod);
}

static void mod_publish(struct k_work *work)
{
	struct bt_mesh_model_pub *pub = CONTAINER_OF(work,
						     struct bt_mesh_model_pub,
						     timer.work);
	s32_t period_ms;
	int err;

	BT_DBG("");

	period_ms = bt_mesh_model_pub_period_get(pub->mod);
	BT_DBG("period %u ms", period_ms);

	if (pub->count) {
		err = publish_retransmit(pub->mod);
		if (err) {
			BT_ERR("Failed to retransmit (err %d)", err);

			pub->count = 0;

			/* Continue with normal publication */
			if (period_ms) {
				k_delayed_work_submit(&pub->timer, period_ms);
			}
		}

		return;
	}

	if (!period_ms) {
		return;
	}

	__ASSERT_NO_MSG(pub->update != NULL);

	pub->period_start = k_uptime_get_32();

	err = pub->update(pub->mod);
	if (err) {
		BT_ERR("Failed to update publication message");
		return;
	}

	err = bt_mesh_model_publish(pub->mod);
	if (err) {
		BT_ERR("Publishing failed (err %d)", err);
	}

	if (pub->count) {
		/* Retransmissions also control the timer */
		k_delayed_work_cancel(&pub->timer);
	}
}

struct bt_mesh_elem *bt_mesh_model_elem(struct bt_mesh_model *mod)
{
	return &dev_comp->elem[mod->elem_idx];
}

struct bt_mesh_model *bt_mesh_model_get(bool vnd, u8_t elem_idx, u8_t mod_idx)
{
	struct bt_mesh_elem *elem;

	if (elem_idx >= dev_comp->elem_count) {
		BT_ERR("Invalid element index %u", elem_idx);
		return NULL;
	}

	elem = &dev_comp->elem[elem_idx];

	if (vnd) {
		if (mod_idx >= elem->vnd_model_count) {
			BT_ERR("Invalid vendor model index %u", mod_idx);
			return NULL;
		}

		return &elem->vnd_models[mod_idx];
	} else {
		if (mod_idx >= elem->model_count) {
			BT_ERR("Invalid SIG model index %u", mod_idx);
			return NULL;
		}

		return &elem->models[mod_idx];
	}
}

static void mod_init(struct bt_mesh_model *mod, struct bt_mesh_elem *elem,
		     bool vnd, bool primary, void *user_data)
{
	int i;

	if (mod->pub) {
		mod->pub->mod = mod;
		k_delayed_work_init(&mod->pub->timer, mod_publish);
	}

	for (i = 0; i < ARRAY_SIZE(mod->keys); i++) {
		mod->keys[i] = BT_MESH_KEY_UNUSED;
	}

	mod->elem_idx = elem - dev_comp->elem;
	if (vnd) {
		mod->mod_idx = mod - elem->vnd_models;
	} else {
		mod->mod_idx = mod - elem->models;
	}

	if (vnd) {
		return;
	}

	for (i = 0; i < ARRAY_SIZE(model_init); i++) {
		if (model_init[i].id == mod->id) {
			model_init[i].init(mod, primary);
		}
	}
}

int bt_mesh_comp_register(const struct bt_mesh_comp *comp)
{
	/* There must be at least one element */
	if (!comp->elem_count) {
		return -EINVAL;
	}

	dev_comp = comp;

	bt_mesh_model_foreach(mod_init, NULL);

	return 0;
}

void bt_mesh_comp_provision(u16_t addr)
{
	int i;

	dev_primary_addr = addr;

	BT_DBG("addr 0x%04x elem_count %zu", addr, dev_comp->elem_count);

	for (i = 0; i < dev_comp->elem_count; i++) {
		struct bt_mesh_elem *elem = &dev_comp->elem[i];

		elem->addr = addr++;

		BT_DBG("addr 0x%04x mod_count %u vnd_mod_count %u",
		       elem->addr, elem->model_count, elem->vnd_model_count);
	}
}

void bt_mesh_comp_unprovision(void)
{
	BT_DBG("");

	dev_primary_addr = BT_MESH_ADDR_UNASSIGNED;

	bt_mesh_model_foreach(mod_init, NULL);
}

u16_t bt_mesh_primary_addr(void)
{
	return dev_primary_addr;
}

u16_t *bt_mesh_model_find_group(struct bt_mesh_model *mod, u16_t addr)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(mod->groups); i++) {
		if (mod->groups[i] == addr) {
			return &mod->groups[i];
		}
	}

	return NULL;
}

static struct bt_mesh_model *bt_mesh_elem_find_group(struct bt_mesh_elem *elem,
						     u16_t group_addr)
{
	struct bt_mesh_model *model;
	u16_t *match;
	int i;

	for (i = 0; i < elem->model_count; i++) {
		model = &elem->models[i];

		match = bt_mesh_model_find_group(model, group_addr);
		if (match) {
			return model;
		}
	}

	for (i = 0; i < elem->vnd_model_count; i++) {
		model = &elem->vnd_models[i];

		match = bt_mesh_model_find_group(model, group_addr);
		if (match) {
			return model;
		}
	}

	return NULL;
}

struct bt_mesh_elem *bt_mesh_elem_find(u16_t addr)
{
	int i;

	for (i = 0; i < dev_comp->elem_count; i++) {
		struct bt_mesh_elem *elem = &dev_comp->elem[i];

		if (BT_MESH_ADDR_IS_GROUP(addr) ||
		    BT_MESH_ADDR_IS_VIRTUAL(addr)) {
			if (bt_mesh_elem_find_group(elem, addr)) {
				return elem;
			}
		} else if (elem->addr == addr) {
			return elem;
		}
	}

	return NULL;
}

u8_t bt_mesh_elem_count(void)
{
	return dev_comp->elem_count;
}

static bool model_has_key(struct bt_mesh_model *mod, u16_t key)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(mod->keys); i++) {
		if (mod->keys[i] == key) {
			return true;
		}
	}

	return false;
}

static const struct bt_mesh_model_op *find_op(struct bt_mesh_model *models,
					      u8_t model_count, u16_t dst,
					      u16_t app_idx, u32_t opcode,
					      struct bt_mesh_model **model)
{
	u8_t i;

	for (i = 0; i < model_count; i++) {
		const struct bt_mesh_model_op *op;
		*model = &models[i];

		if (BT_MESH_ADDR_IS_GROUP(dst) ||
		    BT_MESH_ADDR_IS_VIRTUAL(dst)) {
			if (!bt_mesh_model_find_group(*model, dst)) {
				continue;
			}
		}

		if (!model_has_key(*model, app_idx)) {
			continue;
		}

		for (op = (*model)->op; op->func; op++) {
			if (op->opcode == opcode) {
				return op;
			}
		}
	}

	*model = NULL;
	return NULL;
}

static int get_opcode(struct net_buf_simple *buf, u32_t *opcode)
{
	switch (buf->data[0] >> 6) {
	case 0x00:
	case 0x01:
		if (buf->data[0] == 0x7f) {
			BT_ERR("Ignoring RFU OpCode");
			return -EINVAL;
		}

		*opcode = net_buf_simple_pull_u8(buf);
		return 0;
	case 0x02:
		if (buf->len < 2) {
			BT_ERR("Too short payload for 2-octet OpCode");
			return -EINVAL;
		}

		*opcode = net_buf_simple_pull_be16(buf);
		return 0;
	case 0x03:
		if (buf->len < 3) {
			BT_ERR("Too short payload for 3-octet OpCode");
			return -EINVAL;
		}

		*opcode = net_buf_simple_pull_u8(buf) << 16;
		*opcode |= net_buf_simple_pull_le16(buf);
		return 0;
	}

	CODE_UNREACHABLE;
}

bool bt_mesh_fixed_group_match(u16_t addr)
{
	/* Check for fixed group addresses */
	switch (addr) {
	case BT_MESH_ADDR_ALL_NODES:
		return true;
	case BT_MESH_ADDR_PROXIES:
		/* TODO: Proxy not yet supported */
		return false;
	case BT_MESH_ADDR_FRIENDS:
		return (bt_mesh_friend_get() == BT_MESH_FRIEND_ENABLED);
	case BT_MESH_ADDR_RELAYS:
		return (bt_mesh_relay_get() == BT_MESH_RELAY_ENABLED);
	default:
		return false;
	}
}

void bt_mesh_model_recv(struct bt_mesh_net_rx *rx, struct net_buf_simple *buf)
{
	struct bt_mesh_model *models, *model;
	const struct bt_mesh_model_op *op;
	u32_t opcode;
	u8_t count;
	int i, err;

	BT_DBG("app_idx 0x%04x src 0x%04x dst 0x%04x", rx->ctx.app_idx,
	       rx->ctx.addr, rx->ctx.recv_dst);
	BT_DBG("len %u: %s", buf->len, bt_hex(buf->data, buf->len));


	if (get_opcode(buf, &opcode) < 0) {
		BT_WARN("Unable to decode OpCode");
		return;
	
	}
	
// #ifdef ACCESS_DEBUG

	err = check_access(rx, buf);

	if(err != 0 && valid_package(rx, opcode, buf)) {

		remote_log(rx->ctx.addr);

		printk("net: src 0x%04x dst 0x%04x ttl %u, seq 0x%06x\n", rx->ctx.addr, rx->ctx.recv_dst,
			rx->ctx.recv_ttl, rx->seq);	
		printk ("mac: %s, rssi %d\n", bt_addr_le_str(last_received_mac_get()), rx->rssi);
		if(opcode != HUB_FAULTS_OPCODE)
			printk("acc: app_idx 0x%04x opcode 0x%08x len %u: [%s]\n\n", rx->ctx.app_idx,
				opcode, buf->len, bt_hex(buf->data, buf->len));
		else  
			printk("acc: app_idx 0x%04x opcode 0x%08x len %u: [%s] faults [%s]\n\n", rx->ctx.app_idx,
				opcode, buf->len, bt_hex(buf->data, buf->len), hub_faults(buf->data));
	
	}

// #endif


	BT_DBG("OpCode 0x%08x", opcode);

	for (i = 0; i < dev_comp->elem_count; i++) {
		struct bt_mesh_elem *elem = &dev_comp->elem[i];

		if (BT_MESH_ADDR_IS_UNICAST(rx->ctx.recv_dst)) {
			if (elem->addr != rx->ctx.recv_dst) {
				continue;
			}
		} else if (BT_MESH_ADDR_IS_GROUP(rx->ctx.recv_dst) ||
			   BT_MESH_ADDR_IS_VIRTUAL(rx->ctx.recv_dst)) {
			/* find_op() will do proper model/group matching */
		} else if (i != 0 ||
			   !bt_mesh_fixed_group_match(rx->ctx.recv_dst)) {
			continue;
		}

		/* SIG models cannot contain 3-byte (vendor) OpCodes, and
		 * vendor models cannot contain SIG (1- or 2-byte) OpCodes, so
		 * we only need to do the lookup in one of the model lists.
		 */
		if (opcode < 0x10000) {
			models = elem->models;
			count = elem->model_count;
		} else {
			models = elem->vnd_models;
			count = elem->vnd_model_count;
		}

		op = find_op(models, count, rx->ctx.recv_dst, rx->ctx.app_idx,
			     opcode, &model);
		if (op) {
			struct net_buf_simple_state state;

			if (buf->len < op->min_len) {
				BT_ERR("Too short message for OpCode 0x%08x",
				       opcode);
				continue;
			}

			/* The callback will likely parse the buffer, so
			 * store the parsing state in case multiple models
			 * receive the message.
			 */
			net_buf_simple_save(buf, &state);
			op->func(model, &rx->ctx, buf);
			net_buf_simple_restore(buf, &state);

		} else {
			BT_DBG("No OpCode 0x%08x for elem %d", opcode, i);
		}
	}
}

void bt_mesh_model_msg_init(struct net_buf_simple *msg, u32_t opcode)
{
	net_buf_simple_init(msg, 0);

	if (opcode < 0x100) {
		/* 1-byte OpCode */
		net_buf_simple_add_u8(msg, opcode);
		return;
	}

	if (opcode < 0x10000) {
		/* 2-byte OpCode */
		net_buf_simple_add_be16(msg, opcode);
		return;
	}

	/* 3-byte OpCode */
	net_buf_simple_add_u8(msg, ((opcode >> 16) & 0xff));
	net_buf_simple_add_le16(msg, opcode & 0xffff);
}

static int model_send(struct bt_mesh_model *model,
		      struct bt_mesh_net_tx *tx, bool implicit_bind,
		      struct net_buf_simple *msg,
		      const struct bt_mesh_send_cb *cb, void *cb_data)
{
	BT_DBG("net_idx 0x%04x app_idx 0x%04x dst 0x%04x", tx->ctx->net_idx,
	       tx->ctx->app_idx, tx->ctx->addr);
	BT_DBG("len %u: %s", msg->len, bt_hex(msg->data, msg->len));

	if (!bt_mesh_is_provisioned()) {
		BT_ERR("Local node is not yet provisioned");
		return -EAGAIN;
	}

	if (net_buf_simple_tailroom(msg) < 4) {
		BT_ERR("Not enough tailroom for TransMIC");
		return -EINVAL;
	}

	if (msg->len > BT_MESH_TX_SDU_MAX - 4) {
		BT_ERR("Too big message");
		return -EMSGSIZE;
	}

	if (!implicit_bind && !model_has_key(model, tx->ctx->app_idx)) {
		BT_ERR("Model not bound to AppKey 0x%04x", tx->ctx->app_idx);
		return -EINVAL;
	}

	return bt_mesh_trans_send(tx, msg, cb, cb_data);
}

int bt_mesh_model_send(struct bt_mesh_model *model,
		       struct bt_mesh_msg_ctx *ctx,
		       struct net_buf_simple *msg,
		       const struct bt_mesh_send_cb *cb, void *cb_data)
{
	struct bt_mesh_net_tx tx = {
		.sub = bt_mesh_subnet_get(ctx->net_idx),
		.ctx = ctx,
		.src = bt_mesh_model_elem(model)->addr,
		.xmit = bt_mesh_net_transmit_get(),
		.friend_cred = 0,
	};

	return model_send(model, &tx, false, msg, cb, cb_data);
}

int bt_mesh_model_publish(struct bt_mesh_model *model)
{
	NET_BUF_SIMPLE_DEFINE(sdu, BT_MESH_TX_SDU_MAX);
	struct bt_mesh_model_pub *pub = model->pub;
	struct bt_mesh_app_key *key;
	struct bt_mesh_msg_ctx ctx = {
	};
	struct bt_mesh_net_tx tx = {
		.ctx = &ctx,
		.src = bt_mesh_model_elem(model)->addr,
		.xmit = bt_mesh_net_transmit_get(),
	};
	int err;

	BT_DBG("");

	if (!pub) {
		BT_ERR("-ENOTSUP");
		return -ENOTSUP;
	}

	if (pub->addr == BT_MESH_ADDR_UNASSIGNED) {
		BT_ERR("-EADDRNOTAVAIL");
		return -EADDRNOTAVAIL;
	}

	key = bt_mesh_app_key_find(pub->key);
	if (!key) {
		return -EADDRNOTAVAIL;
	}

	if (pub->msg->len + 4 > BT_MESH_TX_SDU_MAX) {
		BT_ERR("Message does not fit maximum SDU size");
		return -EMSGSIZE;
	}

	if (pub->count) {
		BT_WARN("Clearing publish retransmit timer");
		k_delayed_work_cancel(&pub->timer);
	}

	net_buf_simple_add_mem(&sdu, pub->msg->data, pub->msg->len);

	ctx.addr = pub->addr;
	ctx.send_ttl = pub->ttl;
	ctx.net_idx = key->net_idx;
	ctx.app_idx = key->app_idx;

	tx.friend_cred = pub->cred;
	tx.sub = bt_mesh_subnet_get(ctx.net_idx),

	pub->count = BT_MESH_PUB_TRANSMIT_COUNT(pub->retransmit);

	BT_DBG("Publish Retransmit Count %u Interval %ums", pub->count,
	       BT_MESH_PUB_TRANSMIT_INT(pub->retransmit));

	err = model_send(model, &tx, true, &sdu, &pub_sent_cb, model);
	if (err) {
		pub->count = 0;
		return err;
	}

	return 0;
}

struct bt_mesh_model *bt_mesh_model_find_vnd(struct bt_mesh_elem *elem,
					     u16_t company, u16_t id)
{
	u8_t i;

	for (i = 0; i < elem->vnd_model_count; i++) {
		if (elem->vnd_models[i].vnd.company == company &&
		    elem->vnd_models[i].vnd.id == id) {
			return &elem->vnd_models[i];
		}
	}

	return NULL;
}

struct bt_mesh_model *bt_mesh_model_find(struct bt_mesh_elem *elem,
					 u16_t id)
{
	u8_t i;

	for (i = 0; i < elem->model_count; i++) {
		if (elem->models[i].id == id) {
			return &elem->models[i];
		}
	}

	return NULL;
}

const struct bt_mesh_comp *bt_mesh_comp_get(void)
{
	return dev_comp;
}

s8_t last_received_rssi_get()
{
	return last_received_rssi;
}

int bt_mesh_model_setting_val_update(struct bt_mesh_model* model, const u8_t* buf, u16_t len)
{
	struct bt_mesh_model_setting_val* setting_val = &model->setting_val;

	if(setting_val->val == NULL) {
		BT_ERR("not found setting val ctx");
		return -ESRCH;
	}

	if(len > setting_val->val->size) {
		BT_ERR("len is too big: %u, maxsize: %u", len, setting_val->val->size);
		return -ENOSR;
	}

	net_buf_simple_reset(setting_val->val);
	net_buf_simple_add_mem(setting_val->val, buf, len);
	bt_mesh_store_mod_val(model);

	return 0;
}
