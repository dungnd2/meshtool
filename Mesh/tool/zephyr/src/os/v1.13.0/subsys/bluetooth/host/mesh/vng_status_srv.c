/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/vng_status_srv.h>

#include "mesh.h"


#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_VNG_STATUS_SRV)
#include "common/log.h"

#define CID_VNG 0x00FF

#define BT_MESH_MODEL_OP_VNG_STATUS_SET          BT_MESH_MODEL_OP_3(0x01, CID_VNG)
#define BT_MESH_MODEL_OP_VNG_STATUS_GROUP_SET    BT_MESH_MODEL_OP_3(0x02, CID_VNG)

int bt_mesh_vng_status_srv_init(struct bt_mesh_model *model)
{
    BT_DBG("");
    struct bt_mesh_vng_status_srv *srv = model->user_data;
    srv->model = model;
    return 0;
}


static void vng_status_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{

    struct bt_mesh_vng_status_srv *srv = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    if(srv->set_func) {
        srv->set_func(buf, srv->user_data);
    }
}


static void vng_status_group_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{

    struct bt_mesh_vng_status_srv *srv = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    u16_t ctrl_group;
    u16_t dest_group;

    ctrl_group = net_buf_simple_pull_be16(buf);
    dest_group = net_buf_simple_pull_be16(buf);

    if(!BT_MESH_ADDR_IS_GROUP(ctrl_group) || 
          !BT_MESH_ADDR_IS_GROUP(dest_group)) {
        BT_WARN("Invalid data!");
        return;
    }

    if(srv->group_set_func) {
        srv->group_set_func(ctrl_group, dest_group, srv->user_data);
    }
    
}


const struct bt_mesh_model_op bt_mesh_vng_status_srv_op[] = {
    { BT_MESH_MODEL_OP_VNG_STATUS_SET, 6, vng_status_set },
    { BT_MESH_MODEL_OP_VNG_STATUS_GROUP_SET, 4, vng_status_group_set },
    BT_MESH_MODEL_OP_END,
};


