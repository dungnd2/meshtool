/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/level_all_srv.h>

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_LEVEL_ALL_SRV)
#include "common/log.h"


// #define CID_VNG 0x00FF
#define BT_MESH_MODEL_OP_LEVEL_ALL_SET            BT_MESH_MODEL_OP_2(0x90, 0x00)
#define BT_MESH_MODEL_OP_LEVEL_ALL_PORTS_SET       BT_MESH_MODEL_OP_2(0x90, 0x01)


int bt_mesh_level_all_srv_init(struct bt_mesh_model *model, bool primary)
{
    BT_DBG("");
    struct bt_mesh_level_all_srv *srv = model->user_data;
    srv->model = model;
    return 0;
}


static void level_all_data_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    u16_t level;
    u8_t tid;
    u8_t time;
    u8_t delay;

    struct bt_mesh_level_all_srv *srv = model->user_data;


    level = net_buf_simple_pull_be16(buf);
    tid = net_buf_simple_pull_u8(buf);
    time = net_buf_simple_pull_u8(buf);
    delay = net_buf_simple_pull_u8(buf);

    BT_DBG("level: %d, tid: 0x%02x, time: 0x%02x, delay 0x%02x",
           level, tid, time, delay);

    if (srv->set_func == NULL) {
        BT_ERR("not found set_func");
        return;
    }

    srv->set_func(level, tid, time, delay, srv->user_data);
}


static void level_all_ports_set(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    u32_t ports_enabled;
    u8_t tid;
    struct bt_mesh_level_all_srv *srv = model->user_data;

    BT_DBG("buf: %s", bt_hex(buf->data, buf->len));
    ports_enabled = net_buf_simple_pull_be32(buf);
    tid = net_buf_simple_pull_u8(buf);
    
    if (srv->ports_set_func == NULL) {
        BT_ERR("not found ports_set_func");
        return;
    }

    srv->ports_set_func(ports_enabled, tid, srv->user_data);
}



const struct bt_mesh_model_op bt_mesh_level_all_srv_op[] = {
    { BT_MESH_MODEL_OP_LEVEL_ALL_SET, 5, level_all_data_set },
    { BT_MESH_MODEL_OP_LEVEL_ALL_PORTS_SET, 5, level_all_ports_set },
    BT_MESH_MODEL_OP_END,
};
