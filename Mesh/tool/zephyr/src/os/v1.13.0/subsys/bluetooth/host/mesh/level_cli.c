/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/level_cli.h>

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_LEVEL)
#include "common/log.h"

#define BT_MESH_MODEL_OP_GEN_LEVEL_STATUS    BT_MESH_MODEL_OP_2(0x82, 0x08)

int bt_mesh_gen_level_cli_init(struct bt_mesh_model *model, bool primary)
{
    struct bt_mesh_gen_level_cli *cli = model->user_data;
    cli->model = model;
    return 0;
}


static void level_data_status(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    struct bt_mesh_gen_level_cli *cli = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    if (cli->state_func == NULL)
        return;

    cli->state_func(net_buf_simple_pull_be16(buf),
                    net_buf_simple_pull_be16(buf),
                    net_buf_simple_pull_u8(buf),
                    cli->user_data);

}

const struct bt_mesh_model_op bt_mesh_gen_level_cli_op[] = {
    { BT_MESH_MODEL_OP_GEN_LEVEL_STATUS, 5, level_data_status },
    BT_MESH_MODEL_OP_END,
};
