/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/vng_config_srv.h>
#include <device.h>

#include "mesh.h"
#include "net.h"
#include "access.h"

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_VNG_CONFIG_SRV)
#include "common/log.h"


#define PUBLISH_MODELS   3
#define SUBCRIBE_MODELS  3


#define BT_MESH_MODEL_OP_CONFIG_GROUP               BT_MESH_MODEL_OP_3(0x06, CID_VNG)
	#define VNG_CONFIG_GROUPS_GATEWAY_CONTROL   	0x00
	#define VNG_CONFIG_GROUPS_LOW_PRIORITIES		0x01
	#define VNG_CONFIG_GROUPS_STATUS            	0x02

#define BT_MESH_MODEL_OP_CONFIG_COMMAND	            BT_MESH_MODEL_OP_3(0x07, CID_VNG)
	#define VNG_CONFIG_COMMAND_DEVICE_RESET         0x00



#define MODEL_PUB(_model, _vnd_, _net_idx, _pub)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = _model->elem_idx + primary_addr;\
    if(_vnd_) {\
        BT_DBG("vnd pub, elem [0x%04x], model [0x%04x], group [0x%04x]",\
            addr, _model->vnd.id, _pub->addr);\
        bt_mesh_cfg_mod_pub_set_vnd(_net_idx, primary_addr, addr,\
            _model->vnd.id,_model->vnd.company, _pub, NULL);\
    } else {\
        BT_DBG("vnd pub, elem [0x%04x], model [0x%04x], group [0x%04x]",\
            addr, _model->vnd.id, _pub->addr);\
        bt_mesh_cfg_mod_pub_set(_net_idx, primary_addr, addr,\
            _model->id, _pub, NULL);\
    }\
}

#define MODEL_SUB(_model, _vnd_, _net_idx, _subaddr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = _model->elem_idx + primary_addr;\
    if(_vnd_) {\
        BT_DBG("vnd sub, elem [0x%04x], model [0x%04x], group [0x%04x]",\
            addr, _model->vnd.id, _subaddr);\
        bt_mesh_cfg_mod_sub_add_vnd(_net_idx, primary_addr, addr,\
                _subaddr, _model->vnd.id, CID_VNG, NULL);\
    } else {\
        BT_DBG("sig sub, elem [0x%04x], model [0x%04x], group [0x%04x]",\
            addr, _model->id, _subaddr);\
        bt_mesh_cfg_mod_sub_add(_net_idx, primary_addr, addr,\
                _subaddr, _model->id, NULL);\
    }\
}


#define MODEL_SUB_REPLACE(_model, _vnd_, _net_idx, _subaddr)\
{\
    u16_t addr; \
    u16_t primary_addr;\
    primary_addr = bt_mesh_primary_addr();\
    addr = _model->elem_idx + primary_addr;\
    if(_vnd_) {\
        BT_DBG("vnd sub overwrite, elem [0x%04x], model [0x%04x], group [0x%04x]",\
                addr, _model->vnd.id, _new_subaddr);\
        bt_mesh_cfg_mod_sub_overwrite_vnd(_net_idx, primary_addr, addr,\
                _subaddr, _model->vnd.id, CID_VNG, NULL);\
    } else {\
        BT_DBG("sig sub overwrite, elem [0x%04x], model [0x%04x], group [0x%04x]",\
                addr, _model->id, _new_subaddr);\
        bt_mesh_cfg_mod_sub_overwrite(_net_idx, primary_addr, addr,\
                _subaddr, _model->id, NULL);\
    }\
}



#define PUB_PERIOD_1S(seconds) ((0x01<<6 | seconds) & 0xFF)
#define PUB_PERIOD_10S(n) ((0x02<<6 | n) & 0xFF)

struct publish_register
{
	struct bt_mesh_model* model;
	u16_t group;
    bool configurated;
};

struct subcribe_register
{
	struct bt_mesh_model* model;
    /**
     *  To replace the subcribed group, 
     *  old group need removing firstly.
     */
    struct { 
        u16_t old;
        u16_t new;
    } group;
    bool configurated;

};

struct vng_config_ctx 
{

	struct bt_mesh_vng_config_srv* srv;

	struct {
		struct publish_register models[PUBLISH_MODELS];
		u8_t count;
	} publish;

	struct {	
		struct subcribe_register models[SUBCRIBE_MODELS];
		u8_t count;
	} subcribe;

    struct k_delayed_work timer;

	bool stored;
	
};


static struct vng_config_ctx config_ctx  = {
    .publish = {
        .count = 0,
    },
    .subcribe = {
        .count = 0,
    },
	.stored = false,
};



static void _reconfig_models(struct k_work* work);
static void _store_configuration();

int bt_mesh_vng_config_srv_init(struct bt_mesh_model *model)
{
    BT_DBG("");
    struct bt_mesh_vng_config_srv *srv = model->user_data;
    srv->model = model;
    config_ctx.srv = srv;

    BT_MESH_MODEL_SETTING_VAL_DEFINE(vng_config_model_setting, model, 
    			sizeof(config_ctx.srv->groups));

    k_delayed_work_init(&config_ctx.timer, _reconfig_models);
    return 0;
}


int bt_mesh_vng_config_setting_commit()
{	
    if(config_ctx.srv == NULL) {
        BT_ERR("Not found srv context");
        return -1;
    }

	if (config_ctx.srv->model->setting_val.val->len != 
			sizeof(config_ctx.srv->groups)) {
		BT_WARN("Restored setting data is invalid");
		return -1;
	}


	config_ctx.stored = true;

	memcpy(&config_ctx.srv->groups, config_ctx.srv->model->setting_val.val->data,
					sizeof(config_ctx.srv->groups));

    BT_DBG("buf: %s", bt_hex(&config_ctx.srv->groups, sizeof(config_ctx.srv->groups)));

	return 0;
}


int bt_mesh_vng_config_do_config()
{	
	
	if(config_ctx.stored != true) {
		_store_configuration();
	}

	k_delayed_work_submit(&config_ctx.timer, 100);

	return 0;
}



int bt_mesh_vng_group_publish_register(u16_t group, struct bt_mesh_model *model)
{

    BT_DBG("elem-idx %u, model-idx %u,  group 0x%04x", model->elem_idx, 
                    model->mod_idx, group);

	for(int i = 0; i < config_ctx.publish.count; i++) {
		if(model == config_ctx.publish.models[i].model) {
			config_ctx.publish.models[i].group = group;
			return 0;
		}
	}

    if(config_ctx.publish.count >= PUBLISH_MODELS) {
        BT_WARN("Out of slots");
        return -1;
    }

	config_ctx.publish.models[config_ctx.publish.count].model = model;
    config_ctx.publish.models[config_ctx.publish.count].group = group;

    if(config_ctx.stored == true)
	   config_ctx.publish.models[config_ctx.publish.count].configurated = true;
    else 
       config_ctx.publish.models[config_ctx.publish.count].configurated = false;

    config_ctx.publish.count++;

	return 0;

}


int bt_mesh_vng_group_subcribe_register(u16_t group, struct bt_mesh_model *model, bool only)
{

    BT_DBG("elem-idx %u, model-idx %u,  group 0x%04x", model->elem_idx, 
                    model->mod_idx, group);

    if(only) {
        for(int i = 0; i < config_ctx.subcribe.count; i++) {
            if(model == config_ctx.subcribe.models[i].model) {
                config_ctx.subcribe.models[i].group.old = group;
                config_ctx.subcribe.models[i].group.new = group;
                return 0;
            }
        }
    }

    if(config_ctx.subcribe.count >= SUBCRIBE_MODELS) {
        BT_WARN("Out of slots");
        return -1;
    }
	
	config_ctx.subcribe.models[config_ctx.subcribe.count].model = model;
    config_ctx.subcribe.models[config_ctx.subcribe.count].group.old = group;
    config_ctx.subcribe.models[config_ctx.subcribe.count].group.new = group;

    if(config_ctx.stored == true) 
	   config_ctx.subcribe.models[config_ctx.subcribe.count].configurated = true;
    else 
       config_ctx.subcribe.models[config_ctx.subcribe.count].configurated = false;

    config_ctx.subcribe.count++;

	return 0;
}

int bt_mesh_vng_prioritied_groups_get(u16_t* min, u16_t* max)
{
    if(config_ctx.srv == NULL)
        return -1;

    *min = config_ctx.srv->groups.priorities.min;
    *max = config_ctx.srv->groups.priorities.max;
    return 0;
}


static void _store_configuration()
{
	bt_mesh_model_setting_val_update(config_ctx.srv->model, 
		(const u8_t*)(&config_ctx.srv->groups), sizeof(config_ctx.srv->groups));
}

static void _reconfig_models(struct k_work* work)
{

    if(!bt_mesh_is_provisioned())
        return;

	bool vnd;

    for(int i = 0; i < config_ctx.publish.count; i++) {

    	if(config_ctx.publish.models[i].configurated)
    		continue;

        struct bt_mesh_cfg_mod_pub pub = {
            .addr = config_ctx.publish.models[i].group,
            .app_idx = 0,
            .ttl = 7,
            .transmit = BT_MESH_PUB_TRANSMIT(1, 50),
        };

        config_ctx.publish.models[i].configurated = true;
    	vnd = config_ctx.publish.models[i].model->vnd.company == CID_VNG;
    	MODEL_PUB(config_ctx.publish.models[i].model, 
    		vnd, 0, (&pub));

        /* give other works a chance */
        k_delayed_work_submit(&config_ctx.timer, 200); 
        return;
    }

    for(int i = 0; i < config_ctx.subcribe.count; i++) {
    
    	if(config_ctx.subcribe.models[i].configurated)
    		continue;

        config_ctx.subcribe.models[i].configurated = true;
    	vnd = config_ctx.subcribe.models[i].model->vnd.company == CID_VNG;

        if(config_ctx.subcribe.models[i].group.old ==
                config_ctx.subcribe.models[i].group.new) {

            MODEL_SUB(config_ctx.subcribe.models[i].model, 
                vnd, 0, config_ctx.subcribe.models[i].group.new);    
        } else {
            MODEL_SUB_REPLACE(config_ctx.subcribe.models[i].model, 
                vnd, 0, config_ctx.subcribe.models[i].group.old,
                        config_ctx.subcribe.models[i].group.new); 

            config_ctx.subcribe.models[i].group.old =
                config_ctx.subcribe.models[i].group.new;
        }
    	
        /* give other works a chance */
        k_delayed_work_submit(&config_ctx.timer, 200);
        return;
    }

    // BT_DBG("Out of works");

}


static void _vng_config_groups(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{

	u8_t command;
    struct bt_mesh_vng_config_srv *srv = model->user_data;

    BT_DBG("buf: %s", bt_hex(buf->data, buf->len));

    command = net_buf_simple_pull_u8(buf);

    if (command == VNG_CONFIG_GROUPS_GATEWAY_CONTROL) {
    	
    	BT_DBG("VNG_CONFIG_GROUPS_GATEWAY_CONTROL");
    	
    	u16_t gw_group;
  
    	gw_group = net_buf_simple_pull_be16(buf);
    	if(!BT_MESH_ADDR_IS_GROUP(gw_group)) {
    		BT_WARN("Group gateway is invalid");
    		return;
    	}

    	if(srv->groups.gateway_ctrl == gw_group) {
    		BT_WARN("Match current gateway group");
    		return;
    	}

		config_ctx.stored = false;
    	for(int i = 0; i < PUBLISH_MODELS; i++) {
    		if(config_ctx.publish.models[i].group == 
    			 	srv->groups.gateway_ctrl) {
    
    			config_ctx.publish.models[i].group = gw_group;
    			config_ctx.publish.models[i].configurated = false;
    		}
    	}

    	srv->groups.gateway_ctrl = gw_group;

    } else if (command == VNG_CONFIG_GROUPS_LOW_PRIORITIES) {

    	BT_DBG("VNG_CONFIG_GROUPS_LOW_PRIORITIES");

    	u16_t min_group, max_group;
    	min_group = net_buf_simple_pull_be16(buf);
    	max_group = net_buf_simple_pull_be16(buf);

    	if(!BT_MESH_ADDR_IS_GROUP(min_group) || 
    			!BT_MESH_ADDR_IS_GROUP(max_group)) {
    		BT_WARN("Groups priorities are invalid");
    		return;
    	}

    	if(srv->groups.priorities.min != min_group ||
    			srv->groups.priorities.max != max_group) {

    		config_ctx.stored = false;
    	}

    	srv->groups.priorities.min = min_group;
    	srv->groups.priorities.max = max_group;

    } else if (command == VNG_CONFIG_GROUPS_STATUS) {

    	BT_DBG("VNG_CONFIG_GROUPS_STATUS");

    	u16_t ctrl_group;
    	u16_t dest_group;

    	ctrl_group = net_buf_simple_pull_be16(buf);
    	dest_group = net_buf_simple_pull_be16(buf);

    	if(!BT_MESH_ADDR_IS_GROUP(ctrl_group) || 
    			!BT_MESH_ADDR_IS_GROUP(dest_group)) {
    		BT_WARN("Groups status are invalid");
    		return;
    	}

    	if (srv->groups.status.ctrl != ctrl_group) {

    		config_ctx.stored = false;
    		for(int i = 0; i < SUBCRIBE_MODELS; i++) {
	    		if(config_ctx.subcribe.models[i].group.new == 
	    			 		srv->groups.status.ctrl) {
	    			config_ctx.subcribe.models[i].group.new = ctrl_group;
	    			config_ctx.subcribe.models[i].configurated = false;
	    		}
    		}
    	}

    	if (srv->groups.status.dest != dest_group) {

    		config_ctx.stored = false;

    		for(int i = 0; i < PUBLISH_MODELS; i++) {
	    		if(config_ctx.publish.models[i].group == 
	    			 			srv->groups.status.dest) {
	    			config_ctx.publish.models[i].group = dest_group;
	    			config_ctx.publish.models[i].configurated = false;
	    		}
    		}
    	}
    	
    	srv->groups.status.ctrl = ctrl_group;
    	srv->groups.status.dest = dest_group;

    } else {
    	BT_WARN("Command not supported");
    	return;
    }

    bt_mesh_vng_config_do_config();
    
}

static void _vng_config_command(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{

	u8_t command;

    // struct bt_mesh_vng_config_srv *srv = model->user_data;

    BT_DBG("buf: %s", bt_hex(buf->data, buf->len));

    command = net_buf_simple_pull_u8(buf);

    if(command == VNG_CONFIG_COMMAND_DEVICE_RESET) {
    	u8_t comfirmed = net_buf_simple_pull_u8(buf);
    	if(comfirmed == 1) {
    		// NVIC_SystemReset();
    	}
    } else {
    	BT_WARN("Command not supported");
    	return;
    }

}


const struct bt_mesh_model_op bt_mesh_vng_config_srv_op[] = {
	{ BT_MESH_MODEL_OP_CONFIG_GROUP, 3, _vng_config_groups },
	{ BT_MESH_MODEL_OP_CONFIG_COMMAND, 2, _vng_config_command },
    BT_MESH_MODEL_OP_END,
};
