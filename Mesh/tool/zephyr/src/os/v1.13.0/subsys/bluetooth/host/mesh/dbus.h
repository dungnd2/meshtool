#ifndef MESH_DBUS_H
#define MESH_DBUS_H

#include <zephyr.h>


#define TO_MASTER_CHANNEL "dung.dbus.mesh.master"
#define TO_SLAVE_CHANNEL  "dung.dbus.mesh.slave"
#define TO_LOCAL_CHANNEL  "dung.dbus.mesh.local"


#define DBUS_MESH_ADDR_OFF 0
#define DBUS_MESH_ADDR_LEN sizeof(bt_addr_le_t)
#define DBUS_MESH_RSSI_OFF (DBUS_MESH_ADDR_OFF + DBUS_MESH_ADDR_LEN)
#define DBUS_MESH_RSSI_LEN sizeof(s8_t)
#define DBUS_MESH_ADVT_OFF (DBUS_MESH_RSSI_OFF + DBUS_MESH_RSSI_LEN)
#define DBUS_MESH_ADVT_LEN sizeof(u8_t)
#define DBUS_MESH_DATA_OFF (DBUS_MESH_ADVT_OFF + DBUS_MESH_ADVT_LEN)
#define DBUS_MESH_DATA_LEN_MAX   31


int dbus_mesh_init();

// master receive mesh package then broadcast to all slaves.
#ifdef CONFIG_BT_MESH_DBUS_MASTER
int dbus_mesh_master_broadcast(const u8_t* data, size_t len);
#else 
static inline int dbus_mesh_master_broadcast(const u8_t* data, size_t len)
{
    return 0;
}
#endif 


#ifdef CONFIG_BT_MESH_DBUS_LOCAL
int dbus_mesh_local_broadcast(const u8_t* data, size_t len);
#else 
static inline int dbus_mesh_local_broadcast(const u8_t* data, size_t len)
{
    return 0;
}
#endif 



#ifdef CONFIG_BT_MESH_DBUS_SLAVE
int dbus_mesh_slave_forward(struct net_buf *buf);
#else 
static inline int dbus_mesh_slave_forward(struct net_buf *buf)
{
    return 0;
}
#endif 

int dbus_mesh_start_listen();

#endif //MESH_DBUS_H