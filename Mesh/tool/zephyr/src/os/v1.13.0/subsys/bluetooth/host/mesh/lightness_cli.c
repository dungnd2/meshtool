/*  Bluetooth Mesh */

/*
 * Copyright (c) 2018 VNG Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <zephyr/types.h>
#include <misc/util.h>
#include <misc/byteorder.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/mesh.h>
#include <bluetooth/mesh/lightness_cli.h>

#define BT_DBG_ENABLED IS_ENABLED(CONFIG_BT_MESH_DEBUG_LIGTHT_LIGHTNESS)
#include "common/log.h"

#define BT_MESH_MODEL_OP_LIGHT_LIGHTNESS_STATUS BT_MESH_MODEL_OP_2(0x82, 0x4E)


int bt_mesh_light_lightness_cli_init(struct bt_mesh_model *model, bool primary)
{
    struct bt_mesh_lightness_cli *cli = model->user_data;
    cli->model = model;
    return 0;
}


static void light_lightness_data_status(struct bt_mesh_model *model,
                 struct bt_mesh_msg_ctx *ctx,
                 struct net_buf_simple *buf)
{
    struct bt_mesh_lightness_cli *cli = model->user_data;

    BT_DBG("net_idx 0x%04x app_idx 0x%04x src 0x%04x len %u: %s",
           ctx->net_idx, ctx->app_idx, ctx->addr, buf->len,
           bt_hex(buf->data, buf->len));

    if (cli->func != NULL)
        cli->func(ctx->addr, net_buf_simple_pull_be16(buf), cli->user_data);

}

const struct bt_mesh_model_op bt_mesh_light_lightness_cli_op[] = {
    { BT_MESH_MODEL_OP_LIGHT_LIGHTNESS_STATUS, 2, light_lightness_data_status },
    BT_MESH_MODEL_OP_END,
};
