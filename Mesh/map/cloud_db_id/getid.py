
import json 

lightjson = {}
lightdb = []
lightid = []


firstline = "<td class=\"text-center\"><input type=\"checkbox\" name=\"table_records\""
readline = "<td class=\"col-md-1 ng-binding\">"
macline  = "<td class=\"col-md-2 ng-binding\">"

with open("../db/lighthub.json", "r") as df:
    lightjson = json.loads(df.read())
    lightdb = lightjson["lighthub"]
    # print lightdb
    df.close()



def set_db_id(addr, id):
    for light in lightdb:
        if light["dec"] == addr:
            light["db_id"] = id 
            return 0


with open("vng.html", "r") as db:
    readeds = 0
    reading = 0
    id = 0
    addr = 0
    mac = ""
    for line in db:
        if firstline in line:
            # print "firstline:", line
            reading = 1
            continue
        
        if reading == 1:
            if readeds >= 3:
                reading = 0
                readeds = 0
                # print "reset reading {}, readeds {}".format(reading, readeds)
                continue

            if readline in line or macline in line:

                if readeds == 0:
                    # print "line 0:", line
                    start = line.find(readline) + len(readline)
                    end   = line.rfind('<')
                    _id = line[start:end]
                    id = int(_id, 10)
                    # print "id ", id

                elif readeds == 1:
                    start = line.find(macline) + len(macline)
                    end   = line.rfind('<')
                    mac = line[start:end]
                    # print "mac ", mac

                else:
                    start = line.find(readline) + len(readline)
                    end   = line.rfind('(')
                    
                    _addr = line[start:end]
                    _addr.strip()
                    addr = int(_addr, 10)

                    # print "addr ", addr
                    lightid.append((id, addr, mac))
                    set_db_id(addr, id)
                
                readeds += 1
            

print "lightid", len(lightid)

with open("../db/_lighthub.json", "w") as df:
    lightjson["lighthub"] = lightdb 
    # print lightjson
    json.dump(lightjson, df)
    df.close()
        

