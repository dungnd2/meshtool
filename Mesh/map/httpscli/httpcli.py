import http.client
import json

mapfile = open("mapid.json", "r")
mapid = json.loads(mapfile.read())["map"]

print(mapid)
class CampusMap:
    def __init__(self):
        self.connection = http.client.HTTPSConnection("vng.icbm.vn")

    def close(self):
        self.connection.close()

    def get_path(self, path):
        print "get path {}".format(path)
        self.connection.request("GET", path)
        response = self.connection.getresponse()
        return response
    
    def path_from_id(self, id):
        path = "/iot/be/api/lighthub?cm=get_list_light_hub_by_map_id&dt={\"map_id\":" + str(id) + "}&office=campus"
        return path

    def get_map_id(self, id, tofile = ""):
        path = self.path_from_id(id)
        response = self.get_path(path)
        data = response.read().decode()
        jdata = json.loads(data)
        jdata["area"] = id


        with open(tofile, "w") as df:
            json.dump(jdata, df)
        
        return jdata

        # if tofile != "":
        #     file = open(tofile, "w")
        #     file.write(data)
        #     file.close()

        # return json.loads(data)


campusMap = CampusMap()
for map in mapid:
    store_file ="database/" + map["floor"] + "/" + map["name"]
    response = campusMap.get_map_id(map["id"], store_file)
    # print("response {}".format(response))