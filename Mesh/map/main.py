import json 
import os
from openpyxl import Workbook

AREA_FREFIX = "./HTTPSCLI/DATABASE/F1/"


workbook = Workbook()
hubsheet = workbook.create_sheet("light_hub")
onoffsheet = workbook.create_sheet("onoff")


alls = []
lights = []
f1_lights = []
f2_lights = []
f3_lights = []
onoffs = []


files = []

for r, d, f in os.walk("./httpscli/database"):
    for file in f:
        file_path = os.path.join(r, file)
        files.append(file_path)

files.sort()


def get_floor(file_path):
    if "f1" in file_path:
        return "f1"
    elif "f2" in file_path:
        return "f2"
    elif "f3" in file_path:
        return "f3"
    else:
        return "fa"

for file_path in files:

    # worksheet = workbook.create_sheet(os.path.basename(r) + "." + file)

    area = file_path.upper()[len(AREA_FREFIX):]
    hubsheet.append([""])
    hubsheet.append([file_path.upper()])
    onoffsheet.append([""])
    onoffsheet.append([file_path.upper()])

    
    with open(file_path) as ofile:
        try:
            jfile = json.loads(ofile.read())
            
            area_id = jfile["area"]

            for light in jfile["dt"]["light_hubs"]:
                mac     = light["light_hub_code"]
                unicast = light["unicast_id"]
                ports   = light["port_infos"]
            
                no_ports = len(ports)
                unicast10 = "_{:d}".format(unicast)
                unicast16 = "_{:04X}".format(unicast)

                
                if no_ports != 8:
                    print "hub {:d} ports {:d} ({})".format(unicast, no_ports, file_path)

                floor = get_floor(file_path)

                _type = "DIM"
                if unicast16[4] == '4' or unicast16[4] == 'C':
                    _type = "ONOFF"

                has_uni = False
                if unicast in alls:
                    has_uni = True
                else:
                    alls.append(unicast)

                if _type == "ONOFF":
                    if has_uni: 
                        onoffsheet.append(["", "", unicast10, unicast16, mac, "DUP"])
                    else: 
                        onoffsheet.append(["", "", unicast10, unicast16, mac])

                        onoffs.append((floor, [unicast16, mac], area, area_id))
                else:
                    if has_uni:
                        hubsheet.append(["", "", unicast10, unicast16, mac, "DUP"])
                    else:
                        #excel store
                        hubsheet.append(["", "", unicast10, unicast16, mac])
                        #database store
                        lights.append((floor, [unicast, mac], area, area_id))
                        
                        if floor == "f1":
                            f1_lights.append([unicast16, mac])
                        elif floor == "f2":
                            f2_lights.append([unicast16, mac])
                        elif floor == "f3":
                            f3_lights.append([unicast16, mac])

        except:

            print "json.loads({}) failed".format(file_path)




print "lights {:d}, f1 {:d}, f2 {:d}, f3 {:d}".format(len(lights), len(f1_lights), len(f2_lights), len(f3_lights))
print "onoffs {:d}".format(len(onoffs))

workbook.save("database.xlsx")


records = {}
light_record = []
for idx, light in enumerate(lights):
    
    record = {}
    unicast = light[1][0]
    unicast16 = "{:04X}".format(unicast)
    record["id"]   = idx 
    record["mac"]  = light[1][1]
    record["area_name"] = light[2]
    record["area_id"] = light[3]
    record["dec"] = unicast
    record["hex"] = unicast16
    light_record.append(record)

with open("db/lighthub.json", "w") as df:
    records["lighthub"] = light_record
    json.dump(records, df)



            